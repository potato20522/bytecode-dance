package com.potato.bytecode.processor;

public class IdAlreadyUsedException extends RuntimeException{
    FactoryAnnotatedClass factoryAnnotatedClass;

    public IdAlreadyUsedException() {
        super();
    }

    public IdAlreadyUsedException(FactoryAnnotatedClass existing) {
        this();
        this.factoryAnnotatedClass = existing;
    }

    public FactoryAnnotatedClass getExisting() {
        return factoryAnnotatedClass;
    }
}
