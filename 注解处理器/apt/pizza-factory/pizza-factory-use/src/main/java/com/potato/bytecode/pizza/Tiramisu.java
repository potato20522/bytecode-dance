package com.potato.bytecode.pizza;

import com.potato.bytecode.factory.Factory;

@Factory(id = "Tiramisu", type = Meal.class)
public class Tiramisu implements Meal {
    @Override
    public float getPrice() {
        return 4.5f;
    }
}