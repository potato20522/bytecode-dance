package com.potato.bytecode.pizza;

import com.potato.bytecode.factory.Factory;

@Factory(id = "Calzone", type = Meal.class)
public class CalzonePizza implements Meal {
    @Override 
    public float getPrice() {
        return 8.5f;
    }
}