package com.potato.bytecode.namecheck;

/**
 * 包含多个不规范的命名
 */
public class BADLY_NAMED_CODE {
    enum colors {
        red, blue, green;
    }

    static final int _FORTY_TWO = 66;

    public static int NOT_A_CONSTANT = _FORTY_TWO;

    protected void BADLY_NAMED_CODE2() {
        return;
    }

    public void NOTcamelCASEmethodNAME() {
        return;
    }
}