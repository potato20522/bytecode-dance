package example;
@Log
public class Hello {
    @Log
    public String name;

    @Log
    public void setName(String name) {
        this.name = name;
    }
    @Log
    public static void main(String[] args) {
        @Log
        Hello hello = new Hello();
        hello.setName("aaaa");
        System.out.println(hello.name);
    }
}
