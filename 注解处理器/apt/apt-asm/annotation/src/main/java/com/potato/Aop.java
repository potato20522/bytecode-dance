package com.potato;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标记注解。
 * 在使用上我们需要在类头通过@Aop标记，
 * 内部方法通过@Before,@After,@Around,@AfterReturning,@AfterThrowing标记，
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.TYPE)
public @interface Aop{}