package com.potato;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.List;

public class ImplantCodeClassVisitor extends ClassVisitor {
    private List<MethodPointCut> methodPointCuts;
    private String clzName;

    public ImplantCodeClassVisitor(ClassVisitor classVisitor, List<MethodPointCut> methodPointCuts) {
        super(Opcodes.ASM6, classVisitor);
        this.methodPointCuts = methodPointCuts;
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(version, access, name, signature, superName, interfaces);
        clzName = name;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);
        List<MethodPointCut> methodPointCuts = Util.matcherMethodPointCuts(this.methodPointCuts, clzName, name, descriptor);

        if (mv != null && !methodPointCuts.isEmpty()) {
            mv = new ImplantCodeMethodVisitor(mv, access, name, descriptor, methodPointCuts);
        }
        return mv;
    }
}