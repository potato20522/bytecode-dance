package com.potato;

import lombok.Data;
import org.objectweb.asm.tree.InsnList;


@Data
public class MethodPointCut {
    //切入类全限定名
    private String clzName;
    //切入方法名及描述
    private String methodDesc;
    //前置织入操作码及操作数
    private InsnList beforeInstructions;
    //后置织入操作码及操作数
    private InsnList afterInstructions;
    //注解类
    private Class<?> annoClass;
}