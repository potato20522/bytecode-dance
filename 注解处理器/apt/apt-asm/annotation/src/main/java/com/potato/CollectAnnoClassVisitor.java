package com.potato;

import org.objectweb.asm.*;

import java.util.ArrayList;

public class CollectAnnoClassVisitor extends ClassVisitor {
    private String clzName;
    private boolean needReduce;
    private ArrayList<MethodPointCut> methodPointCuts = new ArrayList<>();

    public CollectAnnoClassVisitor(ClassVisitor classVisitor) {
        super(Opcodes.ASM6, classVisitor);
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(version, access, name, signature, superName, interfaces);
        clzName = name;
    }


    @Override
    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
        needReduce = descriptor.equals(Type.getDescriptor(Aop.class));
        return super.visitAnnotation(descriptor, visible);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);
        if (needReduce) {
            mv = new InstructionsCollectMethodNode(this, access, name, descriptor, signature, exceptions);
        }
        return mv;
    }

    public ArrayList<MethodPointCut> getMethodPointCuts() {
        return methodPointCuts;
    }

    public void setMethodPointCuts(ArrayList<MethodPointCut> methodPointCuts) {
        this.methodPointCuts = methodPointCuts;
    }
}