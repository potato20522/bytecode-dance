package com.potato;


import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.commons.AdviceAdapter;
import org.objectweb.asm.Opcodes;

import java.util.List;

public class ImplantCodeMethodVisitor extends AdviceAdapter {
    private List<MethodPointCut> methodPointCuts;
    private Label handler;

    protected ImplantCodeMethodVisitor(MethodVisitor methodVisitor, int access, String name, String descriptor, List<MethodPointCut> methodPointCuts) {
        super(Opcodes.ASM6, methodVisitor, access, name, descriptor);
        this.methodPointCuts = methodPointCuts;
    }

    @Override
    protected void onMethodEnter() {
        for (MethodPointCut methodPointCut : methodPointCuts) {
            implantBeforeCode(methodPointCut);
        }
        super.onMethodEnter();
    }

    @Override
    public void visitTryCatchBlock(Label start, Label end, Label handler, String type) {
        super.visitTryCatchBlock(start, end, handler, type);
        this.handler = handler;
    }

    @Override
    public void visitLabel(Label label) {
        super.visitLabel(label);
        if (label == handler) {
            for (MethodPointCut methodPointCut : methodPointCuts) {
                if (methodPointCut.getAnnoClass() == AfterThrowing.class) {
                    implantAfterCode(methodPointCut);
                }
            }
        }
    }

    @Override
    protected void onMethodExit(int opcode) {
        for (MethodPointCut methodPointCut : methodPointCuts) {
            if (methodPointCut.getAnnoClass() != AfterThrowing.class) {
                implantAfterCode(methodPointCut);
            }
        }
        super.onMethodExit(opcode);
    }

    private void implantBeforeCode(MethodPointCut methodPointCut) {
        if (methodPointCut.getBeforeInstructions() != null) {
            methodPointCut.getBeforeInstructions().accept(this);
        }
    }


    private void implantAfterCode(MethodPointCut methodPointCut) {
        if (methodPointCut.getAfterInstructions() != null) {
            methodPointCut.getAfterInstructions().accept(this);
        }
    }
}