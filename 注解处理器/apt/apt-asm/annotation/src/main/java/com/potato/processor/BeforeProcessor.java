package com.potato.processor;

import com.google.auto.service.AutoService;
import com.potato.Before;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.Set;

@AutoService(Processor.class)
@SupportedAnnotationTypes("com.potato.Before")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class BeforeProcessor extends AbstractProcessor {
    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Set<? extends Element> elementsAnnotatedWith = roundEnv.getElementsAnnotatedWith(Before.class);
        for (Element element : elementsAnnotatedWith) {
            Before before = element.getAnnotation(Before.class);
            System.out.println(before.clzName());
            System.out.println(before.methodDesc());
        }
        return false;
    }
}
