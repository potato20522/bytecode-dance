package com.potato;

/**
 * 环绕注解需要在原函数的前后都炽入代码，
 * 新增一个接口表示对原函数的调用
 */
public interface Method {
    void invoke();
}