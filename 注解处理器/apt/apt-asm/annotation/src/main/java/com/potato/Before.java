package com.potato;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.CLASS)
public @interface Before {
     String clzName();
    //不能写Class，因为会触发类加载，而编译期类无法加载。
    //Class<?> target();
    String methodDesc();
}
