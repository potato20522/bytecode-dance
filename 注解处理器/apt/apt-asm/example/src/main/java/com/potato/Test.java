package com.potato;

public class Test {
    public void say() {
        System.out.println("say");
    }

    public void hello() {
        System.out.println("hello");
    }

    public void run() {
        System.out.println("run");
    }

    public int ret() {
        System.out.println("ret");
        return 0;
    }

    public void callback() {
        System.out.println("callback");
    }

    public void onCreate() {
        System.out.println("onCreate");
    }
}
