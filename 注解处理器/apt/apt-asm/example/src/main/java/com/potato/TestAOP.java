package com.potato;

/**
 * 1.收集注解
 * 2.
 */
@Aop
public class TestAOP {
    @Around(clzName = "com/potato/Test", methodDesc = "say()")
    public void say() {
        System.out.println("hello2");
    }

    @Before(clzName = "com/potato/Test", methodDesc = "hello()")
    public void helloAdvice() {
        System.out.println("before");
    }

    @After(clzName = "com/potato/Test", methodDesc = "run()")
    public void run() {
        System.out.println("after");
    }

    @AfterReturning(clzName = "com/potato/Test", methodDesc = "ret()")
    public void ret() {
        System.out.println("ret");
    }

    @AfterThrowing(clzName = "com/potato/Test", methodDesc = "ret()")
    public void ret2() {
        System.out.println("error");
    }

    @Before(clzName = "com/potato/Test", methodDesc = "callback()")
    public void callbackAdvice() {
        System.out.println("callback before");
    }

    @After(clzName = "com/potato/Test", methodDesc = "onCreate()")
    public void onCreate() {
        System.out.println("onCreate");
    }

    @Around(clzName = "com/potato/Test", methodDesc = "say()")
    public void say(Method method) {
        System.out.println("hello1");
        method.invoke();
        System.out.println("hello2");
    }
}