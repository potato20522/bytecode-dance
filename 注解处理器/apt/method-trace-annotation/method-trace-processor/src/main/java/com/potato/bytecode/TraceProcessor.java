package com.potato.bytecode;

import com.google.auto.service.AutoService;
import com.squareup.javapoet.MethodSpec;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.Names;
import jdk.internal.org.objectweb.asm.ClassVisitor;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import java.util.Set;

@AutoService(Processor.class)
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes("com.potato.bytecode.Trace")
public class TraceProcessor extends AbstractProcessor {
    private Messager messager; //用于编译时输出信息
    // 生成Java源码，生成在编译target/注解处理器目录,文件生成器， 类 资源 等，就是最终要生成的文件 是需要Filer来完成的
    private Filer filer;
    //提供了待处理的抽象语法树
    private JavacTrees trees;
    //封装了创建AST节点的一些方法
    private TreeMaker treeMaker;
    //提供了创建标识符的方法
    private Names names;
    // 操作Element的工具类（类，函数，属性，其实都是Element）
    private Elements elementTool;
    // type(类信息)的工具类，包含用于操作TypeMirror的工具方法
    private Types typeTool;


    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        messager = processingEnv.getMessager();
        this.trees = JavacTrees.instance(processingEnv);
        Context context = ((JavacProcessingEnvironment) processingEnv).getContext(); //获取Java注解处理器的上下文
        this.treeMaker = TreeMaker.instance(context); //获取TreeMaker对象
        this.names = Names.instance(context);//获取Names对象
        this.filer = processingEnv.getFiler();
        this.elementTool = processingEnv.getElementUtils();
        this.typeTool =  processingEnv.getTypeUtils();
    }

    /**
     *
     * @param annotations 当前注解处理器支持的注解集合
     * @param roundEnv 当前注解处理器环境，可以获取到所有使用了该注解的元素
     * @return true表示表示这个注解处理器已经处理了对应的注解，后面的其它处理器将不再处理
     * false代表这个轮次中代码的语法树没有改变，无需构造新的JavaCompiler实例，反之则需要改变
     */
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(Trace.class);
        for (Element element : elements) {
            if (element.getKind() == ElementKind.METHOD) {


            }
        }
        return true;
    }

    private void warn(Element e, String msg, Object... args) {
        messager.printMessage(Diagnostic.Kind.NOTE, String.format(msg, args), e);
    }
}
