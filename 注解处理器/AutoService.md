# 概述

在写好注解处理器的类后，还需要在resources下创建MET-INF/services目录，再创建文件：javax.annotation.processing.Processor，这个文件里写注解处理器的全类名，比较麻烦，AutoService这个库就可以帮我们偷懒，实现这个操作。

教程

https://blog.csdn.net/wusj3/article/details/107975241

[使用google autoservice 自动生成java spi 描述文件 - 荣锋亮 - 博客园 (cnblogs.com)](https://www.cnblogs.com/rongfengliang/p/11695684.html)

官网 https://github.com/google/auto

# 应用--生成spi描述文件

[java注解处理器之Google Auto Service - strongmore - 博客园 (cnblogs.com)](https://www.cnblogs.com/strongmore/p/13284444.html)

依赖：

```xml
<dependency>
    <groupId>com.google.auto.service</groupId>
    <artifactId>auto-service-annotations</artifactId>
    <version>1.0</version>
</dependency>
```

编译插件：

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    <version>3.8.1</version>
    <configuration>
        <annotationProcessorPaths>
            <path>
                <groupId>com.google.auto.service</groupId>
                <artifactId>auto-service</artifactId>
                <version>1.0</version>
            </path>
        </annotationProcessorPaths>
    </configuration>
</plugin>
```

定义接口实现，使用AutoService注解

```java
@AutoService(UserService.class)
public class LocalUserService implements UserService {

  @Override
  public String userName() {
    return "local user";
  }
}
```



```java
@AutoService(UserService.class)
public class RemoteUserService implements UserService {

  @Override
  public String userName() {
    return "remote user";
  }
}
```

调用

```java
public class Client {
  public static void main(String[] args) {
    ServiceLoader<UserService> serviceLoader = ServiceLoader.load(UserService.class);
    for (UserService userService : serviceLoader) {
      System.out.println(userService.userName());
    }
  }
}
```

输出结果为:

```
local user
remote user
```

从编译之后的目录里可以看到已经生成了对应的配置文件,文件名：com.xxx.UserService，内容：

```
com.xxx.LocalUserService
com.xxx.RemoteUserService
```



# 应用--生成Processor spi 描述文件

spi 是一种服务发现的标准，对于开发中我们通常需要编写 `META-INF/services` 文件夹中定义的类。
google auto 中的autoservice 可以帮助我们生成对应的配置，很方便。

## 注解处理器模块

依赖：

```xml
<dependency>
    <groupId>com.google.auto.service</groupId>
    <artifactId>auto-service-annotations</artifactId>
    <version>1.0</version>
</dependency>
```

编译插件：

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    <version>3.8.1</version>
    <configuration>
        <annotationProcessorPaths>
            <path>
                <groupId>com.google.auto.service</groupId>
                <artifactId>auto-service</artifactId>
                <version>1.0</version>
            </path>
        </annotationProcessorPaths>
    </configuration>
</plugin>
```

注解：

```JAVA
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.SOURCE)
public @interface Builder {

}
```

注解处理器：

```java
@AutoService(Processor.class) //注意这里
@SupportedAnnotationTypes(value = {"com.gitee.potato20522.autoservice.processor.Builder"})
@SupportedSourceVersion(value = SourceVersion.RELEASE_8)
public class BuilderProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (TypeElement typeElement : annotations) {
            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(typeElement);
            Map<Boolean, List<Element>> annotatedMethods = annotatedElements
                    .stream()
                    .collect(Collectors.partitioningBy(
                            element -> (
                                    (ExecutableType) element.asType()).getParameterTypes().size() == 1
                                    && element.getSimpleName().toString().startsWith("set")));
            List<Element> setters = annotatedMethods.get(true);
            List<Element> otherMethods = annotatedMethods.get(false);
            otherMethods.forEach(element ->
                    processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                            "@Builder must be applied to a setXxx method "
                                    + "with a single argument", element));
            Map<String, String> setterMap = setters.stream().collect(Collectors.toMap(
                    setter -> setter.getSimpleName().toString(),
                    setter -> ((ExecutableType) setter.asType())
                            .getParameterTypes().get(0).toString()
            ));
            String className = ((TypeElement) setters.get(0)
                    .getEnclosingElement()).getQualifiedName().toString();
            try {
                writeBuilderFile(className, setterMap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    private void writeBuilderFile(String className, Map<String, String> setterMap) throws IOException {
        String packageName = null;
        int lastDot = className.lastIndexOf('.');
        if (lastDot > 0) {
            packageName = className.substring(0, lastDot);
        }
        String simpleClassName = className.substring(lastDot + 1);
        String builderClassName = className + "Builder";
        String builderSimpleClassName = builderClassName
                .substring(lastDot + 1);

        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(builderClassName);

        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {

            if (packageName != null) {
                out.print("package ");
                out.print(packageName);
                out.println(";");
                out.println();
            }
            out.print("public class ");
            out.print(builderSimpleClassName);
            out.println(" {");
            out.println();
            out.print("    private ");
            out.print(simpleClassName);
            out.print(" object = new ");
            out.print(simpleClassName);
            out.println("();");
            out.println();
            out.print("    public ");
            out.print(simpleClassName);
            out.println(" build() {");
            out.println("        return object;");
            out.println("    }");
            out.println();
            setterMap.forEach((methodName, argumentType) -> {
                out.print("    public ");
                out.print(builderSimpleClassName);
                out.print(" ");
                out.print(methodName);

                out.print("(");

                out.print(argumentType);
                out.println(" value) {");
                out.print("        object.");
                out.print(methodName);
                out.println("(value);");
                out.println("        return this;");
                out.println("    }");
                out.println();
            });
            out.println("}");
        }
    }
}
```

运行maven install后，编译生成了javax.annotation.processing.Processor文件

## 使用注解

再新建一个maven模块，引入之前的模块作为依赖和annotationProcessorPaths

写一个person类，并使用Builder注解：

```java
package com.gitee.potato20522.autoservice.use;

import com.gitee.potato20522.autoservice.processor.Builder;

public class Person {

    private Integer age;
    private String name;

    public Integer getAge() {
        return age;
    }

    @Builder
    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    @Builder
    public void setName(String name) {
        this.name = name;
    }
}
```

编译后，又生成了PersonBuilder：

```java
package com.gitee.potato20522.autoservice.use;

public class PersonBuilder {

    private Person object = new Person();

    public Person build() {
        return object;
    }

    public PersonBuilder setName(java.lang.String value) {
        object.setName(value);
        return this;
    }

    public PersonBuilder setAge(java.lang.Integer value) {
        object.setAge(value);
        return this;
    }

}

```

