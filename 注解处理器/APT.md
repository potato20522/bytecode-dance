# 入门概念

参考：

https://blog.csdn.net/github_35180164/article/details/52055994

https://blog.csdn.net/weixin_39861627/article/details/114097776

[自定义注解(annotation)和注解处理器(annotation processor) - 简书 (jianshu.com)](https://www.jianshu.com/p/37f0086e9d40)

https://www.cnblogs.com/intotw/p/13815793.html

[APT&JavaPoet实践 - 掘金 (juejin.cn)](https://juejin.cn/post/7001709581097238558)

[Java-JSR-269-插入式注解处理器 | Liuye Notebook (liuyehcf.github.io)](https://liuyehcf.github.io/2018/02/02/Java-JSR-269-插入式注解处理器/)

## 介绍

## 机制

![image-20220102000551547](img/APT.assets/image-20220102000551547.png)

![img](img/APT.assets/5a9ec0d8c5204c35b6f2663c63e70caetplv-k3u1fbpfcp-zoom-in-crop-mark3024000.webp)

![img](img/APT.assets/e7a9a5c8682143e09cecce8b9d3ed497tplv-k3u1fbpfcp-zoom-in-crop-mark3024000.webp)

Java的注解处理一般分为2种，最常见也是最显式化的就是Spring以及Spring Boot的注解实现了，在**运行期**容器启动时，根据注解扫描类，并加载到Spring容器中。而另一种就是本文主要介绍的注解处理，即**编译期注解处理器**，用于在编译期通过JDK提供的API，对Java文件编译前生成的Java语法树进行处理，实现想要的功能。

**APT即为Annotation Processing Tool**，是Javac的一个工具，意为编译时注解处理器，或者叫做插入式注解处理器（JSR-269：Pluggable Annotations Processing API）。一般是操作`Java`源文件，当处理完源文件后编译它们，在系统创建的过程中会自动创建一些新的源文件，这些新文件会在新的一轮中的注解处理器中接受检查，直到不再有新的源文件产生为止。这个过程中是发生在**编译期**间(`compile time`)，而非运行期间，所有`apt`产生为自定义注解性能的提高做出了大大的贡献。



**注解处理器是运行它自己的虚拟机JVM中**。是的，你没有看错，**javac**启动一个完整Java虚拟机来运行注解处理器。这对你意味着什么？你可以使用任何你在其他java应用中使用的的东西。使用guava。如果你愿意，你可以使用依赖注入工具，例如dagger或者其他你想要的类库。但是不要忘记，即使是一个很小的处理，你也要像其他Java应用一样，注意算法效率，以及设计模式。



## 插件化注解处理API

插件化注解处理(Pluggable Annotation Processing)API[JSR 269](http://jcp.org/en/jsr/detail?id=269)提供一套标准API来处理Annotations[JSR 175](http://jcp.org/en/jsr/detail?id=175),实际上JSR 269不仅仅用来处理Annotation，我觉得更强大的功能是它建立了Java 语言本身的一个模型,它把method、package、constructor、type、variable、enum、annotation等Java语言元素映射为Types和Elements，从而将Java语言的语义映射成为对象，我们可以在javax.lang.model包下面可以看到这些类。所以我们可以利用JSR 269提供的API来构建一个功能丰富的元编程(metaprogramming)环境。JSR 269用Annotation Processor在编译期间而不是运行期间处理Annotation, Annotation Processor相当于编译器的一个插件,所以称为插入式注解处理.如果Annotation Processor处理Annotation时(执行process方法)产生了新的Java代码，编译器会再调用一次Annotation Processor，如果第二次处理还有新代码产生，就会接着调用Annotation Processor，直到没有新代码产生为止。每执行一次process()方法被称为一个"round"，这样整个Annotation processing过程可以看作是一个round的序列。JSR 269主要被设计成为针对Tools或者容器的API。这个特性虽然在JavaSE 6已经存在，但是很少人知道它的存在。下一篇介绍的**Java奇技淫巧-lombok**就是使用这个特性实现编译期的代码插入的。另外，如果没有猜错，像IDEA在编写代码时候的标记语法错误的红色下划线也是通过这个特性实现的。KAPT(Annotation Processing for Kotlin)，也就是Kotlin的编译也是通过此特性的。



Pluggable Annotation Processing API的核心是Annotation Processor即注解处理器，一般需要继承抽象类`javax.annotation.processing.AbstractProcessor`。注意，与运行时注解`RetentionPolicy.RUNTIME`不同，注解处理器只会处理编译期注解，也就是**RetentionPolicy.SOURCE**的注解类型，处理的阶段位于Java代码编译期间。、



## 使用步骤

注解处理器使用步骤大概如下：

- 1、自定义一个Annotation Processor，需要继承`javax.annotation.processing.AbstractProcessor`，并重写process方法。
- 2、自定义一个注解，注解的元注解需要指定`@Retention(RetentionPolicy.SOURCE)`。
- 3、需要在声明的自定义Annotation Processor中使用`javax.annotation.processing.SupportedAnnotationTypes`指定在第2步创建的注解类型的名称(注意需要全类名，"包名.注解类型名称"，否则会不生效)。
- 4、需要在声明的自定义Annotation Processor中使用`javax.annotation.processing.SupportedSourceVersion`指定编译版本。
- 5、可选操作，可以通在声明的自定义Annotation Processor中使用`javax.annotation.processing.SupportedOptions`指定编译参数。

## AbstractProcessor

自定义处理器都需要继承AbstractProcessor。源码如下：

```java
public abstract class AbstractProcessor implements Processor {
    protected ProcessingEnvironment processingEnv;
    private boolean initialized = false;

    protected AbstractProcessor() {}

    public Set<String> getSupportedOptions() {
        SupportedOptions so = this.getClass().getAnnotation(SupportedOptions.class);
        if  (so == null)
            return Collections.emptySet();
        else
            return arrayToSet(so.value());
    }


    public Set<String> getSupportedAnnotationTypes() {
        SupportedAnnotationTypes sat = this.getClass().getAnnotation(SupportedAnnotationTypes.class);
        if  (sat == null) {
            if (isInitialized())
                processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING,
                                                         "No SupportedAnnotationTypes annotation " +
                                                         "found on " + this.getClass().getName() +
                                                         ", returning an empty set.");
            return Collections.emptySet();
        }
        else
            return arrayToSet(sat.value());
    }

    public SourceVersion getSupportedSourceVersion() {
        SupportedSourceVersion ssv = this.getClass().getAnnotation(SupportedSourceVersion.class);
        SourceVersion sv = null;
        if (ssv == null) {
            sv = SourceVersion.RELEASE_6;
            if (isInitialized())
                processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING,
                                                         "No SupportedSourceVersion annotation " +
                                                         "found on " + this.getClass().getName() +
                                                         ", returning " + sv + ".");
        } else
            sv = ssv.value();
        return sv;
    }

    public synchronized void init(ProcessingEnvironment processingEnv) {
        if (initialized)
            throw new IllegalStateException("Cannot call init more than once.");
        Objects.requireNonNull(processingEnv, "Tool provided null ProcessingEnvironment");

        this.processingEnv = processingEnv;
        initialized = true;
    }

    public abstract boolean process(Set<? extends TypeElement> annotations,
                                    RoundEnvironment roundEnv);

    public Iterable<? extends Completion> getCompletions(Element element,
                                                         AnnotationMirror annotation,
                                                         ExecutableElement member,
                                                         String userText) {
        return Collections.emptyList();
    }

    protected synchronized boolean isInitialized() {
        return initialized;
    }

    private static Set<String> arrayToSet(String[] array) {
        assert array != null;
        Set<String> set = new HashSet<String>(array.length);
        for (String s : array)
            set.add(s);
        return Collections.unmodifiableSet(set);
    }
}
```

### init()方法

init(ProcessingEnvironment processingEnv), 这个方法在整个编译期间仅仅被调用一次，作用就是初始化参数ProcessingEnvironment。

### ProcessingEnvironment

```java
public interface ProcessingEnvironment {
    Map<String, String> getOptions();
    Messager getMessager();
    Filer getFiler();
    Elements getElementUtils();
    Types getTypeUtils();
    SourceVersion getSourceVersion();
    Locale getLocale();
}
```

- `getOptions` 这里的`map`指的是在编译期间，`app`传给注解处理器的值
- `Messager`: 在注解处理器处理注解生成新的源代码过程中，用`Messager`来将一些错误信息打印到控制台上，相当于日志。
-  `Filer`: 通过这个类来创建新的文件。
-  `Elements`: 它其实是一个工具类，用来处理所有的`Element` 元素，而我们可以把生成代码的类中所有的元素都可以成为`Element` 元素，如包就是`PackageElement`, 类和接口为`TypeElement`, 变量为`VariableElement`, 方法为`ExecutableElement`
- Type: 它其实也是一个工具类，只是用来处理**TypeMirror**. 也就是一个类的**父类**。TypeMirror superClassType = currentClass.getSuperclass();

-  SourceVersion源码的版本，比如java8

### process()

**`process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment)`**这个方法是最重要的，注解处理器新生成出来的类就是在这个方法生成的。`ProcessingEnvironment`是包含了注解处理器相关的工具类和编译器配置的参数，而`RoundEnvironment`则是指在每一轮的扫描和处理源代码中获取被注解的`Element`.

process返回false代表这个轮次中代码的语法树没有改变，无需构造新的JavaCompiler实例，反之则需要改变。



为了方便后面例子的叙述，我们这里再先了解一些`api`.

- 获取被注解的`Element`

  ```java
  List<? extends Element>  elements = roundEnv.getElementsAnnotatedWith(Builder.class)
  ```

- 判断某个`Element`的类型

  ```java
  element.getKind() == ElementKind.CLASS
  ```

- 获取父类

  ```java
  TypeMirror superClassType = typeElement.getSuperclass();
  typeElement = (TypeElement) types.asElement(superClassType);
  ```

- 获取类中内部元素

  ```java
  List<? extends Element> closedElements = typeElement.getEnclosedElements()
  ```



## 查看标有注解的元素

1、创建maven模块:annotation

定义一个@Log注解

```java
package example;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface Log {

}
```

注解处理器

```java
package example;

import com.google.auto.service.AutoService;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.util.Set;

@SupportedAnnotationTypes("example.Log")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@AutoService(Processor.class)
public class LogProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (TypeElement annotation : annotations) {
            for (Element element : roundEnv.getElementsAnnotatedWith(annotation)) {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "found @Log at " + element);
            }
        }
        return true;
    }
}
```

执行maven install

2、再建立一个模块：sample，依赖上annotation模块

使用注解

```java
package example;
@Log
public class Hello {
    @Log
    public String name;

    @Log
    public void setName(String name) {
        this.name = name;
    }

    @Log
    public static void main(String[] args) {
        @Log Hello hello = new Hello();
        hello.setName("aaaa");
        System.out.println(hello.name);
    }
}
```

编译sample模块，可以看到编译时控制台上显示的日志，有我们刚刚在代码里写的：

```
[INFO] found @Log at example.Hello
[INFO] found @Log at name
[INFO] found @Log at setName(java.lang.String)
[INFO] found @Log at main(java.lang.String[])
```

奇怪，局部变量怎么没有扫描到。



下面给出几个小例子

# 命名规范检查

来源：《深入理解Java虚拟机：JVM高级特性与最佳实践》

需求：定义一个NameCheck注解处理器，用于在编译期检查Java源码中的命名规范，不规范的在编译期报出警告

新建maven项目：namecheck

## Processor

```java
import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.Set;

/**
 * 注解处理器是运行在它自己的虚拟机jvm当中的，
 * 也就是说，javac启动了一个完整的java虚拟机来运行注解处理器.....
 */
// 可以用"*"表示支持所有Annotations,包括没有注解的情况
@SupportedAnnotationTypes("*")
// 只支持JDK 1.8的Java代码
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class NameCheckProcessor extends AbstractProcessor {

    private NameChecker nameChecker;

    /**
     * 初始化名称检查插件
     */
    @Override
    public void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        nameChecker = new NameChecker(processingEnv);
    }

    /**
     * 对输入的语法树的各个节点进行进行名称检查
     */
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        System.out.println("开始注解处理器");
        if (!roundEnv.processingOver()) {
            for (Element element : roundEnv.getRootElements())
                nameChecker.checkNames(element);
        }
        return false;
    }
}
```

## NameChecker

```java
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.*;
import javax.lang.model.util.ElementScanner8;
import java.util.EnumSet;

import static javax.lang.model.element.ElementKind.*;
import static javax.lang.model.element.Modifier.*;
import static javax.tools.Diagnostic.Kind.*;

/**
 * 程序名称规范的编译器插件：<br>
 * 如果程序命名不合规范，将会输出一个编译器的WARNING信息
 */
public class NameChecker {
    /**
     * WARNING 级别的信息默认不会在编译时显示，也不会妨碍正常编译
     * MANDATORY_WARNING 级别的信息会在编译时显示
     * ERROR 则直接报错并终止编译，并显示是哪一个文件里的哪一行错了
     */
    private final Messager messager;

    NameCheckScanner nameCheckScanner = new NameCheckScanner();

    NameChecker(ProcessingEnvironment processingEnv) {
        this.messager = processingEnv.getMessager();
    }

    /**
     * 对Java程序命名进行检查，根据《Java语言规范》第三版第6.8节的要求，Java程序命名应当符合下列格式：
     *
     * <ul>
     * <li>类或接口：符合驼式命名法，首字母大写。
     * <li>方法：符合驼式命名法，首字母小写。
     * <li>字段：
     * <ul>
     * <li>类、实例变量: 符合驼式命名法，首字母小写。
     * <li>常量: 要求全部大写。
     * </ul>
     * </ul>
     */
    public void checkNames(Element element) {
        nameCheckScanner.scan(element);
    }

    /**
     * 名称检查器实现类，继承了JDK 1.8中的ElementScanner8<br>
     * 将会以Visitor模式访问抽象语法树中的元素
     */
    private class NameCheckScanner extends ElementScanner8<Void, Void> {

        /**
         * 此方法用于检查Java类
         */
        @Override
        public Void visitType(TypeElement e, Void p) {
            scan(e.getTypeParameters(), p);
            checkCamelCase(e, true);
            super.visitType(e, p);
            return null;
        }

        /**
         * 检查方法命名是否合法
         */
        @Override
        public Void visitExecutable(ExecutableElement e, Void p) {
            if (e.getKind() == METHOD) {
                Name name = e.getSimpleName();
                if (name.contentEquals(e.getEnclosingElement().getSimpleName()))
                    messager.printMessage(MANDATORY_WARNING, "一个普通方法 “" + name + "”不应当与类名重复，避免与构造函数产生混淆", e);
                checkCamelCase(e, false);
            }
            super.visitExecutable(e, p);
            return null;
        }

        /**
         * 检查变量命名是否合法
         */
        @Override
        public Void visitVariable(VariableElement e, Void p) {
            // 如果这个Variable是枚举或常量，则按大写命名检查，否则按照驼式命名法规则检查
            if (e.getKind() == ENUM_CONSTANT || e.getConstantValue() != null || heuristicallyConstant(e))
                checkAllCaps(e);
            else
                checkCamelCase(e, false);
            return null;
        }

        /**
         * 判断一个变量是否是常量
         */
        private boolean heuristicallyConstant(VariableElement e) {
            if (e.getEnclosingElement().getKind() == INTERFACE) {
                return true;
            } else {
                return e.getKind() == FIELD && e.getModifiers().containsAll(EnumSet.of(PUBLIC, STATIC, FINAL));
            }
        }

        /**
         * 检查传入的Element是否符合驼式命名法，如果不符合，则输出警告信息
         */
        private void checkCamelCase(Element e, boolean initialCaps) {
            String name = e.getSimpleName().toString();
            boolean previousUpper = false;
            boolean conventional = true;
            int firstCodePoint = name.codePointAt(0);

            if (Character.isUpperCase(firstCodePoint)) {
                previousUpper = true;
                if (!initialCaps) {
                    messager.printMessage(MANDATORY_WARNING, "名称“" + name + "”应当以小写字母开头", e);
                    return;
                }
            } else if (Character.isLowerCase(firstCodePoint)) {
                if (initialCaps) {
                    messager.printMessage(MANDATORY_WARNING, "名称“" + name + "”应当以大写字母开头", e);
                    return;
                }
            } else
                conventional = false;

            if (conventional) {
                int cp = firstCodePoint;
                for (int i = Character.charCount(cp); i < name.length(); i += Character.charCount(cp)) {
                    cp = name.codePointAt(i);
                    if (Character.isUpperCase(cp)) {
                        if (previousUpper) {
                            conventional = false;
                            break;
                        }
                        previousUpper = true;
                    } else
                        previousUpper = false;
                }
            }

            if (!conventional)
                messager.printMessage(MANDATORY_WARNING, "名称“" + name + "”应当符合驼式命名法（Camel Case Names）", e);
        }

        /**
         * 大写命名检查，要求第一个字母必须是大写的英文字母，其余部分可以是下划线或大写字母
         */
        private void checkAllCaps(Element e) {
            String name = e.getSimpleName().toString();

            boolean conventional = true;
            int firstCodePoint = name.codePointAt(0);

            if (!Character.isUpperCase(firstCodePoint))
                conventional = false;
            else {
                boolean previousUnderscore = false;
                int cp = firstCodePoint;
                for (int i = Character.charCount(cp); i < name.length(); i += Character.charCount(cp)) {
                    cp = name.codePointAt(i);
                    if (cp == (int) '_') {
                        if (previousUnderscore) {
                            conventional = false;
                            break;
                        }
                        previousUnderscore = true;
                    } else {
                        previousUnderscore = false;
                        if (!Character.isUpperCase(cp) && !Character.isDigit(cp)) {
                            conventional = false;
                            break;
                        }
                    }
                }
            }

            if (!conventional)
                messager.printMessage(MANDATORY_WARNING, "常量“" + name + "”应当全部以大写字母或下划线命名，并且以字母开头", e);
        }
    }
}
```

## 编写META-INF与配置编译插件

在resources目录下 新建目录：META-INF/services，在此新建文件：javax.annotation.processing.Processor，里面写上我们的Processor的类全路径名：com.potato.bytecode.namecheck.NameCheckProcessor

pom.xml里吗，配置编译插件：

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    <version>${maven-compiler-plugin.version}</version>
    <configuration>
        <source>1.8</source>
        <target>1.8</target>
        <!--不加这一句编译会报找不到processor的异常-->
        <compilerArgument>-proc:none</compilerArgument>
    </configuration>
</plugin>
```

## 打包注解处理器安装到本地maven

**执行mvn install**

## 使用注解处理器

再新建一个项目：namecheck-use

随便写一个命名不规范的类

```java
/**
 * 包含多个不规范的命名
 */
public class BADLY_NAMED_CODE {
    enum colors {
        red, blue, green;
    }

    static final int _FORTY_TWO = 66;

    public static int NOT_A_CONSTANT = _FORTY_TWO;

    protected void BADLY_NAMED_CODE2() {
        return;
    }

    public void NOTcamelCASEmethodNAME() {
        return;
    }
}
```

在pom.xml中引入注解处理器：

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    <version>${maven-compiler-plugin.version}</version>
    <configuration>
        <source>1.8</source>
        <target>1.8</target>
        <annotationProcessorPaths>
            <path>
                <groupId>com.potato.bytecode</groupId>
                <artifactId>namecheck</artifactId>
                <version>1.0-SNAPSHOT</version>
            </path>
        </annotationProcessorPaths>
    </configuration>
</plugin>
```

执行mvn编译

结果如下：

![image-20220101202821033](img/APT.assets/image-20220101202821033.png)

# 披萨工厂生成

## 需求

实现一个披萨店，这个披萨店给消费者提供两种披萨（“Margherita”和“Calzone”）以及提拉米苏甜点(Tiramisu)。



```java
public interface Meal {  
  public float getPrice();
}
 
public class MargheritaPizza implements Meal {
 
  @Override public float getPrice() {
    return 6.0f;
  }
}
 
public class CalzonePizza implements Meal {
 
  @Override public float getPrice() {
    return 8.5f;
  }
}
 
public class Tiramisu implements Meal {
 
  @Override public float getPrice() {
    return 4.5f;
  }
}
```

为了在披萨店PizzsStore下订单，消费者需要输入餐(Meal)的名字。

```java
public class PizzaStore {
 
  public Meal order(String mealName) {
 
    if (mealName == null) {
      throw new IllegalArgumentException("Name of the meal is null!");
    }
 
    if ("Margherita".equals(mealName)) {
      return new MargheritaPizza();
    }
 
    if ("Calzone".equals(mealName)) {
      return new CalzonePizza();
    }
 
    if ("Tiramisu".equals(mealName)) {
      return new Tiramisu();
    }
 
    throw new IllegalArgumentException("Unknown meal '" + mealName + "'");
  }
 
  public static void main(String[] args) throws IOException {
    PizzaStore pizzaStore = new PizzaStore();
    Meal meal = pizzaStore.order(readConsole());
    System.out.println("Bill: $" + meal.getPrice());
  }
}
```

在order()方法中,有很多的if语句，并且如果每添加一种新的披萨，都要添加一条新的if语句。但是使用注解处理和工厂模式，我们可以让注解处理器来帮我们自动生成这些if语句。如此以来，我们期望的是如下的代码：

```java
public class PizzaStore {
 
  private MealFactory factory = new MealFactory();
 
  public Meal order(String mealName) {
    return factory.create(mealName);
  }
 
  public static void main(String[] args) throws IOException {
    PizzaStore pizzaStore = new PizzaStore();
    Meal meal = pizzaStore.order(readConsole());
    System.out.println("Bill: $" + meal.getPrice());
  }
}
```

MealFactory应该是如下的样子：

```java
public class MealFactory {
 
  public Meal create(String id) {
    if (id == null) {
      throw new IllegalArgumentException("id is null!");
    }
    if ("Calzone".equals(id)) {
      return new CalzonePizza();
    }
 
    if ("Tiramisu".equals(id)) {
      return new Tiramisu();
    }
 
    if ("Margherita".equals(id)) {
      return new MargheritaPizza();
    }
 
    throw new IllegalArgumentException("Unknown id = " + id);
  }
}
```

## 定义@Factory注解

```java
@Target(ElementType.TYPE) @Retention(RetentionPolicy.CLASS)
public @interface Factory {
 
  /**
   * 工厂的名字
   */
  Class type();
 
  /**
   * 用来表示生成哪个对象的唯一id
   */
  String id();
}
```



想法是这样的：使用同样的type()注解那些属于同一个工厂的类，并且用注解的id()做一个映射，例如从"Calzone"映射到"ClzonePizza"类。应用@Factory注解到上述的Pizza类中，如下：

```java
@Factory(id = "Calzone", type = Meal.class)
public class CalzonePizza implements Meal {
    @Override 
    public float getPrice() {
        return 8.5f;
    }
}

@Factory(id = "Margherita", type = Meal.class)
public class MargheritaPizza implements Meal {
    @Override
    public float getPrice() {
        return 6.0f;
    }
}

@Factory(id = "Tiramisu", type = Meal.class)
public class Tiramisu implements Meal {
    @Override
    public float getPrice() {
        return 4.5f;
    }
}
```



## @Factory使用规则

在开始写处理器的代码之前，先规定如下一些规则：

1. 只有类可以被@Factory注解，因为接口或者抽象类并不能用new操作实例化；
2. 被@Factory注解的类，必须至少提供一个公开的默认构造器（即没有参数的构造函数）。否者我们没法实例化一个对象。
3. 被@Factory注解的类必须直接或者间接的继承于type()指定的类型；
4. 具有相同的type的注解类，将被聚合在一起生成一个工厂类。这个生成的类使用Factory后缀，例如type = Meal.class，将生成MealFactory工厂类；
5. id只能是String类型，并且在同一个type组中必须唯一。

## 引入AutoService

用来生成 META-INF/services/javax.annotation.processing.Processor文件的

```xml
<!-- https://mvnrepository.com/artifact/com.google.auto.service/auto-service -->
<dependency>
  <groupId>com.google.auto.service</groupId>
  <artifactId>auto-service</artifactId>
  <version>1.0.1</version>
</dependency>
<!-- https://mvnrepository.com/artifact/com.google.auto.service/auto-service-annotations -->
<dependency>
  <groupId>com.google.auto.service</groupId>
  <artifactId>auto-service-annotations</artifactId>
  <version>1.0.1</version>
</dependency>
```

## Processor

将通过添加代码和一段解释的方法，一步一步的引导来构建的处理器。省略号(...)表示省略那些已经讨论过的或者将在后面的步骤中讨论的代码，目的是为了让代码有更好的可读性。来看一下处理器类FactoryProcessor的骨架：

```java
@AutoService(Processor.class)
public class FactoryProcessor extends AbstractProcessor {
 
  private Types typeUtils;
  private Elements elementUtils;
  private Filer filer;
  private Messager messager;
  private Map<String, FactoryGroupedClasses> factoryClasses = new LinkedHashMap<String, FactoryGroupedClasses>();
 
  @Override
  public synchronized void init(ProcessingEnvironment processingEnv) {
    super.init(processingEnv);
    typeUtils = processingEnv.getTypeUtils();
    elementUtils = processingEnv.getElementUtils();
    filer = processingEnv.getFiler();
    messager = processingEnv.getMessager();
  }
 
  @Override
  public Set<String> getSupportedAnnotationTypes() {
    Set<String> annotataions = new LinkedHashSet<String>();
    annotataions.add(Factory.class.getCanonicalName());
    return annotataions;
  }
 
  @Override
  public SourceVersion getSupportedSourceVersion() {
    return SourceVersion.latestSupported();
  }
 
  @Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
    ...
  }
}
```

### Elements和TypeMirrors

init这个初始化方法中，获取了如下的几个类：

- **Elements**：一个用来处理Element的工具类（后面将做详细说明）；
- **Types**：一个用来处理TypeMirror的工具类（后面将做详细说明）；
- **Filer**：正如这个名字所示，使用Filer可以创建文件。

在注解处理过程中，会扫描所有的Java源文件。源代码的每一个部分都是一个特定类型的Element。换句话说：Element代表程序的元素，例如包、类或者方法。每个Element代表一个静态的、语言级别的构件。在下面的例子中，通过注释来说明这个：

```java
package com.example;    // PackageElement

public class Foo {        // TypeElement

    private int a;      // VariableElement
    private Foo other;  // VariableElement

    public Foo () {}    // ExecuteableElement

    public void setA (  // ExecuteableElement
        int newA   // TypeElement
    ) {}
}
```

假如有一个代表public class Foo类的TypeElement元素，编译此类的TypeElement元素：

```java
TypeElement fooClass = ... ;  
for (Element e : fooClass.getEnclosedElements()){ // iterate over children  
    Element parent = e.getEnclosingElement();  // parent == fooClass
}
```



**Element**代表的是源代码。TypeElement代表的是源代码中的类型元素，例如类。然而，TypeElement并不包含类本身的信息。可以从TypeElement中获取类的名字，但是获取不到类的信息，例如它的父类。这种信息需要通过**TypeMirror(父类)**获取。可以通过调用elements.asType()获取元素的TypeMirror。



### 搜索@Factory注解

实现process()方法。首先，从搜索被注解了@Factory的类开始：

```java
@AutoService(Processor.class)
public class FactoryProcessor extends AbstractProcessor {
 
  private Types typeUtils;
  private Elements elementUtils;
  private Filer filer;
  private Messager messager;
  private Map<String, FactoryGroupedClasses> factoryClasses = new LinkedHashMap<String, FactoryGroupedClasses>();
    ...
 
  @Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
 
    // 遍历所有被注解了@Factory的元素
    for (Element annotatedElement : roundEnv.getElementsAnnotatedWith(Factory.class)) {
          ...
    }
  }
 ...
}
```

roundEnv.getElementsAnnotatedWith(Factory.class))返回所有被注解了@Factory的元素的列表，这里返回的Element可能是类，也可能是方法、变量等，接下来要检查Element是否是类。

```java
@Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
 
    for (Element annotatedElement : roundEnv.getElementsAnnotatedWith(Factory.class)) {
 
      // 检查被注解为@Factory的元素是否是一个类
      if (annotatedElement.getKind() != ElementKind.CLASS) {
            ...
      }
   }
   ...
}
```

为什么不用if(!(annotatedElement instanceof TypeElement))来判断Element是否是类呢，因为接口（interface）类型也是TypeElement，所以得用配合TypeMirror使用EmentKind或者TypeKind



### 异常处理

在init()中，获得了一个Messager对象的引用。Messager提供给注解处理器一个报告错误、警告以及提示信息的途径。它不是注解处理器开发者的日志工具，而是用来写一些信息给使用此注解器的第三方开发者的。在官方文档中描述了消息的不同级别。非常重要的是Kind.ERROR，因为这种类型的信息用来表示我们的注解处理器处理失败了。很有可能是第三方开发者错误的使用了@Factory注解（例如，给接口使用了@Factory注解）。这个概念和传统的Java应用有点不一样，在传统Java应用中可能就抛出一个异常Exception。**如果在process()中抛出一个异常，那么运行注解处理器的JVM将会崩溃（**就像其他Java应用一样），使用注解处理器FactoryProcessor第三方开发者将会从javac中得到非常难懂的出错信息，因为它包含FactoryProcessor的堆栈跟踪（Stacktace）信息。因此，注解处理器就有一个Messager类，它能够打印非常优美的错误信息。除此之外，还可以连接到出错的元素。在像IntelliJ这种现代的IDE（集成开发环境）中，第三方开发者可以直接点击错误信息，IDE将会直接跳转到第三方开发者项目的出错的源文件的相应的行。

重新回到process()方法的实现。如果遇到一个非类类型被注解@Factory，发出一个出错信息：

```java
public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
 
    for (Element annotatedElement : roundEnv.getElementsAnnotatedWith(Factory.class)) {
 
      // 检查被注解为@Factory的元素是否是一个类
      if (annotatedElement.getKind() != ElementKind.CLASS) {
        error(annotatedElement, "Only classes can be annotated with @%s",
            Factory.class.getSimpleName());
        return true; // 退出处理
      }
      ...
    }
 
private void error(Element e, String msg, Object... args) {  
    messager.printMessage(
        Diagnostic.Kind.ERROR,
        String.format(msg, args),
        e);
  }
 
}
```

### 数据模型

在继续检查被注解@Fractory的类是否满足上面说的5条规则之前，将介绍一个让我们更方便继续处理的数据结构。想要把一些信息存为对象，在FactoryAnnotatedClass中，保存被注解类的数据，比如合法的类的名字，以及@Factory注解本身的一些信息。所以，保存TypeElement和处理过的@Factory注解：

```java
public class FactoryAnnotatedClass {
 
  private TypeElement annotatedClassElement;
  private String qualifiedSuperClassName;
  private String simpleTypeName;
  private String id;
 
  public FactoryAnnotatedClass(TypeElement classElement) throws IllegalArgumentException {
    this.annotatedClassElement = classElement;
    Factory annotation = classElement.getAnnotation(Factory.class);
    id = annotation.id();
 
    if (StringUtils.isEmpty(id)) {
      throw new IllegalArgumentException(
          String.format("id() in @%s for class %s is null or empty! that's not allowed",
              Factory.class.getSimpleName(), classElement.getQualifiedName().toString()));
    }
 
    // Get the full QualifiedTypeName
    try {
      Class<?> clazz = annotation.type();
      qualifiedSuperClassName = clazz.getCanonicalName();
      simpleTypeName = clazz.getSimpleName();
    } catch (MirroredTypeException mte) {
      DeclaredType classTypeMirror = (DeclaredType) mte.getTypeMirror();
      TypeElement classTypeElement = (TypeElement) classTypeMirror.asElement();
      qualifiedSuperClassName = classTypeElement.getQualifiedName().toString();
      simpleTypeName = classTypeElement.getSimpleName().toString();
    }
  }
 
  /**
   * 获取在{@link Factory#id()}中指定的id
   * return the id
   */
  public String getId() {
    return id;
  }
 
  /**
   * 获取在{@link Factory#type()}指定的类型合法全名
   *
   * @return qualified name
   */
  public String getQualifiedFactoryGroupName() {
    return qualifiedSuperClassName;
  }
 
 
  /**
   * 获取在{@link Factory#type()}{@link Factory#type()}指定的类型的简单名字
   *
   * @return qualified name
   */
  public String getSimpleFactoryGroupName() {
    return simpleTypeName;
  }
 
  /**
   * 获取被@Factory注解的原始元素
   */
  public TypeElement getTypeElement() {
    return annotatedClassElement;
  }
}
```

代码很多，但是最重要的部分是在构造函数中:

```java
Factory annotation = classElement.getAnnotation(Factory.class);  
id = annotation.id(); // Read the id value (like "Calzone" or "Tiramisu")
 
if (StringUtils.isEmpty(id)) {  
    throw new IllegalArgumentException(
          String.format("id() in @%s for class %s is null or empty! that's not allowed",
              Factory.class.getSimpleName(), classElement.getQualifiedName().toString()));
}
```

获取@Factory注解，并且检查id是否为空？如果为空，将抛出 **IllegalArgumentException**异常。可能感到疑惑的是，前面说了不要抛出异常，而是使用Messager。这里仍然不矛盾。抛出内部的异常，将在后面看到会在process()中捕获这个异常。这样做出于一下两个原因：

1. 示意应该像普通的Java程序一样编码。抛出和捕获异常是非常好的Java编程实践；
2. 如果想要在FactoryAnnotatedClass中打印信息，需要也传入Messager对象，并且在异常处理一节中已经提到，为了打印Messager信息，必须成功停止处理器运行。如果使用Messager打印了错误信息，怎样告知process()出现了错误呢？最容易，并且最直观的方式就是抛出一个异常，然后让process()捕获之。

接下来，将获取@Fractory注解中的type成员。比较关心的是合法的全名：

```java
try {  
      Class<?> clazz = annotation.type();
      qualifiedGroupClassName = clazz.getCanonicalName();
      simpleFactoryGroupName = clazz.getSimpleName();
} catch (MirroredTypeException mte) {
      DeclaredType classTypeMirror = (DeclaredType) mte.getTypeMirror();
      TypeElement classTypeElement = (TypeElement) classTypeMirror.asElement();
      qualifiedGroupClassName = classTypeElement.getQualifiedName().toString();
      simpleFactoryGroupName = classTypeElement.getSimpleName().toString();
}
```

这里有一点小麻烦，因为这里的类型是一个java.lang.Class。这就意味着，他是一个真正的Class对象。因为注解处理是在编译Java源代码之前。需要考虑如下两种情况：

1. **这个类已经被编译**：这种情况是：如果第三方.jar包含已编译的被@Factory注解.class文件。在这种情况下，可以向try中那块代码中所示直接获取Class。
2. **这个还没有被编译**：这种情况是尝试编译被@Fractory注解的源代码。这种情况下，直接获取Class会抛出MirroredTypeException异常。幸运的是，MirroredTypeException包含一个TypeMirror，它表示未编译类。因为已经知道它必定是一个类类型（已经在前面检查过），可以直接强制转换为DeclaredType，然后读取TypeElement来获取合法的名字。

现在还需要一个数据结构FactoryGroupedClasses，它将简单的组合所有的FactoryAnnotatedClasses到一起。



```java
public class FactoryGroupedClasses {
 
  private String qualifiedClassName;
 
  private Map<String, FactoryAnnotatedClass> itemsMap =
      new LinkedHashMap<String, FactoryAnnotatedClass>();
 
  public FactoryGroupedClasses(String qualifiedClassName) {
    this.qualifiedClassName = qualifiedClassName;
  }
 
  public void add(FactoryAnnotatedClass toInsert) throws IdAlreadyUsedException {
 
    FactoryAnnotatedClass existing = itemsMap.get(toInsert.getId());
    if (existing != null) {
      throw new IdAlreadyUsedException(existing);
    }
 
    itemsMap.put(toInsert.getId(), toInsert);
  }
 
  public void generateCode(Elements elementUtils, Filer filer) throws IOException {
    ...
  }
}
```

这是一个基本的Map<String, FactoryAnnotatedClass>，这个映射表用来映射@Factory.id()到FactoryAnnotatedClass。选择Map这个数据类型，是因为要确保每个id是唯一的，可以很容易通过map查找实现。generateCode()方法将被用来生成工厂类代码（将在后面讨论）。

### 匹配标准

继续实现process()方法,接下来想要检查被注解的类必须有只要一个公开的构造函数，不是抽象类，继承于特定的类型，以及是一个公开类：

```java
public class FactoryProcessor extends AbstractProcessor {
 
  @Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
 
    for (Element annotatedElement : roundEnv.getElementsAnnotatedWith(Factory.class)) {
 
      ...
 
      // 因为我们已经知道它是ElementKind.CLASS类型，所以可以直接强制转换
      TypeElement typeElement = (TypeElement) annotatedElement;
 
      try {
        FactoryAnnotatedClass annotatedClass =
            new FactoryAnnotatedClass(typeElement); // throws IllegalArgumentException
 
        if (!isValidClass(annotatedClass)) {
          return true; // 已经打印了错误信息，退出处理过程
         }
       } catch (IllegalArgumentException e) {
        // @Factory.id()为空
        error(typeElement, e.getMessage());
        return true;
       }
          ...
   }
 
 private boolean isValidClass(FactoryAnnotatedClass item) {
 
    // 转换为TypeElement, 含有更多特定的方法
    TypeElement classElement = item.getTypeElement();
 
    if (!classElement.getModifiers().contains(Modifier.PUBLIC)) {
      error(classElement, "The class %s is not public.",
          classElement.getQualifiedName().toString());
      return false;
    }
 
    // 检查是否是一个抽象类
    if (classElement.getModifiers().contains(Modifier.ABSTRACT)) {
      error(classElement, "The class %s is abstract. You can't annotate abstract classes with @%",
          classElement.getQualifiedName().toString(), Factory.class.getSimpleName());
      return false;
    }
 
    // 检查继承关系: 必须是@Factory.type()指定的类型子类
    TypeElement superClassElement =
        elementUtils.getTypeElement(item.getQualifiedFactoryGroupName());
    if (superClassElement.getKind() == ElementKind.INTERFACE) {
      // 检查接口是否实现了                                       if(!classElement.getInterfaces().contains(superClassElement.asType())) {
        error(classElement, "The class %s annotated with @%s must implement the interface %s",
            classElement.getQualifiedName().toString(), Factory.class.getSimpleName(),
            item.getQualifiedFactoryGroupName());
        return false;
      }
    } else {
      // 检查子类
      TypeElement currentClass = classElement;
      while (true) {
        TypeMirror superClassType = currentClass.getSuperclass();
 
        if (superClassType.getKind() == TypeKind.NONE) {
          // 到达了基本类型(java.lang.Object), 所以退出
          error(classElement, "The class %s annotated with @%s must inherit from %s",
              classElement.getQualifiedName().toString(), Factory.class.getSimpleName(),
              item.getQualifiedFactoryGroupName());
          return false;
        }
 
        if (superClassType.toString().equals(item.getQualifiedFactoryGroupName())) {
          // 找到了要求的父类
          break;
        }
 
        // 在继承树上继续向上搜寻
        currentClass = (TypeElement) typeUtils.asElement(superClassType);
      }
    }
 
    // 检查是否提供了默认公开构造函数
    for (Element enclosed : classElement.getEnclosedElements()) {
      if (enclosed.getKind() == ElementKind.CONSTRUCTOR) {
        ExecutableElement constructorElement = (ExecutableElement) enclosed;
        if (constructorElement.getParameters().size() == 0 && constructorElement.getModifiers()
            .contains(Modifier.PUBLIC)) {
          // 找到了默认构造函数
          return true;
        }
      }
    }
 
    // 没有找到默认构造函数
    error(classElement, "The class %s must provide an public empty default constructor",
        classElement.getQualifiedName().toString());
    return false;
  }
}
```

这里添加了 isValidClass()方法，来检查是否所有的规则都被满足了：

- **必须是公开类**：classElement.getModifiers().contains(Modifier.PUBLIC)
- **必须是非抽象类**：classElement.getModifiers().contains(Modifier.ABSTRACT)
- **必须是@Factoy.type()指定的类型的子类或者接口的实现**：首先使用elementUtils.getTypeElement(item.getQualifiedFactoryGroupName())创建一个传入的Class(@Factoy.type())的元素。可以仅仅通过已知的合法类名来直接创建TypeElement（使用TypeMirror）。接下来检查它是一个接口还是一个类：superClassElement.getKind() == ElementKind.INTERFACE。所以这里有两种情况：如果是接口，就判断classElement.getInterfaces().contains(superClassElement.asType())；如果是类，就必须使用currentClass.getSuperclass()扫描继承层级。注意，整个检查也可以使用typeUtils.isSubtype()来实现
- **类必须有一个公开的默认构造函数**：遍历所有的闭元素classElement.getEnclosedElements()，然后检查ElementKind.CONSTRUCTOR、Modifier.PUBLIC以及constructorElement.getParameters().size() == 0

如果所有这些条件都满足，isValidClass()返回true，否者就打印错误信息，并且返回false。

### 组合被注解的类

一旦检查isValidClass()成功，将添加FactoryAnnotatedClass到对应的FactoryGroupedClasses中，如下：

```java
public class FactoryProcessor extends AbstractProcessor {
 
   private Map<String, FactoryGroupedClasses> factoryClasses =
      new LinkedHashMap<String, FactoryGroupedClasses>();
 
 
 @Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
      ...
      try {
        FactoryAnnotatedClass annotatedClass =
            new FactoryAnnotatedClass(typeElement); // throws IllegalArgumentException
 
          if (!isValidClass(annotatedClass)) {
          return true; // 错误信息被打印，退出处理流程
        }
 
        // 所有检查都没有问题，所以可以添加了
        FactoryGroupedClasses factoryClass =
        factoryClasses.get(annotatedClass.getQualifiedFactoryGroupName());
        if (factoryClass == null) {
          String qualifiedGroupName = annotatedClass.getQualifiedFactoryGroupName();
          factoryClass = new FactoryGroupedClasses(qualifiedGroupName);
          factoryClasses.put(qualifiedGroupName, factoryClass);
        }
 
        // 如果和其他的@Factory标注的类的id相同冲突，
        // 抛出IdAlreadyUsedException异常
        factoryClass.add(annotatedClass);
      } catch (IllegalArgumentException e) {
        // @Factory.id()为空 --> 打印错误信息
        error(typeElement, e.getMessage());
        return true;
      } catch (IdAlreadyUsedException e) {
        FactoryAnnotatedClass existing = e.getExisting();
        // 已经存在
        error(annotatedElement,
            "Conflict: The class %s is annotated with @%s with id ='%s' but %s already uses the same id",
            typeElement.getQualifiedName().toString(), Factory.class.getSimpleName(),
            existing.getTypeElement().getQualifiedName().toString());
        return true;
      }
    }
    ...
}
```

### 代码生成

已经收集了所有的被@Factory注解的类保存到为FactoryAnnotatedClass，并且组合到了FactoryGroupedClasses。现在为每个工厂生成Java文件：

```java
@Override
public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {  
    ...
  try {
        for (FactoryGroupedClasses factoryClass : factoryClasses.values()) {
          factoryClass.generateCode(elementUtils, filer);
        }
    } catch (IOException e) {
        error(null, e.getMessage());
    }
 
    return true;
}
```



写Java文件，和写其他普通文件没有什么两样。使用Filer提供的Writer对象，可以连接字符串来写生成的Java代码。幸运的是，Square公司（因为提供了许多非常优秀的开源项目二非常有名）提供了 JavaWriter，这是一个高级的生成Java代码的库：

```xml
<dependency>
    <groupId>com.squareup</groupId>
    <artifactId>javawriter</artifactId>
    <version>2.5.1</version>
</dependency>
```



```JAVA
public class FactoryGroupedClasses {
 
  /**
   * 将被添加到生成的工厂类的名字中
   */
  private static final String SUFFIX = "Factory";
 
  private String qualifiedClassName;
 
  private Map<String, FactoryAnnotatedClass> itemsMap =
      new LinkedHashMap<String, FactoryAnnotatedClass>();
    ...
 
  public void generateCode(Elements elementUtils, Filer filer) throws IOException {
 
    TypeElement superClassName = elementUtils.getTypeElement(qualifiedClassName);
    String factoryClassName = superClassName.getSimpleName() + SUFFIX;
 
    JavaFileObject jfo = filer.createSourceFile(qualifiedClassName + SUFFIX);
    Writer writer = jfo.openWriter();
    JavaWriter jw = new JavaWriter(writer);
 
    // 写包名
    PackageElement pkg = elementUtils.getPackageOf(superClassName);
    if (!pkg.isUnnamed()) {
      jw.emitPackage(pkg.getQualifiedName().toString());
      jw.emitEmptyLine();
    } else {
      jw.emitPackage("");
    }
 
    jw.beginType(factoryClassName, "class", EnumSet.of(Modifier.PUBLIC));
    jw.emitEmptyLine();
    jw.beginMethod(qualifiedClassName, "create", EnumSet.of(Modifier.PUBLIC), "String", "id");
 
    jw.beginControlFlow("if (id == null)");
    jw.emitStatement("throw new IllegalArgumentException(\"id is null!\")");
    jw.endControlFlow();
 
    for (FactoryAnnotatedClass item : itemsMap.values()) {
      jw.beginControlFlow("if (\"%s\".equals(id))", item.getId());
      jw.emitStatement("return new %s()", item.getTypeElement().getQualifiedName().toString());
      jw.endControlFlow();
      jw.emitEmptyLine();
    }
 
    jw.emitStatement("throw new IllegalArgumentException(\"Unknown id = \" + id)");
    jw.endMethod();
    jw.endType();
    jw.close();
  }
}
```

**注意**：因为JavaWriter非常非常的流行，所以很多处理器、库、工具都依赖于JavaWriter。如果使用依赖管理工具，例如maven或者gradle，假如一个库依赖的JavaWriter的版本比其他的库新，这将会导致一些问题。所以建议直接拷贝重新打包JavaWiter到注解处理器代码中（实际它只是一个Java文件）。

**更新**：JavaWrite现在已经被[JavaPoet](https://github.com/square/javapoet)取代了。

### 处理循环

注解处理过程可能会多于一次。官方javadoc定义处理过程如下：

注解处理过程是一个有序的循环过程。在每次循环中，一个处理器可能被要求去处理那些在上一次循环中产生的源文件和类文件中的注解。第一次循环的输入是运行此工具的初始输入。这些初始输入，可以看成是虚拟的第0此的循环的输出。

一个简单的定义：一个处理循环是调用一个注解处理器的process()方法。对应到我们的工厂模式的例子中：FactoryProcessor被初始化一次（不是每次循环都会新建处理器对象），然而，如果生成了新的源文件process()能够被调用多次。听起来有点奇怪不是么？原因是这样的，这些生成的文件中也可能包含@Factory注解，它们还将会被FactoryProcessor处理。

例如我们的PizzaStore的例子中将会经过3次循环处理：



| **Round** | **Input**                                                    | **Output**       |
| --------- | ------------------------------------------------------------ | ---------------- |
| 1         | CalzonePizza.java Tiramisu.java MargheritaPizza.java Meal.java PizzaStore.java | MealFactory.java |
| 2         | MealFactory.java                                             | --- none ---     |
| 3         | --- none ---                                                 | --- none ---     |



解释处理循环还有另外一个原因。如果看一下FactoryProcessor代码就能注意到，收集数据和保存它们在一个私有的域中Map<String, FactoryGroupedClasses> factoryClasses。在第一轮中，检测到了MagheritaPizza, CalzonePizza和Tiramisu，然后生成了MealFactory.java。在第二轮中把MealFactory作为输入。因为在MealFactory中没有检测到@Factory注解，我们预期并没有错误，然而我们得到如下的信息：

```
Attempt to recreate a file for type com.hannesdorfmann.annotationprocessing101.factory.MealFactory  
```

这个问题是因为我们没有清除factoryClasses，这意味着，在第二轮的process()中，任然保存着第一轮的数据，并且会尝试生成在第一轮中已经生成的文件，从而导致这个错误的出现。在这个场景中，我们知道只有在第一轮中检查@Factory注解的类，所以可以简单的修复这个问题，如下：

```JAVA
@Override
public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {  
    try {
      for (FactoryGroupedClasses factoryClass : factoryClasses.values()) {
        factoryClass.generateCode(elementUtils, filer);
      }
 
      // 清除factoryClasses
      factoryClasses.clear();
 
    } catch (IOException e) {
      error(null, e.getMessage());
    }
    ...
    return true;
}
```

这有其他的方法来处理这个问题，例如也可以设置一个布尔值标签等。关键的点是：要记住**注解处理过程是需要经过多轮处理的，并且不能重载或者重新创建已经生成的源代码**。

## 分离处理器和注解

将@Factory注解和注解处理器分到两个不同的模块中。因为注解处理器中的代码往往很复杂，而使用注解的项目，在编译后，不需要引入注解处理器代码了，因此分开注解和注解处理很有必要。

在Android开发中，一个.dex文件中，最多只能寻址65000个方法，不分开的话，可能会超出这个限制。

- 新建maven工程：factory，里面只放注解：@Factory

- 新建maven工程：factory-processor，里面放注解处理器，依赖为：

  ```xml
  <dependency>
    <groupId>com.google.auto.service</groupId>
    <artifactId>auto-service</artifactId>
    <version>1.0.1</version>
  </dependency>
  <!-- https://mvnrepository.com/artifact/com.google.auto.service/auto-service-annotations -->
  <dependency>
    <groupId>com.google.auto.service</groupId>
    <artifactId>auto-service-annotations</artifactId>
    <version>1.0.1</version>
  </dependency>
  <!-- https://mvnrepository.com/artifact/com.squareup/javawriter -->
  <dependency>
    <groupId>com.squareup</groupId>
    <artifactId>javawriter</artifactId>
    <version>2.5.1</version>
  </dependency>
  ```

​	不需要配置：\<compilerArgument>-proc:none\</compilerArgument>

- 新建maven工程：pizza-factory-use，放pizza的类，依赖：

  ```xml
  <dependency>
      <groupId>com.potato.bytecode</groupId>
      <artifactId>factory</artifactId>
      <version>1.0-SNAPSHOT</version>
  </dependency>
  ```

  配置：

  ```xml
  <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-compiler-plugin</artifactId>
      <version>${maven-compiler-plugin.version}</version>
      <configuration>
          <source>1.8</source>
          <target>1.8</target>
          <annotationProcessorPaths>
              <path>
                  <groupId>com.potato.bytecode</groupId>
                  <artifactId>factory-processor</artifactId>
                  <version>1.0-SNAPSHOT</version>
              </path>
          </annotationProcessorPaths>
  
          <!--如果依赖中包含注解处理器，就使用下面这个-->
          <!--          <annotationProcessors>-->
          <!--            <annotationProcessor>com.potato.bytecode.processor.FactoryProcessor</annotationProcessor>-->
          <!--          </annotationProcessors>-->
      </configuration>
  </plugin>
  ```

  

## 生成的类的实例化

写一个PizzaStore

```java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PizzaStore {

  private final MealFactory factory = new MealFactory();

  public Meal order(String mealName) {
    return factory.create(mealName);
  }

  private static String readConsole() throws IOException {
    System.out.println("What do you like?");
    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
    String input = bufferRead.readLine();
    return input;
  }

  public static void main(String[] args) throws IOException {
    PizzaStore pizzaStore = new PizzaStore();
    Meal meal = pizzaStore.order(readConsole());
    System.out.println("Bill: $" + meal.getPrice());
  }
}
```

## 结果

生成了MealFactory类：

![image-20220102161812092](img/APT.assets/image-20220102161812092.png)

