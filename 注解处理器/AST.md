来源：

[抽象语法树（AST）入门 - 掘金 (juejin.cn)](https://juejin.cn/post/7030457038840791053)

[Java抽象语法树AST，JCTree 分析_lazyjam的博客-CSDN博客_ast tree](https://blog.csdn.net/u013998373/article/details/90050810)

[java注解处理器——在编译期修改语法树_A_zhenzhen的专栏-CSDN博客_注解处理器修改语法树](https://blog.csdn.net/A_zhenzhen/article/details/86065063)

[Java抽象语法树AST浅析与使用_peng425的博客-CSDN博客_java抽象语法树](https://blog.csdn.net/peng425/article/details/102814754)

[Abstract Syntax Tree 抽象语法树简介 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/26988179)

[抽象语法树简介(ZZ) - 很大很老实 - 博客园 (cnblogs.com)](https://www.cnblogs.com/aomi/p/7410091.html)

# 抽象语法树简介

抽象语法树（abstract syntax code，AST）是源代码的抽象语法结构的树状表示，树上的每个节点都表示源代码中的一种结构，这所以说是抽象的，是因为抽象语法树并不会表示出真实语法出现的每一个细节，比如说，嵌套括号被隐含在树的结构中，并没有以节点的形式呈现。**抽象语法树并不依赖于源语言的语法**，也就是说语法分析阶段所采用的上下文无文文法，因为在写文法时，经常会对文法进行等价的转换（消除左递归，回溯，二义性等），这样会给文法分析引入一些多余的成分，对后续阶段造成不利影响，甚至会使合个阶段变得混乱。因些，很多编译器经常要独立地构造语法分析树，为前端，后端建立一个清晰的接口。

抽象语法树在很多领域有广泛的应用，比如浏览器，智能编辑器，编译器。

## 抽象语法树实例

### 四则运算表达式

表达式: 1+3*(4-1)+2

抽象语法树：

<img src="img/AST.assets/9af3201eaa9847c680ef5ce7d8df2a66tplv-k3u1fbpfcp-watermark.awebp" alt="image.png" style="zoom:80%;" />

### xml

```xml
<letter>
  <address>
    <city>ShiChuang</city>
  </address>
  <people>
    <id>12478</id>
    <name>Nosic</name>
  </people>
</letter>
```

抽象语法树：

![img](img/AST.assets/515240-20170822095202574-1509582137.png)



### 程序1

```
while b != 0
{
    if a > b
        a = a-b
    else
        b = b-a
}
return a
```

抽象语法树：

![img](img/AST.assets/515240-20170822095240027-592461403.png)

### 程序2

```
sum=0
for i in range(0,100)
    sum=sum+i
end
```

抽象语法树：

![img](img/AST.assets/515240-20170822095354011-1077604816.png)





# Java语法树

在注解处理器中，时常需要操作语法树

下面对语法树的操作进行详细的说明，这里需要提到三个类：

1. **JavacTree 提供了待处理的抽象语法树**
2. **TreeMaker 封装了创建AST节点的一些方法**
3. **Names 提供了创建标识符的方法**

