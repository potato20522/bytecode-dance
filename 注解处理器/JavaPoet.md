# 资源

[JavaPoet的使用指南_余杭探险家-CSDN博客_javapoet](https://blog.csdn.net/l540675759/article/details/82931785)

[square/javapoet: A Java API for generating .java source files. (github.com)](https://github.com/square/javapoet)

[(机翻) JavaPoet的基本使用_crazy_jack-CSDN博客_javapoet](https://blog.csdn.net/crazy1235/article/details/51876192)

[JavaPoet 看这一篇就够了 - 掘金 (juejin.cn)](https://juejin.cn/post/6844903475776585741)

[网上](https://juejin.cn/post/6844903475776585741)有个很经典的图，贴在这里

![JavaPoet 看这一篇就够了](img/JavaPoet.assets/c750e87e79ba6f33dc6334afbfbdd685.pngtplv-t2oaga2asx-zoom-crop-mark3024302430241702.webp)

![img](img/JavaPoet.assets/49b9c7f4b3edda78c4e96b3dff9683a2tplv-t2oaga2asx-zoom-in-crop-mark3024000.webp)

# 快速入门

JavaPoet是利用Java API在编译器自动生成java源代码,而不是字节码

要生成的目标代码：

```java
package com.example.helloworld;

public final class HelloWorld {
  public static void main(String[] args) {
    System.out.println("Hello, JavaPoet!");
  }
}
```

JavaPoet:

```xml
<!-- https://mvnrepository.com/artifact/com.squareup/javapoet -->
<dependency>
    <groupId>com.squareup</groupId>
    <artifactId>javapoet</artifactId>
    <version>1.13.0</version>
</dependency>

```



```java
MethodSpec main = MethodSpec.methodBuilder("main")
    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
    .returns(void.class)
    .addParameter(String[].class, "args")
    .addStatement("$T.out.println($S)", System.class, "Hello, JavaPoet!")
    .build();

TypeSpec helloWorld = TypeSpec.classBuilder("HelloWorld")
    .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
    .addMethod(main)
    .build();

JavaFile javaFile = JavaFile.builder("com.example.helloworld", helloWorld)
    .build();

javaFile.writeTo(System.out);
```

运行结果

```java
package com.example.helloworld;

import java.lang.String;
import java.lang.System;

public class HelloWorld {
  public static void main(String[] args) {
    System.out.println("Hello, JavaPoet!");
  }
}

```

可以看到能够进行自动import，而且代码能自动格式化。

还能把代码放到字符串里

```java
package com.potato.bytecode;

import com.squareup.javapoet.MethodSpec;

public class StringCode {
    public static void main(String[] args) {
        MethodSpec main = MethodSpec.methodBuilder("main")
                .addCode(""
                        + "int total = 0;\n"
                        + "for (int i = 0; i < 10; i++) {\n"
                        + "  total += i;\n"
                        + "}\n")
                .build();
        System.out.println(main);
    }
}
```

运行结果：

```java
void main() {
  int total = 0;
  for (int i = 0; i < 10; i++) {
    total += i;
  }
}
```

字符串拼接很难受，还要考虑换行、转义、代码格式化。好在，JavaPoet已经考虑到这些了，可以这样写：

```java
MethodSpec main = MethodSpec.methodBuilder("main")
    .addStatement("int total = 0")
    .beginControlFlow("for (int i = 0; i < 10; i++)")
    .addStatement("total += i")
    .endControlFlow()
    .build();
```

运行结果和上面是一样的。

**因此，JavaPoet适合生成新的类文件，不适合修改已经存在的类文件**

# 常用API

## JavaPoet的常用类

1. TypeSpec————用于生成类、接口、枚举对象的类
2. MethodSpec————用于生成方法对象的类
3. ParameterSpec————用于生成参数对象的类
4. AnnotationSpec————用于生成注解对象的类
5. FieldSpec————用于配置生成成员变量的类
6. ClassName————通过包名和类名生成的对象，在JavaPoet中相当于为其指定Class
7. ParameterizedTypeName————通过MainClass和IncludeClass生成包含泛型的Class
8. JavaFile————控制生成的Java文件的输出的

## JavaPoet的常用方法

**设置修饰关键字**

```java
addModifiers(Modifier... modifiers)
```

Modifier是一个枚举对象，枚举值为修饰关键字Public、Protected、Private、Static、Final等等。
所有在JavaPoet创建的对象都必须设置修饰符(包括方法、类、接口、枚举、参数、变量)。

**设置注解对象**

```java
addAnnotation（AnnotationSpec annotationSpec）
addAnnotation（ClassName annotation）
addAnnotation(Class<?> annotation)
```

该方法即为类或方法或参数设置注解，参数即可以是AnnotationSpec，也可以是ClassName，还可以直接传递Class对象。
一般情况下，包含复杂属性的注解一般用AnnotationSpec，如果单纯添加基本注解，无其他附加属性可以直接使用ClassName或者Class即可。

**设置注释**

```java
addJavadoc（CodeBlock block）
addJavadoc(String format, Object... args)
```

在编写类、方法、成员变量时，可以通过addJavadoc来设置注释，可以直接传入String对象，或者传入CodeBlock（代码块）。

# 生成类、接口、枚举对象

在JavaPoet中生成类、接口、枚举，必须得通过TypeSpec生成，而classBuilder、interfaceBuilder、enumBuilder便是创建其关键的方法：

```java
创建类：
TypeSpec.classBuilder("类名“) 
TypeSpec.classBuilder(ClassName className)

创建接口：
TypeSpec.interfaceBuilder("接口名称")
TypeSpec.interfaceBuilder(ClassName className)

创建枚举：
TypeSpec.enumBuilder("枚举名称")
TypeSpec.enumBuilder(ClassName className)

```

**继承、实现接口**

```
继承类：
.superclass(ClassName className)

实现接口
.addSuperinterface(ClassName className)
```

**继承存在泛型的父类**

当继承父类存在泛型时，需要使用ParameterizedTypeName

```Java
ParameterizedTypeName get(ClassName rawType, TypeName... typeArguments)
```

返回的ParameterizedTypeName对象，已经被添加泛型信息

**方法**

```java
addMethod(MethodSpec methodSpec)
```

通过配置MethodSpec对象，使用addMethod方法将其添加进TypeSpec中。

**枚举**

```
addEnumConstan(String enumValue)
```

通过addEnumConstan方法添加枚举值，参数为枚举值名称。

# 生成成员变量

JavaPoet生成成员变量是通过FieldSpec的build方法生成.

```Java
builder(TypeName type, String name, Modifier... modifiers)
```

只要传入TypeName(Class)、name（名称）、Modifier（修饰符），就可以生成一个基本的成员变量。

成员变量一般来说由注解（Annotation）、修饰符（Modifier）、Javadoc(注释)、initializer(实例化)。

**注解**

```Java
addAnnotation(TypeName name) 
```

**修饰符**

```Java
addModifiers(Modifier ...modifier)
```

**注释**

```Java
addJavadoc(String format, Object... args)
```

由于上述三个方法，都在通用方法介绍过这里就不再重复介绍。

**实例化**

```Java
initializer(String format, Object... args)
```

即成员变量的实例化，例：

```Java
public Activity mActivity = new Activity;
```

而**initializer方法中的内容就是“=”后面的内容**，下面看下具体的代码实现，上面的成员变量：

```Java
  ClassName activity = ClassName.get("android.app", "Activity");
  FieldSpec spec = FieldSpec.builder(activity, "mActivity")
                .addModifiers(Modifier.PUBLIC)
                .initializer("new $T", activity)
                .build();
```

# 生成方法

分为两种，第一种是构造方法，另一种为常规的方法。

## 构造方法

```java
MethodSpec.constructorBuilder()
```

## 常规方法

```
MethodSpec.methodBuilder(String name)
```

方法的主要构成有方法参数、注解、返回值、方法体、抛出异常五种，注解可以参考通用方法addAnnotation，其他方法我们将会一一介绍：

## 返回值

```
returns(TypeName returnType)
```

设置方法的返回值，只需传入一个TypeName对象，而TypeName是ClassName，ParameterizedTypeName的基类。

## 方法体

在JavaPoet中，设置方法体内容有两个方法，分别是addCode和addStatement：

```
addCode()

addStatement()
```

这两个本质上都是设置方法体内容，但是不同的是使用addStatement()方法时，你只需要专注于该段代码的内容，至于结尾的分号和换行它都会帮你做好。
而addCode（）添加的方法体内容就是一段无格式的代码片，需要开发者自己添加其格式。

## 方法体模板

在JavaPoet中，设置方法体使用模板是比较常见的，因为addCode和addStatement方法都存在这样的一个重载:

```
addCode(String format, Object... args)

addStatement(String format, Object... args)

```

在JavaPoet中，format中存在三种特定的占位符：

### $T 类型

$T 在JavaPoet代指的是TypeName，该模板主要将Class抽象出来，用传入的TypeName指向的Class来代替。

```Java
ClassName bundle = ClassName.get("android.os", "Bundle");
addStatement("$T bundle = new $T()",bundle)
```

上述添加的代码内容为：

```Java
Bundle bundle = new Bundle();
```

### $N 名称

$N在JavaPoet中代指的是一个名称，例如调用的方法名称，变量名称，这一类存在意思的名称

```Java
addStatement("data.$N()",toString)
```

上述代码添加的内容：

```java
data.toString();
```

### $S 字符串

\$S在JavaPoet中就和String.format中%s一样,字符串的模板,将指定的字符串替换到\$S的地方，需要注意的是替换后的内容，默认自带了双引号，如果不需要双引号包裹，需要使用$L.

```
.addStatement("return $S", “name”)
```

即将"name"字符串代替到$S的位置上.

### $L 字面量

```java
private MethodSpec computeRange(String name, int from, int to, String op) {
  return MethodSpec.methodBuilder(name)
      .returns(int.class)
      .addStatement("int result = 0")
      .beginControlFlow("for (int i = $L; i < $L; i++)", from, to)//这里
      .addStatement("result = result $L i", op)//这里
      .endControlFlow()
      .addStatement("return result")
      .build();
}
```

## 抛出异常

```Java
.addException(TypeName name)
```

设置方法抛出异常,可以使用addException方法,传入指定的异常的ClassName,即可为该方法设置其抛出该异常.

## 生成方法参数

JavaPoet生成有参方法时,需要填充参数,而生成参数则需要通过ParameterSpec这个类。

```
addParameter(ParameterSpec parameterSpec)
```

#### 初始化ParameterSpec

```
ParameterSpec.builder(TypeName type, String name, Modifier... modifiers)
```

给参数设置其Class,以及参数名称,和修饰符.

通常来说参数的构成包括:参数的类型(Class)、参数的名称（name）、修饰符（modifiers）、注解（Annotation）

除了builder方法初始化类型、以及名称、修饰符之外，其余可以通过如下方法进行设置：

#### 添加修饰符

```Java
.addModifiers(Modifier modifier)
```

#### 添加注解

```Java
addAnnotation(TypeName name) 
```

添加修饰符、注解具体使用可参考通用方法。

# 生成注解

在JavaPoet创建类、成员变量、方法参数、方法，都会用到注解。

如果使用不包含属性的注解可以直接通过

```java
.addAnnotation(TypeName name)
```

直接传入TypeName或者Class进行设置。

如果使用的注解包含属性，并且不止一个时，这时候就需要生成AnnotationSpec来解决，下面简单了解下AnnotationSpec。

## 初始化AnnotationSpec

```Java
AnnotationSpec.builder(ClassName type)
```

可以发现初始化，只需传入ClassName或者Class即可。

## 设置属性

```java
addMember(String name, String format, Object... args)
```

使用addMember可以设置注解的属性值，name对应的就是属性名称，format的内容即属性体，同样方法体的格式化在这里也是适用的。

# JavaFile生成最终代码

如果上述内容你已经看完，那么恭喜你，你已经明白JavaPoet的意图，但是现在的你，还差最后一步，即如何生成代码。

JavaPoet中负责生成的类是JavaFile

```java
JavaFile.builder(String packageName, TypeSpec typeSpec)
```

JavaFile通过向build方法传入PackageName（Java文件的包名）、TypeSpec（生成的内容）生成。

## 打印结果

```java
javaFile.writeTo(System.out)
```

生成的内容会输出到控制台中

## 生成文件

```java
javaFile.writeTo（File file）
```

生成的内容会以java文件的方式，存放到你传入File文件的位置

# 万能例子

最后， 我们展示一段几乎涵盖你所常见 case 的例子 (仅为了展示 JavaPoet 用法， 生成的代码可能编译不过). 如果哪里不知道怎么生成的, 可以方便的在下面的生成代码里查找生成方法.

```java
package com.walfud.howtojavapoet;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.Comparable;
import java.lang.Exception;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.Runnable;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Clazz<T> extends String implements Serializable, Comparable<String>, Map<T, ? extends String> {
  static {
  }

  private int mInt;

  private int[] mArr;

  private File mRef;

  private T mT;

  private List<String> mParameterizedField;

  private List<? extends String> mWildcardField;

  {
  }

  public Clazz() {
  }

  @Override
  public <T> int method(String string, T t, Map<Integer, ? extends T> map) throws IOException,
      RuntimeException {
    int foo = 1;
    String bar = "a string";
    Object obj = new HashMap<Integer, ? extends T>(5);
    baz(new Runnable(String param) {
      @Override
      void run() {
      }
    });
    for (int i = 0; i < 5; i++) {
    }
    while (false) {
    }
    do {
    } while (false);
    if (false) {
    } else if (false) {
    } else {
    }
    try {
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
    }
    return 0;
  }

  class InnerClass {
  }
}
```

JavaPoet 代码:

```java
package com.walfud.howtojavapoet;


import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeVariableName;
import com.squareup.javapoet.WildcardTypeName;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.lang.model.element.Modifier;

/**
 * http://android.walfud.com/javapoet-看这一篇就够了/
 */
class HowToJavaPoetDemo {
    public static void main(String[] args) {
        TypeSpec clazz = clazz(builtinTypeField(),          // int
                               arrayTypeField(),            // int[]
                               refTypeField(),              // File
                               typeField(),                 // T
                               parameterizedTypeField(),    // List<String>
                               wildcardTypeField(),         // List<? extends String>
                               constructor(),               // 构造函数
                               method(code()));             // 普通方法
        JavaFile javaFile = JavaFile.builder("com.walfud.howtojavapoet", clazz).build();

        System.out.println(javaFile.toString());
    }

    /**
     * `public abstract class Clazz<T> extends String implements Serializable, Comparable<String>, Comparable<? extends String> {
     * ...
     * }`
     *
     * @return
     */
    public static TypeSpec clazz(FieldSpec builtinTypeField, FieldSpec arrayTypeField, FieldSpec refTypeField,
                                 FieldSpec typeField, FieldSpec parameterizedTypeField, FieldSpec wildcardTypeField,
                                 MethodSpec constructor, MethodSpec methodSpec) {
        return TypeSpec.classBuilder("Clazz")
                    // 限定符
                    .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                    // 泛型
                    .addTypeVariable(TypeVariableName.get("T"))

                    // 继承与接口
                    .superclass(String.class)
                    .addSuperinterface(Serializable.class)
                    .addSuperinterface(ParameterizedTypeName.get(Comparable.class, String.class))
                    .addSuperinterface(ParameterizedTypeName.get(ClassName.get(Map.class), 
                                                                 TypeVariableName.get("T"), 
                                                                 WildcardTypeName.subtypeOf(String.class)))

                    // 初始化块
                    .addStaticBlock(CodeBlock.builder().build())
                    .addInitializerBlock(CodeBlock.builder().build())

                    // 属性
                    .addField(builtinTypeField)
                    .addField(arrayTypeField)
                    .addField(refTypeField)
                    .addField(typeField)
                    .addField(parameterizedTypeField)
                    .addField(wildcardTypeField)

                    // 方法 （构造函数也在此定义）
                    .addMethod(constructor)
                    .addMethod(methodSpec)

                    // 内部类
                    .addType(TypeSpec.classBuilder("InnerClass").build())

                    .build();
    }

    /**
     * 内置类型
     */
    public static FieldSpec builtinTypeField() {
        // private int mInt;
        return FieldSpec.builder(int.class, "mInt", Modifier.PRIVATE).build();
    }

    /**
     * 数组类型
     */
    public static FieldSpec arrayTypeField() {
        // private int[] mArr;
        return FieldSpec.builder(int[].class, "mArr", Modifier.PRIVATE).build();
    }

    /**
     * 需要导入 import 的类型
     */
    public static FieldSpec refTypeField() {
        // private File mRef;
        return FieldSpec.builder(File.class, "mRef", Modifier.PRIVATE).build();
    }

    /**
     * 泛型
     */
    public static FieldSpec typeField() {
        // private File mT;
        return FieldSpec.builder(TypeVariableName.get("T"), "mT", Modifier.PRIVATE).build();
    }

    /**
     * 参数化类型
     */
    public static FieldSpec parameterizedTypeField() {
        // private List<String> mParameterizedField;
        return FieldSpec.builder(ParameterizedTypeName.get(List.class, String.class),
                                 "mParameterizedField",
                                 Modifier.PRIVATE)
                .build();
    }

    /**
     * 通配符参数化类型
     *
     * @return
     */
    public static FieldSpec wildcardTypeField() {
        // private List<? extends String> mWildcardField;
        return FieldSpec.builder(ParameterizedTypeName.get(ClassName.get(List.class),
                                                           WildcardTypeName.subtypeOf(String.class)),
                                 "mWildcardField",
                                 Modifier.PRIVATE)
                .build();
    }

    /**
     * 构造函数
     */
    public static MethodSpec constructor() {
        return MethodSpec.constructorBuilder()
                .addModifiers(Modifier.PUBLIC)
                .build();
    }

    /**
     * `@Override
     * public <T> Integer method(String string, T t, Map<Integer, ? extends T> map) throws IOException, RuntimeException {
     * ...
     * }`
     *
     * @param codeBlock
     * @return
     */
    public static MethodSpec method(CodeBlock codeBlock) {
        return MethodSpec.methodBuilder("method")
                .addAnnotation(Override.class)
                .addTypeVariable(TypeVariableName.get("T"))
                .addModifiers(Modifier.PUBLIC)
                .returns(int.class)
                .addParameter(String.class, "string")
                .addParameter(TypeVariableName.get("T"), "t")
                .addParameter(ParameterizedTypeName.get(ClassName.get(Map.class), 
                                                        ClassName.get(Integer.class), 
                                                        WildcardTypeName.subtypeOf(TypeVariableName.get("T"))), 
                              "map")
                .addException(IOException.class)
                .addException(RuntimeException.class)
                .addCode(codeBlock)
                .build();
    }

    /**
     * ‘method’ 方法中的具体语句
     */
    public static CodeBlock code() {
        return CodeBlock.builder()
                .addStatement("int foo = 1")
                .addStatement("$T bar = $S", String.class, "a string")

                // Object obj = new HashMap<Integer, ? extends T>(5);
                .addStatement("$T obj = new $T(5)", 
                              Object.class, ParameterizedTypeName.get(ClassName.get(HashMap.class), 
                                                                      ClassName.get(Integer.class), 
                                                                      WildcardTypeName.subtypeOf(TypeVariableName.get("T"))))

                // method(new Runnable(String param) {
                //   @Override
                //   void run() {
                //   }
                // });
                .addStatement("baz($L)", TypeSpec.anonymousClassBuilder("$T param", String.class)
                        .superclass(Runnable.class)
                        .addMethod(MethodSpec.methodBuilder("run")
                                .addAnnotation(Override.class)
                                .returns(TypeName.VOID)
                                .build())
                        .build())

                // for
                .beginControlFlow("for (int i = 0; i < 5; i++)")
                .endControlFlow()

                // while
                .beginControlFlow("while (false)")
                .endControlFlow()

                // do... while
                .beginControlFlow("do")
                .endControlFlow("while (false)")

                // if... else if... else...
                .beginControlFlow("if (false)")
                .nextControlFlow("else if (false)")
                .nextControlFlow("else")
                .endControlFlow()

                // try... catch... finally
                .beginControlFlow("try")
                .nextControlFlow("catch ($T e)", Exception.class)
                .addStatement("e.printStackTrace()")
                .nextControlFlow("finally")
                .endControlFlow()

                .addStatement("return 0")
                .build();
    }
}
```

