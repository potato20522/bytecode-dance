# bytecode-dance

#### 介绍
Java字节码之舞--字节码增强技术，编译期增强、运行期增强、字节码插桩

- AST： java抽象语法树
- APT：编译时注解处理器
- ASM框架：字节码修改，最接近底层，性能最高，但需要懂字节码知识、JVM指令
- Javassist框架： 字节码修改，使用比ASM框架简单，速度比ASM稍微慢些
- Byte-Buddy框架：字节码修改，效率高于Javassist
- java agent：在类加载前，修改类
- 。。。。。。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
