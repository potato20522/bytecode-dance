# 注解在字节码中的表示

先看看注解在字节码中的表示

字节码文件中很多地方都有一个属性表。注解作为一个属性被放在这个位置。

在字节码文件中，注解相关的属性结构细分有7种

| type                                 | descriptor                                   |
| ------------------------------------ | -------------------------------------------- |
| RuntimeVisibleAnnotations            | 运行时可见                                   |
| RuntimeVisibleAnnotations            | 运行时不可见                                 |
| RuntimeVisibleParameterAnnotations   | 标注在参数上，运行时可见的参数型注解         |
| RuntimeInvisibleParameterAnnotations | 标注在参数上，运行时不可见的参数型注解       |
| RuntimeVisibleTypeAnnotations        | 标注在例行上比如泛型，运行时可见的类型注解   |
| RuntimeInvisibleTypeAnnotations      | 标注在例行上比如泛型，运行时不可见的类型注解 |
| AnnotationDefault                    | 注解的默认值                                 |

### RuntimeVisibleAnnotations

顾名思议，就是运行期(runtime) 可见的。延伸一点，运行期可以通过函数等方式获取到的。

```
RuntimeVisibleAnnotations_attribute {  
    // 前6个字节固定格式  
    u2  attribute_name_index;   
    u4  attribute_length;  
    // 表示有几个注解  
    u2  num_annotations;  
    // 注解数组  
    annotation  annotations[num_annotations];  
}   
```



# AnnotationVisitor介绍

访问注解，包括类或接口上、方法上、属性上、注解上、形参上、局部变量上、包上的注解

注解中的属性可以看成键值对的数组，注解中的属性可以是其他注解，因此注解可能嵌套的很复杂

```java
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface MyTag {
    String value() default "";
    String name();
    int age();
}
```

## AnnotationVisitor类

### class info

AnnotationVisitor是个抽象类

```java
public abstract class AnnotationVisitor {
    //...
}
```

### fields

AnnotationVisitor类定义的字段，只有一个AnnotationVisitor

```java
public abstract class AnnotationVisitor {
  protected AnnotationVisitor av;
}
```

### constructors

构造方法

```java
public abstract class AnnotationVisitor {
  public AnnotationVisitor(final int api, final AnnotationVisitor annotationVisitor) {
    //省略校验逻辑的代码
    this.api = api;//ASM版本号
    this.av = annotationVisitor;//因为注解可以包含其他注解
  }
}
```

### methods

AnnotationVisitor定义的方法有5个：visit、visitEnum、visitAnnotation、visitArray、visitEnd

#### 调用顺序

```
 ( visit | visitEnum | visitAnnotation | visitArray )* visitEnd 
```

visitEnd是最后一个被调用

其中，涉及到一些符号，它们的含义如下：

- `[]`: 表示最多调用一次，可以不调用，但最多调用一次。
- `()`和`|`: 表示在多个方法之间，可以选择任意一个，并且多个方法之间不分前后顺序。
- `*`: 表示方法可以调用0次或多次。

#### visit

```java
public void visit(final String name, final Object value) {
    if (av != null) {
        av.visit(name, value);
    }
}
```

- name：注解中的属性名

- value：属性值

作用：访问注解中的基本类型，类型范围包括8种基本数据类型、String。属性值还可以是8种基本数据类型的数组

这里的类型还可以用asm中的Type来表示，

就如下注解中的name属性：

```java
@Target({ElementType.TYPE})
public @interface MyTag {
    String name();
}
```

#### visitEnum

```java
public void visitEnum(final String name, final String descriptor, final String value) {
    if (av != null) {
        av.visitEnum(name, descriptor, value);
    }
}
```

- name：属性名
- descriptor枚举类型的描述
- value：属性值

访问枚举类型的注解

```java
public @interface TableId {
    IdType type() default IdType.NONE;
}
```

其中IdType是个枚举类型

```java
public enum IdType {
    AUTO(0),
    NONE(1),
    INPUT(2),
    ASSIGN_ID(3),
    ASSIGN_UUID(4);

    private final int key;

    private IdType(int key) {
        this.key = key;
    }

    public int getKey() {
        return this.key;
    }
}
```

#### visitAnnotation

```java
  public AnnotationVisitor visitAnnotation(final String name, final String descriptor) {
    if (av != null) {
      return av.visitAnnotation(name, descriptor);
    }
    return null;
  }
```

- name：属性名
- descriptor：注解的描述符
- 返回值是该注解的 AnnotationVisitor

访问嵌套注解类型，也就是一个注解可能被其他注解所注解

```java
@Controller //这个
@ResponseBody //还有这个
public @interface RestController {
	@AliasFor(annotation = Controller.class)
	String value() default "";

}
```

#### visitArray

```JAVA
public AnnotationVisitor visitArray(final String name) {
    if (av != null) {
        return av.visitArray(name);
    }
    return null;
}
```

- name：属性名

返回值是该数组类型用的 AnnotationVisitor

注意：8种基本数据类型的数组可以用visit()方法

```java
public @interface GetMapping {
   @AliasFor(annotation = RequestMapping.class)
   String[] value() default {};
}
```

#### visitEnd

结束时必须调用这个方法

```java
public void visitEnd() {
    if (av != null) {
        av.visitEnd();
    }
}
```



## AnnotationVisitor类示例:给类添加注解

### 目标

```java
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface MyTag {
    String value() default "";
    String name();
    int age();
    String[] path() default {};
    int[] path2() default {};
    IdType type() default IdType.NONE;
}
```

生成如下的类：

```java
@MyTag(name="zhangsan",age = 11,path = {"aa","bb"},path2 = {1,2,3},type=IdType.AUTO)
public class HelloAnnotation {
}
```



### 编码实现

```java

import com.potato.asm.utils.FileUtils;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import static org.objectweb.asm.Opcodes.*;

public class HelloAnnotationDump {
    public static void main(String[] args) throws Exception {
        String relative_path = "sample/HelloAnnotation.class";
        String filepath = FileUtils.getFilePath(relative_path);

        // (1) 生成byte[]内容
        byte[] bytes = dump();

        // (2) 保存byte[]到文件
        FileUtils.writeBytes(filepath, bytes);
    }

    public static byte[] dump() throws Exception {

        ClassWriter classWriter = new ClassWriter(0);
        MethodVisitor methodVisitor;
        AnnotationVisitor annotationVisitor0;

        classWriter.visit(V1_8, ACC_PUBLIC | ACC_SUPER, "sample/HelloAnnotation", null, "java/lang/Object", null);

        {
            annotationVisitor0 = classWriter.visitAnnotation("Lsample/MyTag;", true);
            annotationVisitor0.visit("name", "zhangsan");
            annotationVisitor0.visit("age", 11);
            {
                AnnotationVisitor annotationVisitor1 = annotationVisitor0.visitArray("path");
                annotationVisitor1.visit(null, "aa");
                annotationVisitor1.visit(null, "bb");
                annotationVisitor1.visitEnd();
            }
            annotationVisitor0.visit("path2", new int[]{1, 2, 3});
            annotationVisitor0.visitEnum("type", "Lsample/IdType;", "AUTO");
            annotationVisitor0.visitEnd();
        }

        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            methodVisitor.visitCode();
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
            methodVisitor.visitInsn(RETURN);
            methodVisitor.visitMaxs(1, 1);
            methodVisitor.visitEnd();
        }
        classWriter.visitEnd();

        return classWriter.toByteArray();
    }
}
```

# AnnotationWriter介绍

## AnnotationWriter类

拼接注解相关字节码

### class info

`AnnotationWriter`类的父类是`AnnotationVisitor`类。需要注意的是，`AnnotationWriter`类并不带有`public`修饰，因此它的有效访问范围只局限于它所处的package当中，不能像其它的`public`类一样被外部所使用，而且时final的，不能被继承。

```java
final class AnnotationWriter extends AnnotationVisitor {
    
}
```

### fields

```java

private final SymbolTable symbolTable;

private final boolean useNamedValues;

private final ByteVector annotation;

private final int numElementValuePairsOffset;

private int numElementValuePairs;

private final AnnotationWriter previousAnnotation;

private AnnotationWriter nextAnnotation;
```



1
