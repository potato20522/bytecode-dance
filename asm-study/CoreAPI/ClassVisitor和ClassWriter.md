# ClassVisitor介绍

来源：https://lsieun.github.io/java-asm-01/class-visitor-intro.html

在ASM Core API中，最重要的三个类就是`ClassReader`、`ClassVisitor`和`ClassWriter`类。在进行Class Generation操作的时候，`ClassVisitor`和`ClassWriter`这两个类起着重要作用，而并不需要`ClassReader`类的参与。在本文当中，我们将对`ClassVisitor`类进行介绍。

![ASM里的核心类](img/ASM生成新的类.assets/asm-core-classes.png)



## ClassVisitor类

ClassVisitor抽象类定义了在读取Class字节码时会触发的事件，如类头解析完成、注解解析、字段解析、方法解析等。也就是说，ClassVisitor中的方法都起到**回调函数的作用**，是上述那些事件的回调。



### class info

第一个部分，`ClassVisitor`是一个抽象类。 由于`ClassVisitor类`是一个`abstract`类，所以不能直接使用`new`关键字创建`ClassVisitor`对象。

```java
public abstract class ClassVisitor {
}
```

同时，由于`ClassVisitor`类是一个`abstract`类，要想使用它，就必须有具体的子类来继承它。比较常见的`ClassVisitor`子类有`ClassWriter`类（Core API）和`ClassNode`类（Tree API）。

```java
public class ClassWriter extends ClassVisitor {
}
```

```java
public class ClassNode extends ClassVisitor {
}
```

三个类的关系如下：

- org.objectweb.asm.ClassVisitor
  - org.objectweb.asm.ClassWriter
  - org.objectweb.asm.tree.ClassNode

### fields

第二个部分，`ClassVisitor`类定义的字段有哪些。

```java
public abstract class ClassVisitor {
    protected final int api;
    protected ClassVisitor cv;
}
```

- `api`字段：它是一个`int`类型的数据，指出了当前使用的ASM API版本，其取值有`Opcodes.ASM4`、`Opcodes.ASM5`、`Opcodes.ASM6`、`Opcodes.ASM7`、`Opcodes.ASM8`和`Opcodes.ASM9`。我们使用的ASM版本是9.0，因此我们在给`api`字段赋值的时候，选择`Opcodes.ASM9`就可以了。
- `cv`字段：它是一个`ClassVisitor`类型的数据，它的作用是将多个`ClassVisitor`串连起来。

![img](img/ASM生成新的类.assets/class-visitor-cv-field.png)



### constructors

第三个部分，`ClassVisitor`类定义的构造方法有哪些。

```java
public abstract class ClassVisitor {
    public ClassVisitor(final int api) {
        this(api, null);
    }

    public ClassVisitor(final int api, final ClassVisitor classVisitor) {
        this.api = api;
        this.cv = classVisitor;
    }
}
```

### methods

第四个部分，`ClassVisitor`类定义的方法有哪些。在ASM当中，使用到了Visitor Pattern（访问者模式），所以`ClassVisitor`当中许多的`visitXxx()`方法。

虽然，在`ClassVisitor`类当中，有许多`visitXxx()`方法，但是，我们只需要关注这4个方法：`visit()`、`visitField()`、`visitMethod()`和`visitEnd()`。为什么只关注这4个方法呢？因为这4个方法是`ClassVisitor`类的精髓或骨架，认识了这4个方法，其它的`visitXxx()`都容易扩展；同时，我们将`visitXxx()`方法缩小为4个，也能减少我们在学习ASM过程中的认知负担。

```java
public abstract class ClassVisitor {
    public void visit(
        final int version,
        final int access,
        final String name,
        final String signature,
        final String superName,
        final String[] interfaces);
    public FieldVisitor visitField( // 访问字段
        final int access,
        final String name,
        final String descriptor,
        final String signature,
        final Object value);
    public MethodVisitor visitMethod( // 访问方法
        final int access,
        final String name,
        final String descriptor,
        final String signature,
        final String[] exceptions);
    public void visitEnd();
    // ......
}
```



在`ClassVisitor`的`visit()`方法、`visitField()`方法和`visitMethod()`方法中都带有`signature`参数。这个`signature`参数“泛型”密切相关；换句话说，如果处理的是一个带有泛型信息的类、字段或方法，那么就需要给`signature`参数提供一定的值；如果处理的类、字段或方法不带有“泛型”信息，那么将`signature`参数设置为`null`就可以了。在本次课程当中，我们不去考虑“泛型”相关的内容，所以我们都将`signature`参数设置成`null`值。

如果大家对`signature`参数感兴趣，我们可以使用之前介绍的`ASMPrint`类去打印一下某个泛型类的ASM代码。例如，`java.lang.Comparable`是一个泛型接口，我们就可以使用`ASMPrint`类来打印一下它的ASM代码，从来查看`signature`参数的值是什么。

## 方法的调用顺序

在`ClassVisitor`类当中，定义了多个`visitXxx()`方法。这些`visitXxx()`方法，遵循一定的调用顺序。这个调用顺序，是参考自`ClassVisitor`类的API文档。

visit -> visitSource -> visitModule -> visitNestHost -> visitOuterClass -> visitAnnotation -> visitTypeAnnotation -> visitAttribute -> visitNestMember -> visitPermittedSubclass -> visitInnerClass -> visitRecordComponent -> visitField -> visitMethod -> visitEnd

```
visit
[visitSource][visitModule][visitNestHost][visitPermittedSubclass][visitOuterClass]
(
 visitAnnotation |
 visitTypeAnnotation |
 visitAttribute
)*
(
 visitNestMember |
 visitInnerClass |
 visitRecordComponent |
 visitField |
 visitMethod
)* 
visitEnd
```



其中，涉及到一些符号，它们的含义如下：

- `[]`: 表示最多调用一次，可以不调用，但最多调用一次。
- `()`和`|`: 表示在多个方法之间，可以选择任意一个，并且多个方法之间不分前后顺序。
- `*`: 表示方法可以调用0次或多次。

在本次课程当中，我们只关注`ClassVisitor`类当中的`visit()`方法、`visitField()`方法、`visitMethod()`方法和`visitEnd()`方法**这4个方法**，所以上面的方法调用顺序可以简化如下：

```
visit
(
 visitField |
 visitMethod
)* 
visitEnd
```

也就是说，先调用`visit()`方法，接着调用`visitField()`方法或`visitMethod()`方法，最后调用`visitEnd()`方法。

## visitXxx()方法与ClassFile

`ClassVisitor`的`visitXxx()`方法与`ClassFile`之间存在对应关系。在`ClassVisitor`中定义的`visitXxx()`方法，并不是凭空产生的，这些方法存在的目的就是为了生成一个合法的`.class`文件，而这个`.class`文件要符合ClassFile的结构，所以这些`visitXxx()`方法与ClassFile的结构密切相关。



[Java ASM系列：（014）visitXxx方法与ClassFile_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1N54y1H7qJ)

### visit() 访问类的基本信息

主要用于类访问使用

```java
public void visit(
    final int version,//java版本
    final int access,//修饰符
    final String name,//类名
    final String signature,//泛型信息
    final String superName,//继承的父类
    final String[] interfaces);//实现的接口)
```



```
ClassFile {
    u4             magic;//class文件魔数
    u2             minor_version;//次版本号
    u2             major_version;//主版本号
    u2             constant_pool_count;//常量池容量计数
    cp_info        constant_pool[constant_pool_count-1];
    u2             access_flags;//修饰符
    u2             this_class; //当前类的全局限定名在常量池里的索引
    u2             super_class;//父类的全局限定名在常量池里的索引
    u2             interfaces_count;//实现的接口数量
    u2             interfaces[interfaces_count];//接口数组,接口的全局限定名在常量池里的索引
    u2             fields_count;//成员数量
    field_info     fields[fields_count];//成员数组
    u2             methods_count;//方法数量
    method_info    methods[methods_count];//方法数组
    u2             attributes_count;
    attribute_info attributes[attributes_count];
}
```

| ClassVisitor方法             | 参数                             | ClassFile                        |
| ---------------------------- | -------------------------------- | -------------------------------- |
| `ClassVisitor.visit()`       | `version`                        | `minor_version`和`major_version` |
| `access`                     | `access_flags`                   |                                  |
| `name`                       | `this_class`                     |                                  |
| `signature`                  | `attributes`的一部分信息         |                                  |
| `superName`                  | `super_class`                    |                                  |
| `interfaces`                 | `interfaces_count`和`interfaces` |                                  |
| `ClassVisitor.visitField()`  |                                  | `field_info`                     |
| `ClassVisitor.visitMethod()` |                                  | `method_info`                    |

### visitField()访问字段

```java
public FieldVisitor visitField( // 访问字段
    final int access,
    final String name,
    final String descriptor,
    final String signature,
    final Object value);
```

```
field_info {
    u2             access_flags;
    u2             name_index;
    u2             descriptor_index;
    u2             attributes_count;
    attribute_info attributes[attributes_count];
}
```

| ClassVisitor方法            | 参数                     | field_info     |
| --------------------------- | ------------------------ | -------------- |
| `ClassVisitor.visitField()` | `access`                 | `access_flags` |
| `name`                      | `name_index`             |                |
| `descriptor`                | `descriptor_index`       |                |
| `signature`                 | `attributes`的一部分信息 |                |
| `value`                     | `attributes`的一部分信息 |                |

### visitMethod()访问方法

```java
public MethodVisitor visitMethod( // 访问方法
    final int access,
    final String name,
    final String descriptor,
    final String signature,
    final String[] exceptions);
```

```
method_info {
    u2             access_flags;
    u2             name_index;
    u2             descriptor_index;
    u2             attributes_count;
    attribute_info attributes[attributes_count];
}
```

| ClassVisitor方法             | 参数                     | method_info    |
| ---------------------------- | ------------------------ | -------------- |
| `ClassVisitor.visitMethod()` | `access`                 | `access_flags` |
| `name`                       | `name_index`             |                |
| `descriptor`                 | `descriptor_index`       |                |
| `signature`                  | `attributes`的一部分信息 |                |
| `exceptions`                 | `attributes`的一部分信息 |                |

### visitEnd()访问结束时的方法

`visitEnd()`方法，它是这些`visitXxx()`方法当中最后一个调用的方法。

为什么`visitEnd()`方法是“最后一个调用的方法”呢？是因为在`ClassVisitor`当中，定义了多个`visitXxx()`方法，这些个`visitXxx()`方法之间要遵循一个先后调用的顺序，而`visitEnd()`方法是最后才去调用的。

等到`visitEnd()`方法调用之后，就表示说再也不去调用其它的`visitXxx()`方法了，所有的“工作”已经做完了，到了要结束的时候了。

```java
/*
 * Visits the end of the class.
 * This method, which is the last one to be called,
 * is used to inform the visitor that all the fields and methods of the class have been visited.
 */
public void visitEnd() {
    if (cv != null) {
        cv.visitEnd();
    }
}
```

## 总结

本文主要对`ClassVisitor`类进行介绍，内容总结如下：

- 第一点，介绍了`ClassVisitor`类的不同部分。我们去了解这个类不同的部分，是为了能够熟悉`ClassVisitor`这个类。
- 第二点，在`ClassVisitor`类当中，定义了许多`visitXxx()`方法，这些方法的调用要遵循一定的顺序。
- 第三点，在`ClassVisitor`类当中，定义的`visitXxx()`方法中的参数与ClassFile结构密切相关。

## ClassVisitor例子

### 访问一个类

目标类

```java
public class HelloWorld {
    private static final int age = 10;
    private int name;

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public static void main(String[] args) {
        System.out.println("hello");
    }
}
```

将ClassVisitor中的方法都给写上

```java
public class ClassPrinter extends ClassVisitor {
   
   public ClassPrinter(int api) {
      super(api);
   }
 
   public void visit(int version, int access, String name, String signature,String superName, String[] interfaces) {
       System.out.println("visit,version:"+version+",access:"+access+",name:"+name+",signature:"+signature+",superName:"+superName
             +",interfaces:"+interfaces);
   }
 
   public void visitSource(String source, String debug) {
       System.out.println("visitSource,source:"+source + ",debug:"+debug);
   }
 
   public void visitOuterClass(String owner, String name, String desc) {
       System.out.println("visitOuterClass,owner:"+owner + ",name:"+name+",desc:"+desc);
   }
   
   public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
          System.out.println("visitAnnotation,desc:"+desc + ",visible:"+visible);
      return null;
   }
 
   public void visitAttribute(Attribute attr) {
       System.out.println("visitAttribute,attr:"+attr);
   }
 
   public void visitInnerClass(String name, String outerName,
         String innerName, int access) {
       System.out.println("visitInnerClass,name:"+name+",outerName:"+outerName+",innerName:"+innerName+",access:"+access);
   }
 
   public FieldVisitor visitField(int access, String name, String desc,
         String signature, Object value) {
          System.out.println("visitField,access:"+access+",name:"+name+",desc:"+desc+",signature:"+signature
             +",value:"+value);
      return null;
   }
 
   public MethodVisitor visitMethod(int access, String name, String desc,
         String signature, String[] exceptions) {
       System.out.println("visitMethod,access:"+access+",name:"+name+",desc:"+desc+",signature:"+signature
             +",exceptions:"+exceptions);
      return null;
   }
 
   public void visitEnd() {
      System.out.println("}");
   }

}
```



开始调用：

```java
public class ClassPrinterTest {
    @Test
    void visitHelloWorld() throws IOException {
        ClassPrinter p = new ClassPrinter(ASM9);
        ClassReader cr = new ClassReader("com/potato/autocontroller/controller/HelloWorld");
        cr.accept(p, 0);
    }
}
```

运行结果：

```
visit,version:52,access:33,name:com/potato/autocontroller/controller/HelloWorld,signature:null,superName:java/lang/Object,interfaces:[Ljava.lang.String;@376b4233
visitSource,source:HelloWorld.java,debug:null
visitField,access:26,name:age,desc:I,signature:null,value:10
visitField,access:2,name:name,desc:I,signature:null,value:null
visitMethod,access:1,name:<init>,desc:()V,signature:null,exceptions:null
visitMethod,access:1,name:getName,desc:()I,signature:null,exceptions:null
visitMethod,access:1,name:setName,desc:(I)V,signature:null,exceptions:null
visitMethod,access:9,name:main,desc:([Ljava/lang/String;)V,signature:null,exceptions:null
}
```

# ClassWriter介绍

字节码生成器

## ClassWriter类

### class info

第一个部分，就是`ClassWriter`的父类是`ClassVisitor`，因此`ClassWriter`类继承了`visit()`、`visitField()`、`visitMethod()`和`visitEnd()`等方法。

```java
public class ClassWriter extends ClassVisitor {
}
```

### fields

第二个部分，就是`ClassWriter`定义的字段有哪些。

```java
public class ClassWriter extends ClassVisitor {
    private int version;
    private final SymbolTable symbolTable;

    private int accessFlags;
    private int thisClass;
    private int superClass;
    private int interfaceCount;
    private int[] interfaces;

    private FieldWriter firstField;
    private FieldWriter lastField;

    private MethodWriter firstMethod;
    private MethodWriter lastMethod;

    private Attribute firstAttribute;

    //......
}
```

这些字段与ClassFile结构密切相关：

```
ClassFile {
    u4             magic;
    u2             minor_version;
    u2             major_version;
    u2             constant_pool_count;
    cp_info        constant_pool[constant_pool_count-1];
    u2             access_flags;
    u2             this_class;
    u2             super_class;
    u2             interfaces_count;
    u2             interfaces[interfaces_count];
    u2             fields_count;
    field_info     fields[fields_count];
    u2             methods_count;
    method_info    methods[methods_count];
    u2             attributes_count;
    attribute_info attributes[attributes_count];
}
```

### constructors

第三个部分，就是`ClassWriter`定义的构造方法。

`ClassWriter`定义的构造方法有两个，这里只关注其中一个，也就是只接收一个`int`类型参数的构造方法。在使用`new`关键字创建`ClassWriter`对象时，推荐使用`COMPUTE_FRAMES`参数。

```java
public class ClassWriter extends ClassVisitor {
    /* A flag to automatically compute the maximum stack size and the maximum number of local variables of methods. */
    public static final int COMPUTE_MAXS = 1;
    /* A flag to automatically compute the stack map frames of methods from scratch. */
    public static final int COMPUTE_FRAMES = 2;

    // flags option can be used to modify the default behavior of this class.
    // Must be zero or more of COMPUTE_MAXS and COMPUTE_FRAMES.
    public ClassWriter(final int flags) {
        this(null, flags);
    }
}
```

- `COMPUTE_MAXS`: 计算max stack和max local信息。
- `COMPUTE_FRAMES`: 既计算stack map frame信息，又计算max stack和max local信息。

换句话说，`COMPUTE_FRAMES`是功能最强大的：

```
COMPUTE_FRAMES = COMPUTE_MAXS + stack map frame
```

### methods

第四个部分，就是`ClassWriter`提供了哪些方法。

#### visitXxx()方法

在`ClassWriter`这个类当中，我们仍然是只关注其中的`visit()`方法、`visitField()`方法、`visitMethod()`方法和`visitEnd()`方法。

这些`visitXxx()`方法的调用，就是在为构建ClassFile提供“原材料”的过程。

```java
public class ClassWriter extends ClassVisitor {
    public void visit(
        final int version,
        final int access,
        final String name,
        final String signature,
        final String superName,
        final String[] interfaces);
    public FieldVisitor visitField( // 访问字段
        final int access,
        final String name,
        final String descriptor,
        final String signature,
        final Object value);
    public MethodVisitor visitMethod( // 访问方法
        final int access,
        final String name,
        final String descriptor,
        final String signature,
        final String[] exceptions);
    public void visitEnd();
    // ......
}
```

#### toByteArray()方法

在`ClassWriter`类当中，提供了一个`toByteArray()`方法。这个方法的作用是将“所有的努力”（对`visitXxx()`的调用）转换成`byte[]`，而这些`byte[]`的内容就遵循ClassFile结构。

在`toByteArray()`方法的代码当中，通过三个步骤来得到`byte[]`：

- 第一步，计算`size`大小。这个`size`就是表示`byte[]`的最终的长度是多少。
- 第二步，将数据填充到`byte[]`当中。
- 第三步，将`byte[]`数据返回。

```java
public class ClassWriter extends ClassVisitor {
    public byte[] toByteArray() {
        //...
    }
}
```

## 创建ClassWriter对象

### 推荐使用COMPUTE_FRAMES

在创建`ClassWriter`对象的时候，要指定一个`flags`参数，它可以选择的值有三个：

- 第一个，可以选取的值是`0`。ASM不会自动计算max stacks和max locals，也不会自动计算stack map frames。
- 第二个，可以选取的值是`ClassWriter.COMPUTE_MAXS`。ASM会自动计算max stacks和max locals，但不会自动计算stack map frames。
- 第三个，可以选取的值是`ClassWriter.COMPUTE_FRAMES`（推荐使用）。ASM会自动计算max stacks和max locals，也会自动计算stack map frames。

| flags          | max stacks and max locals | stack map frames |
| -------------- | ------------------------- | ---------------- |
| 0              | NO                        | NO               |
| COMPUTE_MAXS   | YES                       | NO               |
| COMPUTE_FRAMES | YES                       | YES              |

创建`ClassWriter`对象，如下所示：

```java
ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
```

### 为什么推荐使用COMPUTE_FRAMES

在创建`ClassWriter`对象的时候，使用`ClassWriter.COMPUTE_FRAMES`，ASM会自动计算max stacks和max locals，也会自动计算stack map frames。

首先，来看一下max stacks和max locals。在ClassFile结构中，每一个方法都用`method_info`来表示，而方法里定义的代码则使用`Code`属性来表示，其结构如下：

```
Code_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u2 max_stack;     // 这里是max stacks
    u2 max_locals;    // 这里是max locals
    u4 code_length;
    u1 code[code_length];
    u2 exception_table_length;
    {   u2 start_pc;
        u2 end_pc;
        u2 handler_pc;
        u2 catch_type;
    } exception_table[exception_table_length];
    u2 attributes_count;
    attribute_info attributes[attributes_count];
}
```

如果我们在创建`ClassWriter(flags)`对象的时候，将`flags`参数设置为`ClassWriter.COMPUTE_MAXS`或`ClassWriter.COMPUTE_FRAMES`，那么ASM会自动帮助我们计算`Code`结构中`max_stack`和`max_locals`的值。

接着，来看一下stack map frames。在`Code`结构里，可能有多个`attributes`，其中一个可能就是`StackMapTable_attribute`。`StackMapTable_attribute`结构，就是stack map frame具体存储格式，它的主要作用是对ByteCode进行类型检查。

```
StackMapTable_attribute {
    u2              attribute_name_index;
    u4              attribute_length;
    u2              number_of_entries;
    stack_map_frame entries[number_of_entries];
}
```

如果我们在创建`ClassWriter(flags)`对象的时候，将`flags`参数设置为`ClassWriter.COMPUTE_FRAMES`，那么ASM会自动帮助我们计算`StackMapTable_attribute`的内容。

![img](img/ASM生成新的类.assets/max-stacks-max-locals-stack-map-frames.png)

我们推荐使用`ClassWriter.COMPUTE_FRAMES`。因为`ClassWriter.COMPUTE_FRAMES`这个选项，能够让ASM帮助我们自动计算max stacks、max locals和stack map frame的具体内容。

- 如果将`flags`参数的取值为`0`，那么我们就必须要提供正确的max stacks、max locals和stack map frame的值；
- 如果将`flags`参数的取值为`ClassWriter.COMPUTE_MAXS`，那么ASM会自动帮助我们计算max stacks和max locals，而我们则需要提供正确的stack map frame的值。

那么，ASM为什么会提供`0`和`ClassWriter.COMPUTE_MAXS`这两个选项呢？因为ASM在计算这些值的时候，要考虑各种各样不同的情况，所以它的算法相对来说就比较复杂，因而执行速度也会相对较慢。同时，ASM也鼓励开发者去研究更好的算法；如果开发者有更好的算法，就可以不去使用`ClassWriter.COMPUTE_FRAMES`，这样就能让程序的执行效率更高效。

但是，不得不说，要想计算max stacks、max locals和stack map frames，也不是一件容易的事情。出于方便的目的，就推荐大家使用`ClassWriter.COMPUTE_FRAMES`。在大多数情况下，`ClassWriter.COMPUTE_FRAMES`都能帮我们计算出正确的值。在少数情况下，`ClassWriter.COMPUTE_FRAMES`也可能会出错，比如说，有些代码经过混淆（obfuscate）处理，它里面的stack map frame会变更非常复杂，使用`ClassWriter.COMPUTE_FRAMES`就会出现错误的情况。针对这种少数的情况，我们可以在不改变原有stack map frame的情况下，使用`ClassWriter.COMPUTE_MAXS`，让ASM只帮助我们计算max stacks和max locals。

##  如何使用ClassWriter类

使用`ClassWriter`生成一个Class文件，可以大致分成三个步骤：

- 第一步，创建`ClassWriter`对象。
- 第二步，调用`ClassWriter`对象的`visitXxx()`方法。
- 第三步，调用`ClassWriter`对象的`toByteArray()`方法。

示例代码如下：

```java
import org.objectweb.asm.ClassWriter;

import static org.objectweb.asm.Opcodes.*;

public class HelloWorldGenerateCore {
    public static byte[] dump () throws Exception {
        // (1) 创建ClassWriter对象
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        // (2) 调用visitXxx()方法
        cw.visit();
        cw.visitField();
        cw.visitMethod();
        cw.visitEnd();       // 注意，最后要调用visitEnd()方法

        // (3) 调用toByteArray()方法
        byte[] bytes = cw.toByteArray();
        return bytes;
    }
}
```

## 总结

本文主要对`ClassWriter`类进行介绍，内容总结如下：

- 第一点，`ClassWriter`类有哪些部分的信息，以便于对`ClassWriter`类有所了解。
- 第二点，在创建`ClassWriter`对象时，推荐使用`ClassWriter.COMPUTE_FRAMES`选项。
- 第三点，如何使用ClassWriter类。
  - 第一步，创建`ClassWriter`对象。
  - 第二步，调用`ClassWriter`对象的`visitXxx()`方法。
  - 第三步，调用`ClassWriter`对象的`toByteArray()`方法。



