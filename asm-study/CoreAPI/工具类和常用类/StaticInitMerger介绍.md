# StaticInitMerger介绍

`StaticInitMerger`类的特点是，可以实现将多个`<clinit>()`方法合并到一起。

## 1. 如何合并两个类文件

**首先，什么是合并两个类文件？** 假如有两个类，一个是`sample.HelloWorld`类，另一个是`sample.GoodChild`类，我们想将`sample.GoodChild`类里面定义的字段（fields）和方法（methods）全部放到`sample.HelloWorld`类里面，这样就是将两个类合并成一个新的`sample.HelloWorld`类。

**其次，合并两个类文件，有哪些应用场景呢？** 假如`sample/HelloWorld.class`是来自于第三方的软件产品，但是，我们可能会发现它的功能有些不足，所以想对这个类进行扩展。

- 第一种情况，如果扩展的功能比较简单，那么可以直接使用`ClassVisitor`和`MethodVisitor`类可以进行Class Transformation操作。
- 第二种情况，如果扩展的功能比较复杂，例如，需要添加的方法比较多、方法实现的代码逻辑比较复杂，那么使用`ClassVisitor`和`MethodVisitor`类直接修改就会变得比较麻烦。这个时候，如果我们把想添加的功能，使用Java语言编写代码，放到一个全新的`sample.GoodChild`类，将其编译成`sample/GoodChild.class`文件；再接下来，只要我们将`sample/GoodChild.class`定义的字段（fields）和方法（methods）全部迁移到`sample/HelloWorld.class`就可以了。

**最后，合并两个类文件，需要经历哪些步骤呢？** 在这里，我们列出了四个步骤：

- 第一步，读取两个类文件。
- 第二步，将`sample.GoodChild`类重命名为`sample.HelloWorld`。在代码实现上，会用到`ClassRemapper`类。
- 第三步，合并两个类。在这个过程中，要对重复的接口（interface）和`<init>()`方法。在代码实现上，会用到`ClassNode`类（Tree API）。
- 第四步，处理重复的`<clinit>()`方法。在代码实现上，会用到`StaticInitMerger`类。

![合并两个类文件](img/StaticInitMerger介绍.assets/merge-two-classes.png)

## 2. StaticInitMerger类

### 2.1. class info

第一个部分，`StaticInitMerger`类继承自`ClassVisitor`类。

```
public class StaticInitMerger extends ClassVisitor {
}
```

### 2.2. fields

第二个部分，`StaticInitMerger`类定义的字段有哪些。

在`StaticInitMerger`类里面，定义了4个字段：

- `owner`字段，表示当前类的名字。
- `renamedClinitMethodPrefix`字段和`numClinitMethods`字段一起来确定方法的新名字。
- `mergedClinitVisitor`字段，负责生成新的`<clinit>()`方法。

```java
public class StaticInitMerger extends ClassVisitor {
    // 当前类的名字
    private String owner;
    
    // 新方法的名字
    private final String renamedClinitMethodPrefix;
    private int numClinitMethods;
    
    // 生成新方法的MethodVisitor
    private MethodVisitor mergedClinitVisitor;
}
```

### 2.3. constructors

第三个部分，`StaticInitMerger`类定义的构造方法有哪些。

```java
public class StaticInitMerger extends ClassVisitor {
    public StaticInitMerger(final String prefix, final ClassVisitor classVisitor) {
        this(Opcodes.ASM9, prefix, classVisitor);
    }

    protected StaticInitMerger(final int api, final String prefix, final ClassVisitor classVisitor) {
        super(api, classVisitor);
        this.renamedClinitMethodPrefix = prefix;
    }
}
```

### 2.4. methods

第四个部分，`StaticInitMerger`类定义的方法有哪些。

在`StaticInitMerger`类里面，定义了3个`visitXxx()`方法：

- `visit()`方法，负责将当前类的名字记录到`owner`字段
- `visitMethod()`方法，负责将原来的`<clinit>()`方法进行重新命名成`renamedClinitMethodPrefix + numClinitMethods`，并在新的`<clinit>()`方法中对`renamedClinitMethodPrefix + numClinitMethods`方法进行调用。
- `visitEnd()`方法，为新的`<clinit>()`方法添加`return`语句。

```java
public class StaticInitMerger extends ClassVisitor {
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(version, access, name, signature, superName, interfaces);
        this.owner = name;
    }

    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        MethodVisitor methodVisitor;
        if ("<clinit>".equals(name)) {
            int newAccess = Opcodes.ACC_PRIVATE + Opcodes.ACC_STATIC;
            String newName = renamedClinitMethodPrefix + numClinitMethods++;
            methodVisitor = super.visitMethod(newAccess, newName, descriptor, signature, exceptions);
            
            if (mergedClinitVisitor == null) {
                mergedClinitVisitor = super.visitMethod(newAccess, name, descriptor, null, null);
            }
            mergedClinitVisitor.visitMethodInsn(Opcodes.INVOKESTATIC, owner, newName, descriptor, false);
        } else {
            methodVisitor = super.visitMethod(access, name, descriptor, signature, exceptions);
        }
        return methodVisitor;
    }

    public void visitEnd() {
        if (mergedClinitVisitor != null) {
            mergedClinitVisitor.visitInsn(Opcodes.RETURN);
            mergedClinitVisitor.visitMaxs(0, 0);
        }
        super.visitEnd();
    }
}
```

## 3. 示例：合并两个类文件

### 3.1. 预期目标

假如有一个`HelloWorld`类，代码如下：

```java
public class HelloWorld {
    static {
        System.out.println("This is static initialization method");
    }

    private String name;
    private int age;

    public HelloWorld() {
        this("tomcat", 10);
    }

    public HelloWorld(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void test() {
        System.out.println("This is test method.");
    }

    @Override
    public String toString() {
        return String.format("HelloWorld { name='%s', age=%d }", name, age);
    }
}
```

假如有一个`GoodChild`类，代码如下：

```java
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GoodChild implements Serializable {
    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public void printDate() {
        Date now = new Date();
        String str = df.format(now);
        System.out.println(str);
    }
}
```

我们想实现的预期目标：将`HelloWorld`类和`GoodChild`类合并成一个新的`HelloWorld`类。

### 3.2. 编码实现

下面的`ClassMergeVisitor`类的作用是负责将两个类合并到一起。我们需要注意以下三点：

- 第一点，`ClassNode`、`FieldNode`和`MethodNode`都是属于ASM的Tree API部分。
- 第二点，将两个类进行合并的代码逻辑，放在了`visitEnd()`方法内。为什么要把代码逻辑放在`visitEnd()`方法内呢？因为参照`ClassVisitor`类里的`visitXxx()`方法调用的顺序，`visitField()`方法和`visitMethod()`方法正好位于`visitEnd()`方法的前面。
- 第三点，在`visitEnd()`方法的代码逻辑中，忽略掉了`<init>()`方法，这样就避免新生成的类当中包含重复的`<init>()`方法。