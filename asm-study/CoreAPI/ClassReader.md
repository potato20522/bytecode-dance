来源：https://lsieun.github.io/java-asm-01/class-reader-intro.html

# ClassReader介绍

## ClassReader类

`ClassReader`类和`ClassWriter`类，从功能角度来说，是完全相反的两个类，一个用于读取`.class`文件，另一个用于生成`.class`文件。

## 类信息

第一个部分，`ClassReader`的父类是`Object`类。与`ClassWriter`类不同的是，`ClassReader`类并没有继承自`ClassVisitor`类。

`ClassReader`类的定义如下：

```java
public class ClassReader {
}
```

`ClassWriter`类的定义如下：

```java
public class ClassWriter extends ClassVisitor {
}
```

### fields

第二个部分，`ClassReader`类定义的字段有哪些。我们选取出其中的3个字段进行介绍，即`classFileBuffer`字段、`cpInfoOffsets`字段和`header`字段。

```java
public class ClassReader {
    //第1组，真实的数据部分
    final byte[] classFileBuffer;

    //第2组，数据的索引信息
    private final int[] cpInfoOffsets;
    public final int header;
}
```

为什么选择这3个字段呢？因为这3个字段能够体现出`ClassReader`类处理`.class`文件的整体思路：

- 第1组，`classFileBuffer`字段：它里面包含的信息，就是从`.class`文件中读取出来的字节码数据。
- 第2组，`cpInfoOffsets`字段和`header`字段：它们分别标识了`classFileBuffer`中数据里包含的常量池（constant pool）和访问标识（access flag）的位置信息。

我们拿到`classFileBuffer`字段后，一个主要目的就是对它的内容进行修改，来实现一个新的功能。它处理的大体思路是这样的：

```
.class文件 --> ClassReader --> byte[] --> 经过各种转换 --> ClassWriter --> byte[] --> .class文件
```

- 第一，从一个`.class`文件（例如`HelloWorld.class`）开始，它可能存储于磁盘的某个位置；
- 第二，使用`ClassReader`类将这个`.class`文件的内容读取出来，其实这些内容（`byte[]`）就是`ClassReader`对象中的`classFileBuffer`字段的内容；
- 第三，为了增加某些功能，就对这些原始内容（`byte[]`）进行转换；
- 第四，等各种转换都完成之后，再交给`ClassWriter`类处理，调用它的`toByteArray()`方法，从而得到新的内容（`byte[]`）；
- 第五，将新生成的内容（`byte[]`）存储到一个具体的`.class`文件中，那么这个新的`.class`文件就具备了一些新的功能。

### constructors

第三个部分，`ClassReader`类定义的构造方法有哪些。在`ClassReader`类当中定义了5个构造方法。但是，从本质上来说，这5个构造方法本质上是同一个构造方法的不同表现形式。其中，最常用的构造方法有两个：

- 第一个是`ClassReader cr = new ClassReader("sample.HelloWorld");`
- 第二个是`ClassReader cr = new ClassReader(bytes);`

```java
public class ClassReader {

    public ClassReader(final String className) throws IOException { // 第一个构造方法（常用）
        this(
            readStream(ClassLoader.getSystemResourceAsStream(className.replace('.', '/') + ".class"), true)
        );
    }

    public ClassReader(final byte[] classFile) { // 第二个构造方法（常用）
        this(classFile, 0, classFile.length);
    }

    public ClassReader(final byte[] classFileBuffer, final int classFileOffset, final int classFileLength) {
        this(classFileBuffer, classFileOffset, true);
    }

    ClassReader( // 这是最根本、最本质的构造方法
        final byte[] classFileBuffer,
        final int classFileOffset,
        final boolean checkClassVersion) {
        // ......
    }

    private static byte[] readStream(final InputStream inputStream, final boolean close) throws IOException {
        if (inputStream == null) {
            throw new IOException("Class not found");
        }
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            byte[] data = new byte[INPUT_STREAM_DATA_CHUNK_SIZE];
            int bytesRead;
            while ((bytesRead = inputStream.read(data, 0, data.length)) != -1) {
                outputStream.write(data, 0, bytesRead);
            }
            outputStream.flush();
            return outputStream.toByteArray();
        } finally {
            if (close) {
                inputStream.close();
            }
        }
    }
}
```

所有构造方法，本质上都执行下面的逻辑：

```java
public class ClassReader {
    ClassReader(final byte[] classFileBuffer, final int classFileOffset, final boolean checkClassVersion) {
        this.classFileBuffer = classFileBuffer;

        // Check the class' major_version.检查魔数和版本号
        // This field is after the magic and minor_version fields, which use 4 and 2 bytes respectively.
        if (checkClassVersion && readShort(classFileOffset + 6) > Opcodes.V16) {
            throw new IllegalArgumentException("Unsupported class file major version " + readShort(classFileOffset + 6));
        }

        // Create the constant pool arrays.创建常量池数组
        // The constant_pool_count field is after the magic, minor_version and major_version fields,
        // which use 4, 2 and 2 bytes respectively.
        int constantPoolCount = readUnsignedShort(classFileOffset + 8);
        cpInfoOffsets = new int[constantPoolCount];

        // Compute the offset of each constant pool entry,
        // as well as a conservative estimate of the maximum length of the constant pool strings.
        // The first constant pool entry is after the magic, minor_version, major_version and constant_pool_count fields,
        // which use 4, 2, 2 and 2 bytes respectively.
        int currentCpInfoIndex = 1;
        int currentCpInfoOffset = classFileOffset + 10;

        // The offset of the other entries depend on the total size of all the previous entries.
        while (currentCpInfoIndex < constantPoolCount) {
            cpInfoOffsets[currentCpInfoIndex++] = currentCpInfoOffset + 1;
            int cpInfoSize;
            switch (classFileBuffer[currentCpInfoOffset]) {
                case Symbol.CONSTANT_FIELDREF_TAG:
                case Symbol.CONSTANT_METHODREF_TAG:
                case Symbol.CONSTANT_INTERFACE_METHODREF_TAG:
                case Symbol.CONSTANT_INTEGER_TAG:
                case Symbol.CONSTANT_FLOAT_TAG:
                case Symbol.CONSTANT_NAME_AND_TYPE_TAG:
                    cpInfoSize = 5;
                    break;
                case Symbol.CONSTANT_DYNAMIC_TAG:
                    cpInfoSize = 5;
                    break;
                case Symbol.CONSTANT_INVOKE_DYNAMIC_TAG:
                    cpInfoSize = 5;
                    break;
                case Symbol.CONSTANT_LONG_TAG:
                case Symbol.CONSTANT_DOUBLE_TAG:
                    cpInfoSize = 9;
                    currentCpInfoIndex++;
                    break;
                case Symbol.CONSTANT_UTF8_TAG:
                    cpInfoSize = 3 + readUnsignedShort(currentCpInfoOffset + 1);
                    break;
                case Symbol.CONSTANT_METHOD_HANDLE_TAG:
                    cpInfoSize = 4;
                    break;
                case Symbol.CONSTANT_CLASS_TAG:
                case Symbol.CONSTANT_STRING_TAG:
                case Symbol.CONSTANT_METHOD_TYPE_TAG:
                case Symbol.CONSTANT_PACKAGE_TAG:
                case Symbol.CONSTANT_MODULE_TAG:
                    cpInfoSize = 3;
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            currentCpInfoOffset += cpInfoSize;
        }

        // The Classfile's access_flags field is just after the last constant pool entry.
        header = currentCpInfoOffset;
    }
}
```

上面的代码，要结合ClassFile的结构进行理解：

```
ClassFile {
    u4             magic;
    u2             minor_version;
    u2             major_version;
    u2             constant_pool_count;
    cp_info        constant_pool[constant_pool_count-1];
    u2             access_flags;
    u2             this_class;
    u2             super_class;
    u2             interfaces_count;
    u2             interfaces[interfaces_count];
    u2             fields_count;
    field_info     fields[fields_count];
    u2             methods_count;
    method_info    methods[methods_count];
    u2             attributes_count;
    attribute_info attributes[attributes_count];
}
```

### methods

第四个部分，`ClassReader`类定义的方法有哪些。

#### getXxx()方法

这里介绍的几个`getXxx()`方法，都是在`header`字段的基础上获得的：

```java
public class ClassReader {
    public int getAccess() {
        return readUnsignedShort(header);
    }

    public String getClassName() {
        // this_class is just after the access_flags field (using 2 bytes).
        return readClass(header + 2, new char[maxStringLength]);
    }

    public String getSuperName() {
        // super_class is after the access_flags and this_class fields (2 bytes each).
        return readClass(header + 4, new char[maxStringLength]);
    }

    public String[] getInterfaces() {
        // interfaces_count is after the access_flags, this_class and super_class fields (2 bytes each).
        int currentOffset = header + 6;
        int interfacesCount = readUnsignedShort(currentOffset);
        String[] interfaces = new String[interfacesCount];
        if (interfacesCount > 0) {
            char[] charBuffer = new char[maxStringLength];
            for (int i = 0; i < interfacesCount; ++i) {
                currentOffset += 2;
                interfaces[i] = readClass(currentOffset, charBuffer);
            }
        }
        return interfaces;
    }
}
```

同样，上面的几个`getXxx()`方法也需要参考ClassFile结构来理解：

```
ClassFile {
    u4             magic;
    u2             minor_version;
    u2             major_version;
    u2             constant_pool_count;
    cp_info        constant_pool[constant_pool_count-1];
    u2             access_flags;
    u2             this_class;
    u2             super_class;
    u2             interfaces_count;
    u2             interfaces[interfaces_count];
    u2             fields_count;
    field_info     fields[fields_count];
    u2             methods_count;
    method_info    methods[methods_count];
    u2             attributes_count;
    attribute_info attributes[attributes_count];
}
```

假如，有如下一个类：

```java
import java.io.Serializable;

public class HelloWorld extends Exception implements Serializable, Cloneable {

}
```

我们可以使用`ClassReader`类中的`getXxx()`方法来获取相应的信息：

```java
import com.potato.asm.utils.FileUtils;
import org.objectweb.asm.ClassReader;

import java.util.Arrays;

public class HelloWorldRun {
    public static void main(String[] args) throws Exception {
        String relative_path = "sample/HelloWorld.class";
        String filepath = FileUtils.getFilePath(relative_path);
        byte[] bytes = FileUtils.readBytes(filepath);

        //（1）构建ClassReader
        ClassReader cr = new ClassReader(bytes);

        // (2) 调用getXxx()方法
        int access = cr.getAccess();
        System.out.println("access: " + access);

        String className = cr.getClassName();
        System.out.println("className: " + className);

        String superName = cr.getSuperName();
        System.out.println("superName: " + superName);

        String[] interfaces = cr.getInterfaces();
        System.out.println("interfaces: " + Arrays.toString(interfaces));
    }
}
```

输出结果：

```
access: 33
className: sample/HelloWorld
superName: java/lang/Exception
interfaces: [java/io/Serializable, java/lang/Cloneable]
```

#### accept()方法

在`ClassReader`类当中，有一个`accept()`方法，这个方法接收一个`ClassVisitor`类型的参数，因此`accept()`方法是将`ClassReader`和`ClassVisitor`进行连接的“桥梁”。`accept()`方法的代码逻辑就是按照一定的顺序来调用`ClassVisitor`当中的`visitXxx()`方法。

```java
public class ClassReader {
    // A flag to skip the Code attributes.
    public static final int SKIP_CODE = 1;

    // A flag to skip the SourceFile, SourceDebugExtension,
    // LocalVariableTable, LocalVariableTypeTable,
    // LineNumberTable and MethodParameters attributes.
    public static final int SKIP_DEBUG = 2;

    // A flag to skip the StackMap and StackMapTable attributes.
    public static final int SKIP_FRAMES = 4;

    // A flag to expand the stack map frames.
    public static final int EXPAND_FRAMES = 8;


    public void accept(final ClassVisitor classVisitor, final int parsingOptions) {
        accept(classVisitor, new Attribute[0], parsingOptions);
    }

    public void accept(
        final ClassVisitor classVisitor,
        final Attribute[] attributePrototypes,
        final int parsingOptions) {
        Context context = new Context();
        context.attributePrototypes = attributePrototypes;
        context.parsingOptions = parsingOptions;
        context.charBuffer = new char[maxStringLength];

        // Read the access_flags, this_class, super_class, interface_count and interfaces fields.
        char[] charBuffer = context.charBuffer;
        int currentOffset = header;
        int accessFlags = readUnsignedShort(currentOffset);
        String thisClass = readClass(currentOffset + 2, charBuffer);
        String superClass = readClass(currentOffset + 4, charBuffer);
        String[] interfaces = new String[readUnsignedShort(currentOffset + 6)];
        currentOffset += 8;
        for (int i = 0; i < interfaces.length; ++i) {
            interfaces[i] = readClass(currentOffset, charBuffer);
            currentOffset += 2;
        }

        // ......

        // Visit the class declaration. The minor_version and major_version fields start 6 bytes before
        // the first constant pool entry, which itself starts at cpInfoOffsets[1] - 1 (by definition).
        classVisitor.visit(readInt(cpInfoOffsets[1] - 7), accessFlags, thisClass, signature, superClass, interfaces);

        // ......

        // Visit the fields and methods.
        int fieldsCount = readUnsignedShort(currentOffset);
        currentOffset += 2;
        while (fieldsCount-- > 0) {
            currentOffset = readField(classVisitor, context, currentOffset);
        }
        int methodsCount = readUnsignedShort(currentOffset);
        currentOffset += 2;
        while (methodsCount-- > 0) {
            currentOffset = readMethod(classVisitor, context, currentOffset);
        }

        // Visit the end of the class.
        classVisitor.visitEnd();
    }

}
```

另外，我们也可以回顾一下`ClassVisitor`类中`visitXxx()`方法的调用顺序：

```
visit
[visitSource][visitModule][visitNestHost][visitPermittedSubclass][visitOuterClass]
(
 visitAnnotation |
 visitTypeAnnotation |
 visitAttribute
)*
(
 visitNestMember |
 visitInnerClass |
 visitRecordComponent |
 visitField |
 visitMethod
)* 
visitEnd
```

## 如何使用ClassReader类

在现阶段，我们接触了`ClassVisitor`、`ClassWriter`和`ClassReader`类，因此可以介绍Class Transformation的操作。

```java
import com.potato.asm.utils.FileUtils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;

public class HelloWorldTransformCore {
    public static void main(String[] args) {
        String relative_path = "sample/HelloWorld.class";
        String filepath = FileUtils.getFilePath(relative_path);
        byte[] bytes1 = FileUtils.readBytes(filepath);

        //（1）构建ClassReader
        ClassReader cr = new ClassReader(bytes1);

        //（2）构建ClassWriter
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        //（3）串连ClassVisitor
        int api = Opcodes.ASM9;
        ClassVisitor cv = new ClassVisitor(api, cw) { /**/ };

        //（4）结合ClassReader和ClassVisitor
        int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
        cr.accept(cv, parsingOptions);

        //（5）生成byte[]
        byte[] bytes2 = cw.toByteArray();

        FileUtils.writeBytes(filepath, bytes2);
    }
}
```

代码的整体处理流程是如下这样的：

```
.class --> ClassReader --> ClassVisitor1 ... --> ClassVisitorN --> ClassWriter --> .class文件
```

我们可以将整体的处理流程想像成一条河流，那么

- 第一步，构建`ClassReader`。生成的`ClassReader`对象，它是这条“河流”的“源头”。
- 第二步，构建`ClassWriter`。生成的`ClassWriter`对象，它是这条“河流”的“归处”，它可以想像成是“百川东到海”中的“大海”。
- 第三步，串连`ClassVisitor`。生成的`ClassVisitor`对象，它是这条“河流”上的重要节点，可以想像成一个“水库”；可以有多个`ClassVisitor`对象，也就是在这条“河流”上存在多个“水库”，这些“水库”可以对“河水”进行一些处理，最终会这些“水库”的水会流向“大海”；也就是说多个`ClassVisitor`对象最终会连接到`ClassWriter`对象上。
- 第四步，结合`ClassReader`和`ClassVisitor`。在`ClassReader`类上，有一个`accept()`方法，它接收一个`ClassVisitor`类型的对象；换句话说，就是将“河流”的“源头”和后续的“水库”连接起来。
- 第五步，生成`byte[]`。到这一步，就是所有的“河水”都流入`ClassWriter`这个“大海”当中，这个时候我们调用`ClassWriter.toByteArray()`方法，就能够得到`byte[]`内容。

![img](img/ClassReader.assets/asm-core-classes.png)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                

## parsingOptions参数

在`ClassReader`类当中，`accept()`方法接收一个`int`类型的`parsingOptions`参数。

```
public void accept(final ClassVisitor classVisitor, final int parsingOptions)
```

`parsingOptions`参数可以选取的值有以下5个：

- `0`
- `ClassReader.SKIP_CODE`
- `ClassReader.SKIP_DEBUG`
- `ClassReader.SKIP_FRAMES`
- `ClassReader.EXPAND_FRAMES`

推荐使用：

- 在调用`ClassReader.accept()`方法时，其中的`parsingOptions`参数，推荐使用`ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES`。
- 在创建`ClassWriter`对象时，其中的`flags`参数，推荐使用`ClassWriter.COMPUTE_FRAMES`。

示例代码如下：

```java
ClassReader cr = new ClassReader(bytes);
int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
cr.accept(cv, parsingOptions);

ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
```

为什么我们推荐使用`ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES`呢？因为使用这样的一个值，可以生成最少的ASM代码，但是又能实现完整的功能。

- `0`：会生成所有的ASM代码，包括调试信息、frame信息和代码信息。
- `ClassReader.SKIP_CODE`：会忽略代码信息，例如，会忽略对于`MethodVisitor.visitXxxInsn()`方法的调用。
- `ClassReader.SKIP_DEBUG`：会忽略调试信息，例如，会忽略对于`MethodVisitor.visitParameter()`、`MethodVisitor.visitLineNumber()`和`MethodVisitor.visitLocalVariable()`等方法的调用。
- `ClassReader.SKIP_FRAMES`：会忽略frame信息，例如，会忽略对于`MethodVisitor.visitFrame()`方法的调用。
- `ClassReader.EXPAND_FRAMES`：会对frame信息进行扩展，例如，会对`MethodVisitor.visitFrame()`方法的参数有影响。

对于这些参数的使用，我们可以在`PrintASMCodeCore`类的基础上进行实验。

我们使用`ClassReader.SKIP_DEBUG`的时候，就不会生成调试信息。因为这些调试信息主要是**记录某一条instruction在代码当中的行数，以及变量的名字等信息**；如果没有这些调试信息，也不会影响程序的正常运行，也就是说功能不受影响，因此省略这些信息，就会让ASM代码尽可能的简洁。

我们使用`ClassReader.SKIP_FRAMES`的时候，就会忽略frame的信息。为什么要忽略这些frame信息呢？因为frame计算的细节会很繁琐，需要处理的情况也有很多，总的来说，就是比较麻烦。我们解决这个麻烦的方式，就是让ASM帮助我们来计算frame的情况，也就是在创建`ClassWriter`对象的时候使用`ClassWriter.COMPUTE_FRAMES`选项。

在刚开始学习ASM的时候，对于`parsingOptions`参数，我们推荐使用`ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES`的组合值。但是，以后，随着大家对ASM的知识越来越熟悉，或者随着功能需求的变化，大家可以尝试着使用其它的选项值。

## 总结

本文主要对`ClassReader`类进行了介绍，内容总结如下：

- 第一点，了解`ClassReader`类的成员都有哪些。
- 第二点，如何使用`ClassReader`类，来进行Class Transformation的操作。
- 第三点，在`ClassReader`类当中，对于`accept()`方法的`parsingOptions`参数，我们推荐使用`ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES`。



# ClassReader代码示例

整体思路：

对于一个`.class`文件进行Class Transformation操作，整体思路是这样的：

```
ClassReader --> ClassVisitor(1) --> ... --> ClassVisitor(N) --> ClassWriter
```

其中，

- `ClassReader`类，是ASM提供的一个类，可以直接拿来使用。
- `ClassWriter`类，是ASM提供的一个类，可以直接拿来使用。
- `ClassVisitor`类，是ASM提供的一个抽象类，因此需要写代码提供一个`ClassVisitor`的子类，在这个子类当中可以实现对`.class`文件进行各种处理操作。换句话说，进行Class Transformation操作，编写`ClassVisitor`的子类是关键。

## 修改类的信息

### 示例一：修改类的版本

预期目标：假如有一个`HelloWorld.java`文件，经过Java 8编译之后，生成的`HelloWorld.class`文件的版本就是Java 8的版本，我们的目标是将`HelloWorld.class`由Java 8版本转换成Java 7版本。

```java
public class HelloWorld1 {
}
```

#### ClassVisitor

```java
public class ClassChangeVersionVisitor extends ClassVisitor {
    public ClassChangeVersionVisitor(int api, ClassVisitor classVisitor) {
        super(api, classVisitor);
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(Opcodes.V1_7, access, name, signature, superName, interfaces);
    }
}
```

#### 进行转换

```java
import com.potato.asm.utils.FileUtils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;

public class HelloWorldTransformCore {
    public static void main(String[] args) {
        String relative_path = "sample/HelloWorld1.class";
        String filepath = FileUtils.getFilePath(relative_path);
        byte[] bytes1 = FileUtils.readBytes(filepath);

        //（1）构建ClassReader
        ClassReader cr = new ClassReader(bytes1);

        //（2）构建ClassWriter
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        //（3）串连ClassVisitor
        int api = Opcodes.ASM9;
        ClassVisitor cv = new ClassChangeVersionVisitor(api, cw);

        //（4）结合ClassReader和ClassVisitor
        int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
        cr.accept(cv, parsingOptions);

        //（5）生成byte[]
        byte[] bytes2 = cw.toByteArray();

        FileUtils.writeBytes(filepath, bytes2);
    }
}
```

#### 验证结果

修改前：

![image-20211210213656758](img/ClassReader.assets/image-20211210213656758.png)

修改后：

![image-20211210213730955](img/ClassReader.assets/image-20211210213730955.png)

或者使用命令查看：

```
javap -p -v sample.HelloWorld1
```

### 示例二：修改类的接口

预期目标：在下面的`HelloWorld`类中，我们定义了一个`clone()`方法，但存在一个问题，也就是，如果没有实现`Cloneable`接口，`clone()`方法就会出错，我们的目标是希望通过ASM为`HelloWorld`类添加上`Cloneable`接口。

#### 目标源码

```java
public class HelloWorld {
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
```

#### ClassVisitor

编码实现：

```java
import org.objectweb.asm.ClassVisitor;

public class ClassCloneVisitor extends ClassVisitor {
    public ClassCloneVisitor(int api, ClassVisitor cw) {
        super(api, cw);
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(version, access, name, signature, superName, new String[]{"java/lang/Cloneable"});
    }
}
```

#### 进行转换

```java
import com.potato.asm.utils.FileUtils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;

public class HelloWorldTransformCore {
    public static void main(String[] args) {
        String relative_path = "sample/HelloWorld.class";
        String filepath = FileUtils.getFilePath(relative_path);
        byte[] bytes1 = FileUtils.readBytes(filepath);

        //（1）构建ClassReader
        ClassReader cr = new ClassReader(bytes1);

        //（2）构建ClassWriter
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        //（3）串连ClassVisitor
        int api = Opcodes.ASM9;
        ClassVisitor cv = new ClassCloneVisitor(api, cw);

        //（4）结合ClassReader和ClassVisitor
        int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
        cr.accept(cv, parsingOptions);

        //（5）生成byte[]
        byte[] bytes2 = cw.toByteArray();

        FileUtils.writeBytes(filepath, bytes2);
    }
}
```

#### 验证结果

```java
public class HelloWorldRun {
    public static void main(String[] args) throws Exception {
        HelloWorld obj = new HelloWorld();
        Object anotherObj = obj.clone();
        System.out.println(anotherObj);
    }
}
```

修改前：

![image-20211210212450970](img/ClassReader.assets/image-20211210212450970.png)

修改后：

![image-20211210212405539](img/ClassReader.assets/image-20211210212405539.png)

### 示例三：添加类的注解

#### 目标

现有如下的类

```java
public class Student {

}
```

需要加上注解,变成如下

```java
@TableName("\"MB_Student\"") //Mybatis-Plus的注解
public class Student {

}
```

#### ClassVisitor

```java

```



### 小总结

基于ClassVisitor.visit()方法

```java
public void visit(int version, int access, String name, String signature, String superName, String[] interfaces)
```

这两个示例，就是通过修改`visit()`方法的参数实现的：

- 修改类的版本信息，是通过修改`version`这个参数实现的
- 修改类的接口信息，是通过修改`interfaces`这个参数实现的

其实，在`visit()`方法当中的其它参数也可以修改：

- 修改`access`参数，也就是修改了类的访问标识信息。
- 修改`name`参数，也就是修改了类的名称。但是，在大多数的情况下，不推荐修改`name`参数。因为调用类里的方法，都是先找到类，再找到相应的方法；如果将当前类的类名修改成别的名称，那么其它类当中可能就找不到原来的方法了，因为类名已经改了。但是，也有少数的情况，可以修改`name`参数，比如说对代码进行混淆（obfuscate）操作。
- 修改`superName`参数，也就是修改了当前类的父类信息。



## 修改字段信息

### 示例三：删除字段

预期目标：删除掉`HelloWorld`类里的`String strValue`字段。

```java
public class HelloWorld {
    public int intValue;
    public String strValue; // 删除这个字段
}
```



编码实现：

```java
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;

public class ClassRemoveFieldVisitor extends ClassVisitor {
    private final String fieldName;
    private final String fieldDesc;

    public ClassRemoveFieldVisitor(int api, ClassVisitor cv, String fieldName, String fieldDesc) {
        super(api, cv);
        this.fieldName = fieldName;
        this.fieldDesc = fieldDesc;
    }

    @Override
    public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
        if (name.equals(fieldName) && descriptor.equals(fieldDesc)) {
            return null;//要想删除，直接返回null就行
        }
        return super.visitField(access, name, descriptor, signature, value);
    }
}
```

上面代码思路的关键就是`ClassVisitor.visitField()`方法。在正常的情况下，`ClassVisitor.visitField()`方法返回一个`FieldVisitor`对象；但是，如果`ClassVisitor.visitField()`方法返回的是`null`，就么能够达到删除该字段的效果。

我们之前说过一个形象的类比，就是将`ClassReader`类比喻成河流的“源头”，而`ClassVisitor`类比喻成河流的经过的路径上的“水库”，而`ClassWriter`类则比喻成“大海”，也就是河水的最终归处。如果说，其中一个“水库”拦截了一部分水流，那么这部分水流就到不了“大海”了；这就相当于`ClassVisitor.visitField()`方法返回的是`null`，从而能够达到删除该字段的效果。。

或者说，换一种类比，用信件的传递作类比。将`ClassReader`类想像成信件的“发出地”，将`ClassVisitor`类想像成信件运送途中经过的“驿站”，将`ClassWriter`类想像成信件的“接收地”；如果是在某个“驿站”中将其中一封邮件丢失了，那么这封信件就抵达不了“接收地”了。

```java
import com.potato.asm.utils.FileUtils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;

public class HelloWorldTransformCore {
    public static void main(String[] args) {
        String relative_path = "com/potato/asm/classreader/field/HelloWorld.class";
        String filepath = FileUtils.getFilePath(relative_path);
        byte[] bytes1 = FileUtils.readBytes(filepath);

        //（1）构建ClassReader
        ClassReader cr = new ClassReader(bytes1);

        //（2）构建ClassWriter
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        //（3）串连ClassVisitor
        int api = Opcodes.ASM9;
        ClassVisitor cv = new ClassRemoveFieldVisitor(api, cw, "strValue", "Ljava/lang/String;");

        //（4）结合ClassReader和ClassVisitor
        int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
        cr.accept(cv, parsingOptions);

        //（5）生成byte[]
        byte[] bytes2 = cw.toByteArray();

        FileUtils.writeBytes(filepath, bytes2);
    }
}
```

### 示例四：添加字段



预期目标：为了`HelloWorld`类添加一个`Object objValue`字段。

```java
public class HelloWorld {
    public int intValue;
    public String strValue;
    // 添加一个Object objValue字段
}
```

编码实现：

```java
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;

public class ClassAddFieldVisitor extends ClassVisitor {
    private final int fieldAccess;
    private final String fieldName;
    private final String fieldDesc;
    private boolean isFieldPresent;

    public ClassAddFieldVisitor(int api, ClassVisitor classVisitor, int fieldAccess, String fieldName, String fieldDesc) {
        super(api, classVisitor);
        this.fieldAccess = fieldAccess;
        this.fieldName = fieldName;
        this.fieldDesc = fieldDesc;
    }

    @Override
    public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
        if (name.equals(fieldName)) {
            isFieldPresent = true;
        }
        return super.visitField(access, name, descriptor, signature, value);
    }

    @Override
    public void visitEnd() {
        if (!isFieldPresent) {
            FieldVisitor fv = cv.visitField(fieldAccess, fieldName, fieldDesc, null, null);
            if (fv != null) {
                fv.visitEnd();
            }
        }
        super.visitEnd();
    }
}
```

上面的代码思路：第一步，在`visitField()`方法中，判断某个字段是否已经存在，其结果存在于`isFieldPresent`字段当中；第二步，就是在`visitEnd()`方法中，根据`isFieldPresent`字段的值，来决定是否添加新的字段。

```java
import com.potato.asm.utils.FileUtils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;

public class HelloWorldTransformCore2 {
    public static void main(String[] args) {
        String relative_path = "sample/HelloWorld.class";
        String filepath = FileUtils.getFilePath(relative_path);
        byte[] bytes1 = FileUtils.readBytes(filepath);

        //（1）构建ClassReader
        ClassReader cr = new ClassReader(bytes1);

        //（2）构建ClassWriter
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        //（3）串连ClassVisitor
        int api = Opcodes.ASM9;
        ClassVisitor cv = new ClassAddFieldVisitor(api, cw, Opcodes.ACC_PUBLIC, "objValue", "Ljava/lang/Object;");

        //（4）结合ClassReader和ClassVisitor
        int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
        cr.accept(cv, parsingOptions);

        //（5）生成byte[]
        byte[] bytes2 = cw.toByteArray();

        FileUtils.writeBytes(filepath, bytes2);
    }
}
```

### 小总结

对于字段的操作，都是基于`ClassVisitor.visitField()`方法来实现的：

```
public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value);
```

那么，对于字段来说，可以进行哪些操作呢？有三种类型的操作：

- 修改现有的字段。例如，修改字段的名字、修改字段的类型、修改字段的访问标识，这些需要通过修改`visitField()`方法的参数来实现。
- 删除已有的字段。在`visitField()`方法中，返回`null`值，就能够达到删除字段的效果。
- 添加新的字段。在`visitField()`方法中，判断该字段是否已经存在；在`visitEnd()`方法中，如果该字段不存在，则添加新字段。

一般情况下来说，不推荐“修改已有的字段”，也不推荐“删除已有的字段”，原因如下：

- 不推荐“修改已有的字段”，因为这可能会引起字段的名字不匹配、字段的类型不匹配，从而导致程序报错。例如，假如在`HelloWorld`类里有一个`intValue`字段，而且`GoodChild`类里也使用到了`HelloWorld`类的这个`intValue`字段；如果我们将`HelloWorld`类里的`intValue`字段名字修改为`myValue`，那么`GoodChild`类就再也找不到`intValue`字段了，这个时候，程序就会出错。当然，如果我们把`GoodChild`类里对于`intValue`字段的引用修改成`myValue`，那也不会出错了。但是，我们要保证所有使用`intValue`字段的地方，都要进行修改，这样才能让程序不报错。
- 不推荐“删除已有的字段”，因为一般来说，类里的字段都是有作用的，如果随意的删除就会造成字段缺失，也会导致程序报错。

为什么不在`ClassVisitor.visitField()`方法当中来添加字段呢？**如果在`ClassVisitor.visitField()`方法，就可能添加重复的字段，这样就不是一个合法的ClassFile了。**

## 修改方法信息

### 示例五：删除方法

预期目标：删除掉`HelloWorld`类里的`add()`方法。

```java
public class HelloWorld {
    public int add(int a, int b) { // 删除add方法
        return a + b;
    }

    public int sub(int a, int b) {
        return a - b;
    }
}
```

编码实现：

```java
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

public class ClassRemoveMethodVisitor extends ClassVisitor {
    private final String methodName;
    private final String methodDesc;

    public ClassRemoveMethodVisitor(int api, ClassVisitor cv, String methodName, String methodDesc) {
        super(api, cv);
        this.methodName = methodName;
        this.methodDesc = methodDesc;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        if(name.equals(methodName) && descriptor.equals(methodDesc)) {
            return null;
        }
        return super.visitMethod(access, name, descriptor, signature, exceptions);
    }
}
```

上面删除方法的代码思路，与删除字段的代码思路是一样的。

 ```java
import com.potato.asm.utils.FileUtils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;

public class HelloWorldTransformCore {
    public static void main(String[] args) {
        String relative_path = "sample/HelloWorld.class";
        String filepath = FileUtils.getFilePath(relative_path);
        byte[] bytes1 = FileUtils.readBytes(filepath);

        //（1）构建ClassReader
        ClassReader cr = new ClassReader(bytes1);

        //（2）构建ClassWriter
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        //（3）串连ClassVisitor
        int api = Opcodes.ASM9;
        ClassVisitor cv = new ClassRemoveMethodVisitor(api, cw, "add", "(II)I");

        //（4）结合ClassReader和ClassVisitor
        int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
        cr.accept(cv, parsingOptions);

        //（5）生成byte[]
        byte[] bytes2 = cw.toByteArray();

        FileUtils.writeBytes(filepath, bytes2);
    }
}
 ```

### 示例六：添加方法

预期目标：为`HelloWorld`类添加一个`mul()`方法。

```java
public class HelloWorld {
    public int add(int a, int b) {
        return a + b;
    }

    public int sub(int a, int b) {
        return a - b;
    }

    // TODO: 添加一个乘法
}
```

编码实现：

```java
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

public abstract class ClassAddMethodVisitor extends ClassVisitor {
    private final int methodAccess;
    private final String methodName;
    private final String methodDesc;
    private final String methodSignature;
    private final String[] methodExceptions;
    private boolean isMethodPresent;

    public ClassAddMethodVisitor(int api, ClassVisitor cv, int methodAccess, String methodName, String methodDesc,
                                 String signature, String[] exceptions) {
        super(api, cv);
        this.methodAccess = methodAccess;
        this.methodName = methodName;
        this.methodDesc = methodDesc;
        this.methodSignature = signature;
        this.methodExceptions = exceptions;
        this.isMethodPresent = false;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        if (name.equals(methodName) && descriptor.equals(methodDesc)) {
            isMethodPresent = true;
        }
        return super.visitMethod(access, name, descriptor, signature, exceptions);
    }

    @Override
    public void visitEnd() {
        if (!isMethodPresent) {
            MethodVisitor mv = super.visitMethod(methodAccess, methodName, methodDesc, methodSignature, methodExceptions);
            if (mv != null) {
                // create method body
                generateMethodBody(mv);
            }
        }

        super.visitEnd();
    }

    protected abstract void generateMethodBody(MethodVisitor mv);
}
```

添加新的方法，和添加新的字段的思路，在前期，两者是一样的，都是先要判断该字段或该方法是否已经存在；但是，在后期，两者会有一些差异，**因为方法需要有“方法体”**，在上面的代码中，我们定义了一个`generateMethodBody()`方法，这个方法需要在子类当中进行实现。

```java
import com.potato.asm.utils.FileUtils;
import org.objectweb.asm.*;

import static org.objectweb.asm.Opcodes.*;

public class HelloWorldTransformCore2 {
    public static void main(String[] args) {
        String relative_path = "com/potato/asm/classreader/method/HelloWorld.class";
        String filepath = FileUtils.getFilePath(relative_path);
        byte[] bytes1 = FileUtils.readBytes(filepath);

        //（1）构建ClassReader
        ClassReader cr = new ClassReader(bytes1);

        //（2）构建ClassWriter
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        //（3）串连ClassVisitor
        int api = Opcodes.ASM9;
        ClassVisitor cv = new ClassAddMethodVisitor(api, cw, Opcodes.ACC_PUBLIC, "mul", "(II)I", null, null) {
            @Override
            protected void generateMethodBody(MethodVisitor mv) {
                mv.visitCode();
                mv.visitVarInsn(ILOAD, 1);
                mv.visitVarInsn(ILOAD, 2);
                mv.visitInsn(IMUL);
                mv.visitInsn(IRETURN);
                mv.visitMaxs(2, 3);
                mv.visitEnd();
            }
        };

        //（4）结合ClassReader和ClassVisitor
        int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
        cr.accept(cv, parsingOptions);

        //（5）生成byte[]
        byte[] bytes2 = cw.toByteArray();

        FileUtils.writeBytes(filepath, bytes2);
    }
}
```

### 小总结

对于方法的操作，都是基于`ClassVisitor.visitMethod()`方法来实现的：

```
public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions);
```

与字段操作类似，对于方法来说，可以进行的操作也有三种类型：

- 修改现有的方法。
- 删除已有的方法。
- 添加新的方法。

**我们不推荐“删除已有的方法”，因为这可能会引起方法调用失败，从而导致程序报错。**

另外，对于“修改现有的方法”，我们**不建议修改方法的名称、方法的类型（接收参数的类型和返回值的类型）**，因为别的地方可能会对该方法进行调用，修改了方法名或方法的类型，就会使方法调用失败。但是，我们**可以“修改现有方法”的“方法体”**，也就是方法的具体实现代码。

## 总结

本文主要是使用`ClassReader`类进行Class Transformation的代码示例进行介绍，内容总结如下：

- 第一点，类层面的信息，例如，类名、父类、接口等，可以通过`ClassVisitor.visit()`方法进行修改。
- 第二点，字段层面的信息，例如，添加新字段、删除已有字段等，可能通过`ClassVisitor.visitField()`方法进行修改。
- 第三点，方法层面的信息，例如，添加新方法、删除已有方法等，可以通过`ClassVisitor.visitMethod()`方法进行修改。

但是，对于方法层面来说，还有一个重要的方面没有涉及，也就是对于现有方法里面的代码进行修改，我们在后续内容中会有介绍。

1