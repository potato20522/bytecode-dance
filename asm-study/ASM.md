# 参考资料

**感谢lsieun老师的免费分享**

视频教程： [Java ASM系列：（001）课程介绍_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1Py4y1g7dN)

老师笔记：

[Java ASM系列一：Core API | lsieun](https://lsieun.cn/java/asm/java-asm-season-01.html)

https://blog.51cto.com/lsieun/2924583

https://lsieun.github.io/java/asm/index.html （推荐）

老师源码：

[learn-java-asm: 学习Java ASM (gitee.com)](https://gitee.com/lsieun/learn-java-asm)

[src/main/java/run · lsieun/java8-classfile-tutorial - 码云 - 开源中国 (gitee.com)](https://gitee.com/lsieun/java8-classfile-tutorial/tree/master/src/main/java/run)

ASM官网：http://asm.ow2.org/

ASM Javadoc [在线文档-asm (oschina.net)](https://tool.oschina.net/apidocs/apidoc?api=asm)

ASM9.2 Javadoc [Overview (ASM 9.2) (ow2.io)](https://asm.ow2.io/javadoc/index.html)

美团ASM文章：[字节码增强技术探索 - 美团技术团队 (meituan.com)](https://tech.meituan.com/2019/09/05/java-bytecode-enhancement.html)

ASM使用手册：[ASM4 使用手册(中文版) · 语雀 (yuque.com)](https://www.yuque.com/mikaelzero/asm)

# ASM介绍

## ASM是什么

筒单来说,**ASM是一个操作Java字节码的类库。**

为了能够更好的理解ASM是什么,我们需要来搞清楚两个问题:

第一个问题:**ASM的操作对象是什么呢**?

第二个问题:**ASM是如何处理字节码( Byte Code)数据的**?

首先，我们来看第一个问题:ASM的操作对象是什么呢?回答:ASM所操作的对是**字节码( Bytecode)数据**

我们都知道,一个,java文件经过java缟译器( javac)编译之后会生成一个.class文件。在.class文件中,存储的是字节码( Byte Code)数据。ASM所的操作对象是是字节码( Bytecode),而在许多情况下,字节码( Byte Code)的具体表现形式是.class文件。

接着,我们来看第二个问题:ASM是如何处理字节码( Byte Code)数据的?

回答:**ASM处理字节码( Byte Code)的方式是”拆分-修改-合并**"。

ASM处理字节码( Bytecode)数据的思路是这样的:

**第一步**,将.class文件拆分成多个部分;

**第二步**,对某一个部分的信息进行修改;

**第三步**,将多个部分重新组织成一个新的.class文件

在 Wikipedia上,对ASM进行了如下描述：

ASM provides a simple API for decomposing(**将一个整体拆分成多个部分**), modifying(**修改某一部分的信息**), and recomposing(**将多个部分重新组织成一个整体**) binary Javaclassees(i.e. Byte Code)

## ASM的过去和现在

**ASM的过去**

对于ASM的过去,主要说明三个问题

第一个问题,ASM从什么时候开始出现的?

第二个问题,ASM的作者是谁?

第三个问题,A的名字有什么含义?

在2002年的时候, Eric Bruneton、 Romain Lenglet和 Thierry Coupaye发表了一篇文章,名为《AsM: a code manipulation tool to implement adaptable systems》。在这篇文章当中,他们提出了ASM的设计思路

一般来说,大写字母的组合,可能是多个单词的缩写形式,例如,JWM表示 Java Virtual Machine"。但是,ASM并不是多个单词的首字母缩写形式。在上面的文章中,记录了下面的话The ASM name does not mean anything: it is just a reference to the asm keyword in C which allows some functions to be implemented in assemby language.C语言中ASM关键字可以使用汇编语言，向C语言中ASM致敬。

**ASM现在**

对于ASM的现在,主要说明两个问题·

第一个问题,ASM属于哪一个机构?·

第二个问题,ASM的Log是什么样的?

The ASM library is a project of the **OW2** Consortium. ow2 is an independent, global, open-source software community.

作为一个小故事,我们来说一下OW2组织是如何形成的。OW2组织的形成,与中国的一些大学和公司也有很大的关系

2002年, Objectweb项目启动,它是由 INRIA、Bull和 France Telecom共同开发的项目,并形成了一个成熟的、开源软件社区

2004年, Orientware项目启动,由中国的北京大学、北航、囯防科技大学、中创软件和中囯科学院软件硏究所共同研发

2005年, Objectweb和 Orientware签署了一份协议,决定共享代码库,一起开发中间件软件。

2006年, Objectweb和 Orientware两个社区组织融合,形成了OW2组织。OW2组织名字,可能是由 Objectweb和 Orientware名称当中的两组0和w组合而来。

ASM的Logo设计很有特点,它在旋转的过程中,会分别呈现出"A"、"S"和"M"这三个字母

**ASM的版本发展**

对于ASM版本的发展,我们要说明两点·第一点,Java语言在不断发展,那么,ASM版本也要不断发展来跟得上Java的发展·第二点,在选择ASM版本的时候,要注意它支持的]ava版本,来确保兼容性。

比如说,我们常用的]ava版本是java8和Java11。针对java8版本,我们需要使用ASM5.0版本,就能正常工作。对于java11版本,我们需要使用AsM7.0版本,就能正常工作。当然,我们可以尽量使用较高的ASM版本

| ASM版本 | 发布日期   | 支持的Java版本 |
| ------- | ---------- | -------------- |
| 2.0     | 2005-05-17 | 5              |
| 3.2     | 2009-06-11 | 6              |
| 4.0     | 2011-10-29 | 7              |
| 5.0     | 2014-03-16 | **8**          |
| 6.0     | 2017-09-23 | 9              |
| 6.1     | 2018-03-11 | 10             |
| 7.0     | 2018-10-27 | **11**         |
| 7.1     | 2019-03-03 | 13             |
| 8.0     | 2020-03-28 | 14             |
| 9.0     | 2020-09-22 | 16             |
| 9.1     | 2021-02-06 | 17             |
| 9.2     | 2021-06-26 | 18             |

## ASM能做什么

通俗的理解：

- 父类:修改成一个新的父类

- 接囗:添加一个新的接口、删除已有的接囗·

- 字段:添加一个新的字段、删除已有的字段·

- 方法:添加一个新的方法、删除已有的方法、修改己有的方法

- ......

专业的理解：

ASM is an all- purpose(多用途的;通用的) Java Byte Code manipulation and analysis framework. It can be used to modify existing classes or to dynamically generateclasses, directly in binary formJs goal of the ASM library is to generate, transform and analyze compiled Java classes, represented as byte arrays(as they are stored on disk and loaded in thea virtual Machine)

<img src="ASM.assets/image-20210630220602596.png" alt="image-20210630220602596" style="zoom:80%;" />

**Program analysis** , which can range from a simple syntactic parsing to a full semantic analysis , can be used to find potential bugs in applications , to detect unusedcode , to reverse engineer code , etc .

**Program generation** is used in compilers . This includes traditional compilers , but also stub or skeleton compilers used for distributed programming , Just in Timecompilers , etc

**Program transformation** can be used to optimize or obfuscate programs , to insert debugging or performance monitoring code into applications , for aspectoriented programming , etc

## 为什么要学ASM

平常,我们使用Java语言进行开发,能够解决很多的问题。我们可以把Java语言解决问题的范围称之为]ava语言的世界"。那么,ASM起什么作用呢?ASM就是处位于"Java语言的世界”边界上的一扇大门,通过这扇大门,我们可以前往”字节码的世界”。在字节码的世界"里,我们会看到不一样的”风景”,能够解决不一样的问题"。

<img src="ASM.assets/image-20210630220937223.png" alt="image-20210630220937223" style="zoom:80%;" />

ASM往往在一些框架的底层起着重要的作用。接下来,我们介绍两个关于ASM的应用场景: Spring和JDK。这两个应用场景例子的目的,就是希望大家了解到ASM的重要性

### Spring当中的ASM

第一个应用场景,是 **Spring**框架当中的AoP。在很多]ava项目中,都会使用到 Spring框架,而 Spring框架当中的AOP( Aspect Oriented Programming)是依赖于ASl具体来说Spring的AOP,可以通过]DK的动态代理来实现,也可以通过 CGLIB实现。其中**, Cglib**( Code Generation Library)是在ASM的基础上构建起来的,所以, **Spring AOP是间接的使用了ASM**。(参考自 Spring Framework Reference Documentation的8.6 Proxying mechanisms)。

### JDK当中的ASM

**JDK**当中的ASM第二个应用场景,是**JDK当中的 Lambda表达式**。在]ava8中引入了一个非常重要的特性,就是支持 Lambda表达式。 Lambda表达式,允许把方法作为参数进行传递,它能够使代码变的更加简洁紧凑。但是,我们可能没有注意到,其实,在现阶段(Java8版本), L**ambda表达式的调用是通过ASM来实现的**

在rt.ar文件的jdk.internal.org. objectweb.asm包当中,就包含了JDK内置的ASM代码。在JDK8版本当中,它所使用的ASM50版本。如果我们跟踪 Lambda表达式的编码实现,就会找到

InnerClasslambdaMetaFactory.spinInnerClass()方法。在这个方法当中,我们就会看到:JDK会使用jdk.internal.org.objectweb asm.ClassWriter个类,将 lambda表达式的代码包装起来。

![image-20210630221859294](ASM.assets/image-20210630221859294.png)

- LambdaMetaFactory.metaFactory() 第一步,找到这个方法

- InnerClasslambdaMetaFactory.buildCallSite() 第二步,找到这个方法
- InnerClasslambdaMetaFactory.spinInnerClass() 第三步,找到这个方法

![image-20210630222214317](ASM.assets/image-20210630222214317.png)

ClassWriter cw 这个对象

![image-20210630222247631](ASM.assets/image-20210630222247631.png)

#### JDK收编了整个ASM

![image-20211212222751354](img/ASM.assets/image-20211212222751354.png)

本文主要是对ASM进行简单的介绍,希望大家能够对ASM有基本的了解,内容总结如下第二点ASM所处理对象是字节码数据,也可以直观的理解成.c1as5文件,不是.java文件。ASM能够对字节码数据进行哪些操作呢?回答: analyze、 generate、 transform·第二点,ASM可以形象的理解为]ava语言世界“的边缘上一扇大门,通过这扇大门,可以帮助我们进入到字节码的世界同时,本文当中并没有涉及到技术性的知识点,因此也不需要记忆任何内容,了解一下就够了

# 搭建ASM开发环境

```xml
<dependency>
    <groupId>org.ow2.asm</groupId>
    <artifactId>asm</artifactId>
    <version>${asm.version}</version>
</dependency>
<dependency>
    <groupId>org.ow2.asm</groupId>
    <artifactId>asm-commons</artifactId>
    <version>${asm.version}</version>
</dependency>
<dependency>
    <groupId>org.ow2.asm</groupId>
    <artifactId>asm-util</artifactId>
    <version>${asm.version}</version>
</dependency>
<dependency>
    <groupId>org.ow2.asm</groupId>
    <artifactId>asm-tree</artifactId>
    <version>${asm.version}</version>
</dependency>
<dependency>
    <groupId>org.ow2.asm</groupId>
    <artifactId>asm-analysis</artifactId>
    <version>${asm.version}</version>
</dependency>
```

我们的目标是，生成一个HelloWorld类，但不是去手写这个java文件

```java
package com.potato.asm;

public class HelloWorld {
    @Override
    public String toString() {
        return "This is HelloWorld object";
    }
}
```

实现：

```java
package com.potato.asm;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class HelloWorldDump implements Opcodes {
    public static byte[] dump(){
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        cw.visit(V1_8, ACC_PUBLIC | ACC_SUPER, "sample/HelloWorld", null, "java/lang/Object", null);
        {
            MethodVisitor mv1 = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            mv1.visitCode();
            mv1.visitVarInsn(ALOAD, 0);
            mv1.visitMethodInsn(INVOKESPECIAL,"java/lang/Object","<init>", "()V",false);
            mv1.visitInsn(RETURN);
            mv1.visitMaxs(1, 1);
            mv1.visitEnd();
        }
        {
            MethodVisitor mv2 = cw.visitMethod(ACC_PUBLIC, "toString", "()Ljava/lang/String;", null, null);
            mv2.visitCode();
            mv2.visitLdcInsn("This is a HelloWorld object.");
            mv2.visitInsn(ARETURN);
            mv2.visitMaxs(1, 1);
            mv2.visitEnd();
        }
        cw.visitEnd();
        return cw.toByteArray();
    }
}

```

验证结果：

```java
package com.potato.asm;

public class MyClassLoader extends ClassLoader{
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        if ("sample.HelloWorld".equals(name)){
            byte[] bytes = HelloWorldDump.dump();
            Class<?> clazz = defineClass(name, bytes, 0, bytes.length);
            return clazz;
        }
        throw new ClassNotFoundException("Class Not Found:" + name);
    }
}

```



```java
package com.potato.asm;

public class HelloWorldRun {
    public static void main(String[] args) throws Exception {
        MyClassLoader classLoader = new MyClassLoader();
        Class<?> clazz = classLoader.loadClass("sample.HelloWorld");
        Object instance = clazz.newInstance();
        System.out.println(instance);
    }
}
```

This is a HelloWorld object.

# ASM的组成

组成结构上来说,ASM分成两部分,一部分为 Core ApI另一部分为 Tree API·其中, Core Ap包括asm,jsm-util jar Fn asm-commons jar其中, Tree api包括asm-tar和asm- analysis,jar

其中, Core API包括asm.jar、asm-utill.jar和asm-commons.jar

其中, Tree API包括asm-tree.jar和asm-analysis.jar



<img src="ASM.assets/image-20210630222620197.png" alt="image-20210630222620197" style="zoom:80%;" />

从两者的关系来说, Core API是基础,而 Tree API是在 Core ApI的这个基础上构建起来的。

从 ASM API演进的历史来讲,先有 Core api,后有 Tree API。最初,在2002年, Eric Bruneton等发表了一篇文章,即《AsM: a code manipulation tool to implement adaptablesystems》。在这篇文章当中,最早提出了ASM的设计思路。当时,ASM只包含13个类文件,Jar包的大小只有21KB这13个类文件,就是现在所说的 Core ap的雏形,但当时并没有提出Core Apl这样的概念。随着时代的变化,人们对于修改Java字节码提出更多的需求。为了满足人们的需求,ASM就需要添加新的类。类的数量变多了,代码的管理也就变得困难起来。为了更好的管理ASM的代码,就将这些类(按照功能的不同)分配到不同的Jar包当中,这样就逐渐衍生出 Core api和 Tree api的稅念。

## Core API

### asm.jar

在 asm. jar文件中,一共包含了30多个类,我们会介绍其中10个类。那么,剩下的20多个类,为什么不介绍呢?因为剩下的20多个主要起到辅助"的作用,它们更多的倾向于是”幕后工作者;而登上舞台表演的则是属于那10个类

在第二章“当中,我们会主要介绍从无到有”生成一个新的类,其中会涉及到ClassVistor、 ClassWriter、 FieldVisitor、 FieldWriter、 MethodVisitor、 MethodWriter、Label和 Opcodes类

在”第三章”当中,我们会主要介绍修改”已经存在的类”,使之内容发生改变,其中会涉及到ClassReader和Type

在这10个类当中,最重要的是三个类,即ClassReader、ClassVistor和ClassWriter类。这三个类的关系,可以描述成下图。

![image-20210630224303349](ASM.assets/image-20210630224303349.png)

这三个类的作用,可以简单理解成这样

ClassReader类,负责读取,class文件里的内容,然后折分成各个不同的部分。

ClassVisitor类,负责对class文件中某一部分里的信息进行修改。

ClassWriter类,负责将各个不同的部分重新组合成一个完整的,class文件

在第二章当中,主要围绕着ClassVisitor和 ClassWriter这两个类展开,因为在这个部分,我们是从“无"到有生成个新的类,不需要ClassReader类的参与。

在”第三章”当中,就需要ClassReader、 ClassVisitor和 ClassWriter这三个类的共同参与。

### asm-util.jar

asm-util.jar主要包含的是一些工具

在下图当中,可以看到asm-util.jar里面包含的具体类文件。这些类主要分成两种类型:check开头和 Trace开头

- 以 Check开头的类,主要负责检查( Check)生成的.class文件内容是否正确。·
- 以 Trace开头的类,主要负责将,class文件的内容打印成文字输出。根据输出的文字信息,可以探索或追踪( Trace),class文件的內部信息。

<img src="ASM.assets/image-20210630224814233.png" alt="image-20210630224814233" style="zoom:80%;" />

在asm-util.jar当中,主要介绍 CheckClassAdapter类和 TraceClassVistor类,也会简略的说明一下 Printer、 ASMifier和 Textifier类在第四章”当中,会介绍asm-util.jar里的内容

### asm-commons.jar

常用功能类

<img src="ASM.assets/image-20210630225321753.png" alt="image-20210630225321753" style="zoom:80%;" />

我们会介绍到其中的 AdviceAdapter、 Analyzeradapter、 Classremapper、 Generatoradapter、 Instructionadapter、 Localvariable sorter、 Serialversionuidadapter和 Staticinitmerger类

在第四章当中,介绍asm- commons.jar里的内容。

一个非常容易混淆的问题就是, asm-utiljar与 asm-commons.jar有什么区别呢?

在asm-util.jar里,它提供的是通用性的功能,没有特别明确的应用场景;而在asm-commons.jar里,它提供的功能,都是为解决某一种特定场景中出现的问题而提出的解决思路

## Tree API

###  asm-tree.jar

在`asm-tree.jar`(9.0版本)当中，一共包含了36个类，我们会涉及到其中的20个类，这20个类就构成`asm-tree.jar`主体部分。

在`asm-tree.jar`当中，这20个类具体内容如下：

- ClassNode
- FieldNode
- MethodNode
- InsnList
- AbstractInsnNode
  - FieldInsnNode
  - IincInsnNode
  - InsnNode
  - IntInsnNode
  - InvokeDynamicInsnNode
  - JumpInsnNode
  - LabelNode
  - LdcInsnNode
  - LookupSwitchInsnNode
  - MethodInsnNode
  - MultiANewArrayInsnNode
  - TableSwitchInsnNode
  - TypeInsnNode
  - VarInsnNode
- TryCatchBlockNode

为了方便于理解，我们可以将这些类按“包含”关系组织起来：

- ClassNode（类）
  - FieldNode（字段）
  - MethodNode（方法）
    - InsnList（有序的指令集合）
      - AbstractInsnNode（单条指令）
    - TryCatchBlockNode（异常处理）

我们可以用文字来描述它们之间的关系：

- 第一点，类（`ClassNode`）包含字段（`FieldNode`）和方法（`MethodNode`）。
- 第二点，方法（`MethodNode`）包含有序的指令集合（`InsnList`）和异常处理（`TryCatchBlockNode`）。
- 第三点，有序的指令集合（`InsnList`）由多个单条指令（`AbstractInsnNode`）组合而成。

### asm-analysis.jar

在`asm-analysis.jar`(9.0版本)当中，一共包含了13个类，我们会涉及到其中的10个类：

- Analyzer
- BasicInterpreter
- BasicValue
- BasicVerifier
- Frame
- Interpreter
- SimpleVerifier
- SourceInterpreter
- SourceValue
- Value

同样，为了方便于理解，我们也可以将这些类按“包含”关系组织起来：

- Analyzer
  - Frame
  - Interpreter + Value
    - BasicInterpreter + BasicValue
    - BasicVerifier + BasicValue
    - SimpleVerifier + BasicValue
    - SourceInterpreter + SourceValue

接着，我们用文字来描述它们之间的关系：

- 第一点，`Analyzer`是一个“胶水”，它起到一个粘合的作用，它就是将`Frame`（不变的部分）和`Interpreter`（变化的部分）组织到了一起。`Frame`类表示的就是一种不变的规则，就类似于现实世界的物理规则；而`Interpreter`类则是在这个规则基础上衍生的变化，就比如说能否造出一个圆珠笔，能否造出一个火箭飞上天空。
- 第二点，`Frame`类并不是绝对的“不变”，它本身也包含“不变”和“变化”的部分。其中，“不变”的部分就是指`Frame.execute()`方法，它就是模拟opcode在执行过程中，对于local variable和operand stack的影响，这是[JVM文档](https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-6.html)所规定的内容；而“变化”的部分就是指`Frame`类的`V[] values`字段，它需要记录每一个opcode执行前、后的具体值是什么。
- 第三点，`Interpreter`类就是体现“个人创造力”的一个类。比如说，同样是受这个现实物理世界规则的约束，在不同的历史阶段过程中，有人发明了风筝（纸鸢），有人发明了烟花，有人发明了滑翔机，有人发明了喷气式飞机，有人发明了宇宙飞船。同样，我们可以实现具体的`Interpreter`子类来完成特定的功能，那么到底能够达到一种什么样的效果，还是会取决于我们自身的“创造力”。

另外，我们也不会涉及`Subroutine`类。因为`Subroutine`类对应于`jsr`这条指令，而`jsr`指令是处于“以前确实有用，现在已经过时”的状态。



# ASM与ClassFile

## ClassFile

我们都知道，在`.class`文件中，存储的是ByteCode数据。但是，这些ByteCode数据并不是杂乱无章的，而是遵循一定的数据结构。

![From Java to Class](ASM.assets/javac-from-dot-java-to-dot-class.jpeg)

这个`.class`文件遵循的数据结构就是由[Java Virtual Machine Specification](https://docs.oracle.com/javase/specs/jvms/se8/html/index.html)中定义的 [The class File Format](https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-4.html)，如下所示。

```
ClassFile {
    u4             magic;
    u2             minor_version;
    u2             major_version;
    u2             constant_pool_count;
    cp_info        constant_pool[constant_pool_count-1];
    u2             access_flags;
    u2             this_class;
    u2             super_class;
    u2             interfaces_count;
    u2             interfaces[interfaces_count];
    u2             fields_count;
    field_info     fields[fields_count];
    u2             methods_count;
    method_info    methods[methods_count];
    u2             attributes_count;
    attribute_info attributes[attributes_count];
}
```

## 字节码类库

ASM是操作字节码的类库，但不是唯一的，还有很多其他操作字节码的类库

- Apache Commons BCEL:其中BCEL为 Byte Code Engineering Library首字母的缩写
-  Javassist: Javassist表示 Java programming assistant· 
- ObjectWeb ASM:本课程的研究对象。
- Byte Buddy:在ASM基础上实现的一个类库。
- CGLIB 基于ASM开发的类库

那么,字节码的类库和 ClassFle之间是什么样的关系呢?我们可以用下图来表示

![image-20210630233427253](ASM.assets/image-20210630233427253.png)

对于上图,我们用三句来描述它们的关系

- 中间层-多个.class文件,虽然每个类里面的内容各不相同,但它们里面的内容都称为字节码( Byte Code)。
- 中下层-不论这些.class文件内容有怎样大的差异,它们都共同遵守同一个数据结构,即 Classfile 
- 中上层-为了方便于人们对于字节码( Byte Code)内容的操作,逐渐衍生出了各种操作字节码的类库。
- 上下层-不考虑中间层,我们可以说,不同的字节码类库是在同一个 Classfile结构上发展起来的。

既然有多个可以选择的字节码类库,那么我们为什么要选择ASM呢?这就得看ASM自身所有的特点,或者说与众不同的地方

**ASM特点**

与其它的操作]ava字节码的类库相比,ASM有哪些与众不同的地方呢?·

在实现相同的功前前提下,使用ASM,**运行速度更快**(运行时间短,崖于时间维度”),**占用的内存空间更小**(内存空间,厘于空间维度”)。

ASM与ClassFile的关系

为了大家更直观的理解ASM与 ClassFile之间关系,我们用下图来表示。其中, Java ClassFile相当于树根"部分, Objectweb ASM相当于树干"部分,而ASM的各种应用场景属于树枝"或树叶部分。

<img src="ASM.assets/image-20210630233817962.png" alt="image-20210630233817962" style="zoom:80%;" />

学习ASM有三个不同的层次

- 第一个层次：ASM的应用层次，使用ASM来做什么呢?对于一个c1ass文件来说,我们可以使用ASM进行 analysis、 generation和 transformation操作。
- 第二ASM的源码层面。也就是,ASM的代码组织形式,它为分 Core api和 Tree API的内容。
- 第三个层次, Java ClassFile层面。从JVM规范的角度,来理解.c1ass文件的结构,来理解ASM中方法和参数的含义。

![image-20210630234029082](ASM.assets/image-20210630234029082.png)

本文主要围绕看” Classfile"和"ASM"展开,内容总结如下·第一点,具体的,c1ass文件遵循C1 assfile的结构。·第二点,操作字节码( Byte Code)的类库有多个。ASM是其中一种,它的特点是执行速度快、占用空间小·第三点,ASM与 Classfile之间的关系。形象的来说, Classfile相当于是树根”,而AsM相当于是”树干”,而AsM的应用场景相当于"树枝”或"树叶"。

本文的重点是理解ASM与 Classfile之间的关系”,也不需要记忆任何内容

正所谓”君子务本,本立而道生”, Classfile是根本",而ASM是在 Classfile这个根本上所衍生出来的一条修改字节码( Byte Code)道路或途径。

# ClassFile快速参考

## Java ClassFile

对于一个具体的`.class`而言，它是遵循ClassFile结构的。这个数据结构位于[Java Virtual Machine Specification](https://docs.oracle.com/javase/specs/jvms/se8/html/index.html)的 [The class File Format](https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-4.html)部分。

```
ClassFile {
    u4             magic;
    u2             minor_version;
    u2             major_version;
    u2             constant_pool_count;
    cp_info        constant_pool[constant_pool_count-1];
    u2             access_flags;
    u2             this_class;
    u2             super_class;
    u2             interfaces_count;
    u2             interfaces[interfaces_count];
    u2             fields_count;
    field_info     fields[fields_count];
    u2             methods_count;
    method_info    methods[methods_count];
    u2             attributes_count;
    attribute_info attributes[attributes_count];
}
```

其中，

- `u1`: 表示占用1个字节
- `u2`: 表示占用2个字节
- `u4`: 表示占用4个字节
- `u8`: 表示占用8个字节

而`cp_info`、`field_info`、`method_info`和`attribute_info`表示较为复杂的结构，但它们也是由`u1`、`u2`、`u4`和`u8`组成的。

相应的，在`.class`文件当中，定义的字段，要遵循`field_info`的结构。

```
field_info {
    u2             access_flags;
    u2             name_index;
    u2             descriptor_index;
    u2             attributes_count;
    attribute_info attributes[attributes_count];
}
```

同样的，在`.class`文件当中，定义的方法，要遵循`method_info`的结构。

```
method_info {
    u2             access_flags;
    u2             name_index;
    u2             descriptor_index;
    u2             attributes_count;
    attribute_info attributes[attributes_count];
}
```

在`method_info`结构中，方法当中方法体的代码，是存在于`Code`属性结构中，其结构如下：

```
Code_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u2 max_stack;
    u2 max_locals;
    u4 code_length;
    u1 code[code_length];
    u2 exception_table_length;
    {   u2 start_pc;
        u2 end_pc;
        u2 handler_pc;
        u2 catch_type;
    } exception_table[exception_table_length];
    u2 attributes_count;
    attribute_info attributes[attributes_count];
}
```

## 示例演示

假如，我们有一个`sample.HelloWorld`类，它的内容如下：

```java
package sample;

public class HelloWorld implements Cloneable {
    private static final int intValue = 10;

    public void test() {
        int a = 1;
        int b = 2;
        int c = a + b;
    }
}
```

针对`sample.HelloWorld`类，我们可以

- 第一，运行`run.A_File_Hex`类，查看`sample.HelloWorld`类当中包含的数据，以十六进制进行呈现。
- 第二，运行`run.B_ClassFile_Raw`类，能够对`sample.HelloWorld`类当中包含的数据进行拆分。这样做的目的，是为了与ClassFile的结构进行对照，进行参考。
- 第三，运行`run.I_Attributes_Method`类，能够对`sample.HelloWorld`类当中方法的Code属性结构进行查看。
- 第四，运行`run.K_Code_Locals`类，能够对`sample.HelloWorld`类当中`Code`属性包含的instruction进行查看。

##  总结

本文主要是对Java ClassFile进行了介绍，内容总结如下：

- 第一点，一个具体的`.class`文件，它是要遵循ClassFile结构的；而ClassFile的结构是定义在[The Java Virtual Machine Specification](https://docs.oracle.com/javase/specs/jvms/se8/html/index.html)文档中。
- 第二点，示例演示，主要是希望大家能够把`.class`文件里的内容与ClassFile结构之间对应起来。

在后续内容当中，我们会讲到从“无”到“有”的生成新的Class文件，以及对已有的Class文件进行转换；此时，我们对ClassFile进行介绍，目的就是为了对生成Class或转换Class的过程有一个较深刻的理解。

# 如何编写ASM代码

在刚开始学习ASM的时候，编写ASM代码是不太容易的。或者，有些人原来对ASM很熟悉，但由于长时间不使用ASM，编写ASM代码也会有一些困难。在本文当中，我们介绍一个`ASMPrint`类，它能帮助我们将`.class`文件转换为ASM代码，这个功能非常实用。

## ASMPrint

下面是`ASMPrint`类的代码，它是利用`org.objectweb.asm.util.TraceClassVisitor`类来实现的。在使用的时候，我们注意修改一下`className`、`parsingOptions`和`asmCode`参数就可以了。

```java
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.util.ASMifier;
import org.objectweb.asm.util.Printer;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceClassVisitor;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * 这里的代码是参考自{@link org.objectweb.asm.util.Printer#main}
 */
public class ASMPrint {
    public static void main(String[] args) throws IOException {
        // (1) 设置参数
        String className = "sample.HelloWorld";
        int parsingOptions = ClassReader.SKIP_FRAMES | ClassReader.SKIP_DEBUG;
        boolean asmCode = true;

        // (2) 打印结果
        Printer printer = asmCode ? new ASMifier() : new Textifier();
        PrintWriter printWriter = new PrintWriter(System.out, true);
        TraceClassVisitor traceClassVisitor = new TraceClassVisitor(null, printer, printWriter);
        new ClassReader(className).accept(traceClassVisitor, parsingOptions);
    }
}
```

在现在阶段，我们可能并不了解这段代码的含义，没有关系的。现在，我们主要是使用这个类，来帮助我们生成ASM代码；等后续内容中，我们会介绍到`TraceClassVisitor`类，也会讲到`ASMPrint`类的代码，到时候就明白这段代码的含义了。



## ASMPrint类使用示例

假如，有如下一个`HelloWorld`类：

```java
public class HelloWorld {
    public void test() {
        System.out.println("Test Method");
    }
}
```

对于`ASMPrint`类来说，其中

- `className`值设置为类的全限定名，可以是我们自己写的类，例如`sample.HelloWorld`，也可以是JDK自带的类，例如`java.lang.Comparable`。
- `asmCode`值设置为`true`或`false`。如果是`true`，可以打印出对应的ASM代码；如果是`false`，可以打印出方法对应的Instruction。
- `parsingOptions`值设置为`ClassReader.SKIP_CODE`、`ClassReader.SKIP_DEBUG`、`ClassReader.SKIP_FRAMES`、`ClassReader.EXPAND_FRAMES`的组合值，也可以设置为`0`，可以打印出详细程度不同的信息。

## 总结

本文主要介绍了`ASMPrint`类和它的使用示例，内容总结如下：

- 第一点，`ASMPrint`类，是通过`org.objectweb.asm.util.TraceClassVisitor`实现的。
- 第二点，`ASMPrint`类的作用，是帮助我们生成ASM代码。当我们想实现某一个功能时，不知道如何下手，可以使用`ASMPrint`类生成的ASM代码，作为思考的起点。

在当前的阶段，我们可能并不了解`ASMPrint`类里面代码的含义，但是并不影响我们使用它，让它来帮助我们生成ASM代码。在后续的课程当中，我们会逐步的介绍Core API的内容，到时候就能够去理解代码的含义了。
