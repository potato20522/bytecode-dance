# MybatisPlus命名规则改造计划

## 需求

目标：在源码层面实体类无注解

### 表名与类名映射

表名：

 通用规则 :

"MB_Student"《===》 StudentEntity,注意psql中表名只要包含大写字母，就必须加双引号"MB_Student"

自定义命名映射：MB_Class 《===》KlassEntity





![image-20211219112426892](img/MybatisPlus命名规则改造计划.assets/image-20211219112426892.png)