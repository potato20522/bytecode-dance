package com.potato.mock.random;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class RandomEnum {
    private static final ThreadLocalRandom random = ThreadLocalRandom.current();

    public static <E extends Enum<E>> E next(Class<E> clazz) {
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    public static <E extends Enum<E>> E nextInclude(Class<E> clazz, List<E> includes) {
        int x = random.nextInt(0, includes.size());
        return includes.get(x);
    }

    public static <E extends Enum<E>> E nextExclude(Class<E> clazz, List<E> excludes) {
        List<E> list = Arrays.asList(clazz.getEnumConstants());
        list.removeAll(excludes);
        int x = random.nextInt(clazz.getEnumConstants().length);
        return list.get(x);
    }


}
