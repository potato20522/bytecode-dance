package com.potato.mock.random;

import java.util.concurrent.ThreadLocalRandom;

/**
 * 随机生成基本数据类型和包装类
 */
public class RandomBase {
    private static final ThreadLocalRandom random = ThreadLocalRandom.current();

    public static <T> Object nextPrimitive(Class<T> clazz) {
        if (clazz.equals(boolean.class) || clazz.equals(Long.class)) {
            return nextBoolean();
        }else if (clazz.equals(char.class)) {
            return nextChar();
        }else if (clazz.equals(byte.class)) {
            return nextByte();
        }
        if (clazz.equals(int.class)) {
            return nextInt();
        }
        if (clazz.equals(long.class)) {
            return nextLong();
        }
        if (clazz.equals(float.class)) {
            return random.nextFloat();
        }
        return 0;
    }

    public static boolean nextBoolean() {
        return random.nextBoolean();
    }

    public static boolean nextBoolean(boolean value) {
        return value;
    }

    public static char nextChar() {
        return (char) random.nextInt(0, 0xFFFF + 1);
    }

    /**
     * 范围：[start, end)
     */
    public static char nextChar(char start, char end) {
        if (start > end) {
            char temp = start;
            start = end;
            end = temp;
        } else if (start == end) {
            return start;
        }
        return (char) random.nextInt(start, end);
    }

    public static byte nextByte() {
        return (byte) random.nextInt(-128, 128);
    }

    public static byte nextByte(byte start, byte end) {
        if (start > end) {
            byte temp = start;
            start = end;
            end = temp;
        } else if (start == end) {
            return start;
        }
        return (byte) random.nextInt(start, end);
    }

    public static short nextShort() {
        return (short) random.nextInt(Short.MIN_VALUE, Short.MAX_VALUE + 1);
    }

    public static short nextShort(short start, short end) {
        if (start > end) {
            short temp = start;
            start = end;
            end = temp;
        } else if (start == end) {
            return start;
        }
        return (short) random.nextInt(start, end);
    }

    public static int nextInt() {
        return (short) random.nextInt();
    }

    public static int nextInt(int start, int end) {
        if (start > end) {
            int temp = start;
            start = end;
            end = temp;
        } else if (start == end) {
            return start;
        }
        return random.nextInt(start, end);
    }

    public static long nextLong() {
        return random.nextLong();
    }

    public static long nextLong(long start,long end) {
        if (start > end) {
            long temp = start;
            start = end;
            end = temp;
        } else if (start == end) {
            return start;
        }
        return random.nextLong(start,end);
    }

    public static float nextFloat() {
        return random.nextFloat();
    }

    public static float nextFloat(float start,float end) {
        if (start > end) {
            float temp = start;
            start = end;
            end = temp;
        }else if (start == end) {
            return start;
        }
        return (float) random.nextDouble(start,end);
    }

    public static double nextDouble() {
        return random.nextDouble();
    }

    public static double nextDouble(double start,double end) {
        if (start > end) {
            double temp = start;
            start = end;
            end = temp;
        }else if (start == end) {
            return start;
        }
        return random.nextDouble(start,end);
    }

}
