package com.potato.mock.type;

/**
 * 暂时只支持ArrayList、HashMap、HashSet
 * 对于POJO来说，够用了吧
 */
public enum CollectionType {
    ARRAY_LIST,HASH_MAP,HASH_SET;
}
