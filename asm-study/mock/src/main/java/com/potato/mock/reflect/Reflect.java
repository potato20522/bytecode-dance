package com.potato.mock.reflect;

import lombok.SneakyThrows;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;

@SuppressWarnings("unchecked")
public class Reflect {

    /**
     * 反射创建对象
     * @param clazz 不能是接口或抽象类
     */
    @SneakyThrows
    public static <T> T newInstance(Class<T> clazz) {
        if (clazz.isInterface() || Modifier.isAbstract(clazz.getModifiers())) {
            return null;
        }
        Constructor<?>[] constructors = clazz.getDeclaredConstructors();
        for (Constructor<?> constructor : constructors) {
            constructor.setAccessible(true);
            if (constructor.getParameterCount() != 0) {
                continue;
            }
            return (T) constructor.newInstance();
        }
        //没有无参构造就随便找一个
        for (Constructor<?> constructor : constructors) {
            constructor.setAccessible(true);
            for (Parameter parameter : constructor.getParameters()) {

            }
        }
        return null;
    }
}
