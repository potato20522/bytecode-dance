package com.potato.mock.random;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RandomString {
    private static final ThreadLocalRandom random = ThreadLocalRandom.current();

    private static final String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String next() {
        return next(random.nextInt(30));
    }

    public static String next(int length) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }
}
