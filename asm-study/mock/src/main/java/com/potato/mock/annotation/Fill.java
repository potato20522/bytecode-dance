package com.potato.mock.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * 优先级从高到低：
 * nil、zero、depth、endNil、endZero
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({TYPE, FIELD, LOCAL_VARIABLE})
public @interface Fill {
    /**
     * 是否填充null，优先级高于depth、 fillZero
     * 第一层深度
     */
    boolean nil() default false;

    /**
     * 是否填充0值，
     * 集合、数组填充空(size=0)，包装类给0值
     * 优先级高于depth
     * 第一层深度
     */
    boolean zero() default false;

    /**
     * 数据填充的深度（大于1的整数），不支持循环引用的填充
     */
    byte depth() default 3;

    /**
     * 是否填充null，最后一层（取决于depth和POJO的深度）
     */
    boolean endNil() default false;

    /**
     * 是否填充0值，最后一层（取决于depth和POJO的深度）
     */
    boolean endZero() default false;

}
