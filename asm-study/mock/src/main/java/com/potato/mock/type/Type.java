package com.potato.mock.type;

import java.util.*;

/**
 * 包含基本类型和包装类型、字符串、枚举、数组、时间日期、集合
 */
public enum Type {
    BASE, //8种基本数据类型及其包装类
    STRING,//字符串
    ENUM,//枚举
    DATETIME,//各种时间和日期
    ARRAY,//数组
    COLLECTION,//集合
    OBJECT;//兜底

    public static <T> Type who(Class<T> clazz) {
        if (isPrimitiveOrWrapper(clazz)) {
            return BASE;
        } else if (clazz.equals(String.class) || clazz.equals(CharSequence.class)) {
            return STRING;
        } else if (clazz.equals(Enum.class)) {
            return ENUM;
        } else if (isDateTime(clazz)) {
            return DATETIME;
        } else if (clazz.isArray()) {
            return ARRAY;
        } else if (isCollection(clazz)) {
            return COLLECTION;
        } else {
            return OBJECT;
        }
    }

    public static <T> boolean isPrimitive(Class<T> clazz) {
        return clazz.isPrimitive();
    }

    public static <T> boolean isWrapper(Class<T> clazz) {
        Class<?>[] classes = {Boolean.class, Character.class, Byte.class, Short.class,
                Integer.class, Long.class, Float.class, Double.class};
        return Arrays.asList(classes).contains(clazz);
    }

    public static <T> boolean isPrimitiveOrWrapper(Class<T> clazz) {
        return clazz.isPrimitive() || isWrapper(clazz);
    }

    public static <T> boolean isArray(Class<T> clazz) {
        return clazz.isArray();
    }

    public static <T> boolean isCollection(Class<T> clazz) {
        return Collection.class.isAssignableFrom(clazz);
    }

    public static <T> boolean isDateTime(Class<T> clazz) {
        Class<?>[] classes = {
                java.util.Date.class, java.util.Calendar.class,
                java.sql.Time.class, java.sql.Date.class,
                java.sql.Timestamp.class, java.time.LocalDateTime.class,
                java.time.LocalTime.class, java.time.LocalDate.class
        };
        return Arrays.asList(classes).contains(clazz);
    }
}
