package com.potato.mock.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Wrapper {
    private Boolean booleanWrapper;
    private Character charWrapper;
    private Byte byteWrapper;
    private Short shortWrapper;
    private Integer intWrapper;
    private Long longWrapper;
    private Float floatWrapper;
    private Double doubleWrapper;
}
