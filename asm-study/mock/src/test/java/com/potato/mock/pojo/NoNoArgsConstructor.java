package com.potato.mock.pojo;

public class NoNoArgsConstructor {
    private int a;

    public NoNoArgsConstructor(int a) {
        this.a = a;
    }

    @Override
    public String toString() {
        return "NoNoArgsConstructor{" +
                "a=" + a +
                '}';
    }
}
