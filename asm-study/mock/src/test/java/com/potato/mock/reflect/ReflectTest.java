package com.potato.mock.reflect;

import com.potato.mock.pojo.NoNoArgsConstructor;
import com.potato.mock.pojo.PrivateConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReflectTest {

    @Test
    void newInstance() {
        PrivateConstructor instance = Reflect.newInstance(PrivateConstructor.class);
        Assertions.assertNotNull(instance);

        Assertions.assertThrows(IllegalArgumentException.class, () -> Reflect.newInstance(NoNoArgsConstructor.class));
    }
}