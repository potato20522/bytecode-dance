package com.potato.mock.pojo;

public class PrivateConstructor {
    public int a;

    public PrivateConstructor(int a) {
        this.a = a;
    }

    private PrivateConstructor() {
    }
}
