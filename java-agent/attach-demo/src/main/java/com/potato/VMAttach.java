package com.potato;

import com.sun.tools.attach.*;
import com.sun.tools.attach.spi.AttachProvider;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class VMAttach {
    public static void main(String[] args) throws IOException, AttachNotSupportedException,
            AgentLoadException, AgentInitializationException {
        // 注意：需要修改pid的值
        String pid = "23176";
        String agentPath = "E:\\Gitee\\bytecode-dance\\java-agent\\attach-demo\\target\\TheAgent.jar";
        VirtualMachine vm = VirtualMachine.attach(pid);
        vm.loadAgent(agentPath, "Hello JVM Attach");

        //获取目标虚拟机中的 System Properties
        Properties systemProperties = vm.getSystemProperties();
        systemProperties.list(System.out);

        System.out.println("=====");

        //获取目标虚拟机中的 Agent Properties
        Properties agentProperties = vm.getAgentProperties();
        agentProperties.list(System.out);

        System.out.println("=====");


        vm.detach();
    }

    @Test
    public void main2() throws IOException, AttachNotSupportedException {
        List<VirtualMachineDescriptor> list = VirtualMachine.list();

        for (VirtualMachineDescriptor vmd : list) {
            String id = vmd.id();
            String displayName = vmd.displayName();
            AttachProvider provider = vmd.provider();
            System.out.println("Id: " + id);
            System.out.println("Name: " + displayName);
            System.out.println("Provider: " + provider);
            System.out.println("=====================");
        }
    }

    @Test
    public void main3() throws IOException, AttachNotSupportedException {
        List<VirtualMachineDescriptor> list = VirtualMachine.list();

        String className = "sample.Program";
        VirtualMachine vm = null;
        for (VirtualMachineDescriptor item : list) {
            String displayName = item.displayName();
            if (displayName != null && displayName.equals(className)) {
                vm = VirtualMachine.attach(item);
                break;
            }
        }

        if (vm != null) {
            Properties properties = vm.getSystemProperties();
            properties.list(System.out);
            vm.detach();
        }
    }

    @Test
    public void main() throws Exception {
        // 注意：需要修改pid的值
        String pid = "23176";
        VirtualMachine vm = VirtualMachine.attach(pid);
        AttachProvider provider = vm.provider();
        String name = provider.name();
        String type = provider.type();
        System.out.println("Provider Name: " + name);
        System.out.println("Provider Type: " + type);
        System.out.println("Provider Impl: " + provider.getClass());
    }

}