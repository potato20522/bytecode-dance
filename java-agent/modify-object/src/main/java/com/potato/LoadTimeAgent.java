package com.potato;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

import java.lang.instrument.ClassDefinition;
import java.lang.instrument.Instrumentation;

public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        try {
            Class<?> clazz = Object.class;
            if (inst.isModifiableClass(clazz)) {
                ClassReader cr = new ClassReader(clazz.getName());
                ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
                ClassVisitor cv = new ToStringVisitor(cw, "我是修改后的ToString");
                int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
                cr.accept(cv, parsingOptions);
                ClassDefinition classDefinition = new ClassDefinition(clazz,  cw.toByteArray());
                inst.redefineClasses(classDefinition);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}