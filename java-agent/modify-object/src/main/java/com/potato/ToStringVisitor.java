package com.potato;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.commons.AdviceAdapter;

import static org.objectweb.asm.Opcodes.ASM9;

public class ToStringVisitor extends ClassVisitor {
    private static boolean hasModified = false;
    private static String input;

    public ToStringVisitor(ClassWriter cw, String input) {
        super(ASM9,cw);
        ToStringVisitor.input = input;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);
        if ("toString".equals(name) && !hasModified) {
            hasModified = true;
            return new ToStringMethodVisitor(mv, access, name, descriptor);
        }
        return mv;
    }

    static class ToStringMethodVisitor extends AdviceAdapter {
        protected ToStringMethodVisitor(MethodVisitor mv, int access, String name, String descriptor) {
            super(ASM9, mv, access, name, descriptor);
        }

        @Override
        protected void onMethodEnter() {
            mv.visitLdcInsn(input);//从常量池加载字符串
            mv.visitInsn(ARETURN);//返回
        }
    }
}


