# java.lang.instrument包

Java 1.5引入的。

- 定义了一些“规范”，比如Manifest当中的和属性，再比如和方法，这些“规范”是Agent Jar必须遵守的。Premain-Class  Agent-Class  premain  agentmain
- 定义了一些类和接口，例如和，这些类和接口允许我们在Agent Jar当中实现修改某些类的字节码。Instrumentation  ClassFileTransformer

举个形象化的例子。这些“规范”让一个普通的文件（普通士兵）成为Agent Jar（禁卫军）； 接着，Agent Jar（禁卫军）就可以在target JVM（紫禁城）当中对Application当中加载的类（平民、官员）进行instrumentation（巡查守备）任务了。它让一个普通的Jar文件成为一个Agent Jar

instrumentation的机制就是修改**方法**的字节码

```
instrumentation = modification of the byte-codes of methods
```

- 在Java 8版本中，位于文件`rt.jar`
- 在Java 9版本之后，位于模块（）`java.instrument JDK_HOME/jmods/java.instrument.jmod`



包里有这些类：

- ClassDefinition (类)
- ClassFileTransformer (接口)
- Instrumentation (接口)
- IllegalClassFormatException (异常)
- UnmodifiableClassException (异常)

异常没啥好说的，主要看前面三个

# ClassDefinition类

这个类非常简单，主要是对**Class对象**和类的**字节码**进行的封装

```java
public final class ClassDefinition {
    
    private final Class<?> mClass;
    private final byte[]   mClassFile;

    public ClassDefinition(Class<?> theClass,byte[]  theClassFile) {
        if (theClass == null || theClassFile == null) {
            throw new NullPointerException();
        }
        mClass      = theClass;
        mClassFile  = theClassFile;
    }
    
    public Class<?> getDefinitionClass() {
        return mClass;
    }
    
    public byte[] getDefinitionClassFile() {
        return mClassFile;
    }
}
```

# ClassFileTransformer接口

作用：在JVM加载类之前，对类进行修改

如何使用：实现此接口的transform()，并在premain或agentmain中注册对象

transform方法定义如下：

```java
public interface ClassFileTransformer {
    byte[] transform(ClassLoader         loader,
                     String              className, 
                     Class<?>            classBeingRedefined,
                     ProtectionDomain    protectionDomain,
                     byte[]              classfileBuffer)
        throws IllegalClassFormatException;
}
```

- loader：类加载器，为null时，使用bootstrap loader
- `className`: 表示internal class name，例如。`java/util/List`
- `classfileBuffer`: 一定不要修改它的原有内容，可以复制一份，在复制的基础上就进行修改。

举个例子：

自定义类文件转换器，通过ASM修改MyBizMain类字节码

```java

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

import java.lang.instrument.ClassFileTransformer;
import java.security.ProtectionDomain;

import static org.objectweb.asm.Opcodes.ASM9;

public class MyClassFileTransformer implements ClassFileTransformer {
    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) {
        System.out.println("transform :"+className);
        if (!"com/potato/biz/MyBizMain".equals(className)) return classfileBuffer;
        //以下为ASM常规操作，详情可以查看ASM使用相关文档
        ClassReader cr = new ClassReader(classfileBuffer);
        ClassWriter cw = new ClassWriter(cr,ClassWriter.COMPUTE_FRAMES);
        System.out.println("className:"+className);
        ClassVisitor cv = new MyClassVisitor(ASM9,cw);
        cr.accept(cv,ClassReader.SKIP_FRAMES | ClassReader.SKIP_DEBUG);
        return cw.toByteArray();
    }
}
```

在agentmain中注册：

```java
public class MyBizAgentMain {
    public static void agentmain(String agentArgs, Instrumentation inst) throws UnmodifiableClassException {
        System.out.println("---agent called---");
        inst.addTransformer(new MyClassFileTransformer(),true);//添加类文件转换器，第二个参数必须设置为true，表示可以重新转换类文件
        Class[] classes = inst.getAllLoadedClasses();
        for (Class aClass : classes) {
            if ("com.potato.biz.MyBizMain".equals(aClass.getName())) {
                System.out.println("----重新加载MyBizMain开始----");
                inst.retransformClasses(aClass);
                System.out.println("----重新加载MyBizMain完毕----");
                break;
            }
        }
    }

}
```

# Instrumentation接口

作用：检测类的加载行为对其进行干扰（修改替换）

在这两个地方用到：

```java
public static void premain(String agentArgs, Instrumentation inst){

}

public static void agentmain(String agentArgs, Instrumentation inst) {

}

```

Agent Class --> Instrumentation --> ClassFileTransformer

三个类之间更详细的关系如下：

```
               ┌───   premain(String agentArgs, Instrumentation inst)
Agent Class ───┤
               └─── agentmain(String agentArgs, Instrumentation inst)

                   ┌─── void addTransformer(ClassFileTransformer transformer, boolean canRetransform)
Instrumentation ───┤
                   └─── boolean removeTransformer(ClassFileTransformer transformer)
```

## isXxxSupported

读取文件中的属性信息：`META-INF/MANIFEST.MF`

```java
public interface Instrumentation {
    boolean isRedefineClassesSupported();
    boolean isRetransformClassesSupported();
    boolean isNativeMethodPrefixSupported();
}
```

- 第一点，判断某一个isXxxSupported()方法是否为true，需要考虑两个因素：
  - 判断JVM是否支持该功能。
  - 判断Agent.jar内的MANIFEST.MF文件里的属性是否为true。
- 第二点，在一个JVM实例当中，多次调用某个isXxxSupported()方法，该方法的返回值是不会改变的。

```java
import java.lang.instrument.Instrumentation;

public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        System.out.println("Premain-Class: " + LoadTimeAgent.class.getName());
        System.out.println("Can-Redefine-Classes: " + inst.isRedefineClassesSupported());
        System.out.println("Can-Retransform-Classes: " + inst.isRetransformClassesSupported());
        System.out.println("Can-Set-Native-Method-Prefix: " + inst.isNativeMethodPrefixSupported());
    }
}
```

## transform

### xxxTransformer

添加和删除：`ClassFileTransformer`，

```java
public interface Instrumentation {
    void addTransformer(ClassFileTransformer transformer);
    void addTransformer(ClassFileTransformer transformer, boolean canRetransform);
    boolean removeTransformer(ClassFileTransformer transformer);
}
```

### addTransformer

在Instrumentation当中，定义了两个addTransformer方法，但两者本质上一样的：调用addTransformer(ClassFileTransformer transformer)方法，相当于调用addTransformer(transformer, false)

那么，`addTransformer`方法的`canRetransform`参数起到一个什么样的作用呢？

- 第一点，它影响`transformer`对象**存储的位置**
- 第二点，它影响`transformer`对象**功能的发挥**

关于`transformer`对象存储的位置，我们可以参考`sun.instrument.InstrumentationImpl`源码当中的实现：

```java
public class InstrumentationImpl implements Instrumentation {
    private final TransformerManager mTransformerManager;
    private TransformerManager mRetransfomableTransformerManager;
    
    // 以下是经过简化之后的代码
    public synchronized void addTransformer(ClassFileTransformer transformer, boolean canRetransform) {
        if (canRetransform) {
            mRetransfomableTransformerManager.addTransformer(transformer);
        }
        else {
            mTransformerManager.addTransformer(transformer);
        }
    }    
}
```

- 如果`canRetransform`的值为`true`，我们就将`transformer`对象称为retransformation capable transformer
- 如果`canRetransform`的值为`false`，我们就将`transformer`对象称为retransformation incapable transformer



### removeTransformer

不管是retransformation capable transformer，还是retransformation incapable transformer，都使用同一个`removeTransformer`方法：

```java
public interface Instrumentation {
    boolean removeTransformer(ClassFileTransformer transformer);
}
```

同样，我们可以参考`InstrumentationImpl`类当中的实现：

```java
public class InstrumentationImpl implements Instrumentation {
    private final TransformerManager mTransformerManager;
    private TransformerManager mRetransfomableTransformerManager;
    
    // 以下是经过简化之后的代码
    public synchronized boolean removeTransformer(ClassFileTransformer transformer) {
        TransformerManager mgr = findTransformerManager(transformer);
        if (mgr != null) {
            mgr.removeTransformer(transformer);
            return true;
        }
        return false;
    }  
}
```

在什么时候对`removeTransformer`方法进行调用呢？有两种情况。

第一种情况，想处理的`Class`很明确，那就尽量早的调用`removeTransformer`方法，让`ClassFileTransformer`**影响的范围最小化**。

```java
public class DynamicAgent {
    public static void agentmain(String agentArgs, Instrumentation inst) {
        System.out.println("Agent-Class: " + DynamicAgent.class.getName());
        ClassFileTransformer transformer = new ASMTransformer();
        try {
            inst.addTransformer(transformer, true);
            Class<?> targetClass = Class.forName("sample.HelloWorld");
            inst.retransformClasses(targetClass);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            inst.removeTransformer(transformer);
        }
    }
}
```

第二种情况，想处理的`Class`不明确，可以不调用`removeTransformer`方法。

```java
public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        System.out.println("Premain-Class: " + LoadTimeAgent.class.getName());
        ClassFileTransformer transformer = new InfoTransformer();
        inst.addTransformer(transformer);
    }
}
```

### 调用的时机

当我们将`ClassFileTransformer`添加到`Instrumentation`之后，`ClassFileTransformer`类当中的`transform`方法什么时候执行呢？

```
public interface ClassFileTransformer {
    byte[] transform(ClassLoader         loader,
                     String              className,
                     Class<?>            classBeingRedefined,
                     ProtectionDomain    protectionDomain,
                     byte[]              classfileBuffer)
        throws IllegalClassFormatException;
}
```

首先，对`ClassFileTransformer.transform()`方法调用的时机有三个：

- 类加载的时候
- 调用`Instrumentation.redefineClasses`方法的时候
- 调用`Instrumentation.retransformClasses`方法的时候

在OpenJDK的源码中，`hotspot/src/share/vm/prims/jvmtiThreadState.hpp`文件定义了一个`JvmtiClassLoadKind`结构：

```c++
enum JvmtiClassLoadKind {
    jvmti_class_load_kind_load = 100,
    jvmti_class_load_kind_retransform,
    jvmti_class_load_kind_redefine
};
```

接着，来介绍一下redefine和retransform两个概念，它们与类的加载状态有关系：

- 对于**正在加载的类**进行修改，它属于define和transform的范围。
- 对于**已经加载的类**进行修改，它属于redefine和retransform的范围。

对于已经加载的类（loaded class），redefine侧重于以**“新”换“旧”**，而retransform侧重于**对“旧”的事物进行“修补”。**

```
                               ┌─── define: ClassLoader.defineClass
               ┌─── loading ───┤
               │               └─── transform
class state ───┤
               │               ┌─── redefine: Instrumentation.redefineClasses
               └─── loaded ────┤
                               └─── retransform: Instrumentation.retransformClasses
```

再者，触发的方式不同：

- load，是类在加载的过程当中，JVM内部机制来自动触发。
- redefine和retransform，是我们自己写代码触发。

最后，就是不同的时机（load、redefine、retransform）能够接触到的transformer也不相同：

![img](img/InstrumentationAPI.assets/define-redefine-retransform.png)

## redefineClasses

```java
void redefineClasses(ClassDefinition... definitions)
    throws  ClassNotFoundException, UnmodifiableClassException;
```

作用：修改已加载的类

redefine不会触发类的初始化行为，类的初始化还是按照 JVM 正常流程来走

能力：重新定义方法体、常量池、属性

限制：不能修改原有类声明，即不可以添加、移除、重命名方法和方法和入参，不能更改方法签名或更改继承

注意：在转换之前，不会检查、验证和加载字节码，如果结果字节码出现错误，此方法将抛出异常。而抛出异常将不会有类被重新定义

## retransformClasses

```java
void retransformClasses(Class<?>... classes) 
    throws UnmodifiableClassException;
```

和redefineClasses很像

能力：重新定义方法体、常量池、属性

限制：不能修改旧有的类声明，譬如不能增加属性、不能修改方法声明



redefineClasses和retransformClasses用哪个？

redefineClasses() 适合将新加入的类做修改，而 retransformClasses() 可以将哪些已经加载到内存中的类定义做替换

## loader + class + object

 ClassLoaderSearch

```java
public interface Instrumentation {
    // 1.6
    void appendToSystemClassLoaderSearch(JarFile jarfile);
    // 1.6
    void appendToBootstrapClassLoaderSearch(JarFile jarfile);
}
```



xxxClasses

下面三个方法都与已经加载的相关：`Class`

```java
public interface Instrumentation {
    Class[] getAllLoadedClasses();
    Class[] getInitiatedClasses(ClassLoader loader);
    boolean isModifiableClass(Class<?> theClass);
}
```



Object

```java
public interface Instrumentation {
    long getObjectSize(Object objectToSize);
}
```

## native

```java
public interface Instrumentation {
    boolean isNativeMethodPrefixSupported();
    void setNativeMethodPrefix(ClassFileTransformer transformer, String prefix);
}
```

## module

Java 9引入

```java
public interface Instrumentation {
    boolean isModifiableModule(Module module);
    void redefineModule (Module module,
                         Set<Module> extraReads,
                         Map<String, Set<Module>> extraExports,
                         Map<String, Set<Module>> extraOpens,
                         Set<Class<?>> extraUses,
                         Map<Class<?>, List<Class<?>>> extraProvides);
}
```

# redefineClasses示例一：修改toString方法

pom.xml：

```xml
<dependencies>
    <dependency>
      <groupId>org.ow2.asm</groupId>
      <artifactId>asm</artifactId>
      <version>9.2</version>
    </dependency>
    <dependency>
      <groupId>org.ow2.asm</groupId>
      <artifactId>asm-commons</artifactId>
      <version>9.2</version>
    </dependency>
    <dependency>
      <groupId>com.sun</groupId>
      <artifactId>tools</artifactId>
      <version>1.8</version>
      <scope>system</scope>
      <systemPath>${JAVA_HOME}/lib/tools.jar</systemPath>
    </dependency>
  </dependencies>

  <build>
    <finalName>TheAgent</finalName>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>
        <version>3.2.2</version>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>shade</goal>
            </goals>
            <configuration>
              <transformers>
                <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                  <manifestEntries>
                    <Premain-Class>com.potato.LoadTimeAgent</Premain-Class>
                    <Can-Redefine-Classes>true</Can-Redefine-Classes>
                    <Can-Retransform-Classes>true</Can-Retransform-Classes>
                  </manifestEntries>
                </transformer>
              </transformers>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
```

agent：

```java
package com.potato;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

import java.lang.instrument.ClassDefinition;
import java.lang.instrument.Instrumentation;

public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        try {
            Class<?> clazz = Object.class;
            if (inst.isModifiableClass(clazz)) {
                ClassReader cr = new ClassReader(clazz.getName());
                ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
                ClassVisitor cv = new ToStringVisitor(cw, "我是修改后的ToString");
                int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
                cr.accept(cv, parsingOptions);
                ClassDefinition classDefinition = new ClassDefinition(clazz,  cw.toByteArray());
                inst.redefineClasses(classDefinition);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

ToStringVisitor

```java
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.commons.AdviceAdapter;

import static org.objectweb.asm.Opcodes.ASM9;

public class ToStringVisitor extends ClassVisitor {
    private static boolean hasModified = false;
    private static String input;

    public ToStringVisitor(ClassWriter cw, String input) {
        super(ASM9,cw);
        ToStringVisitor.input = input;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);
        if ("toString".equals(name) && !hasModified) {
            hasModified = true;
            return new ToStringMethodVisitor(mv, access, name, descriptor);
        }
        return mv;
    }

    static class ToStringMethodVisitor extends AdviceAdapter {
        protected ToStringMethodVisitor(MethodVisitor mv, int access, String name, String descriptor) {
            super(ASM9, mv, access, name, descriptor);
        }

        @Override
        protected void onMethodEnter() {
            mv.visitLdcInsn(input);//从常量池加载字符串
            mv.visitInsn(ARETURN);//返回
        }
    }
}
```

目标程序：

```java
public class Program {
    public static void main(String[] args) {
        Object obj = new Object();
        System.out.println(obj);
    }
}
```



第一次运行，直接运行Program类：


```
java.lang.Object@15db9742
```

第二次运行，加上agent

```
-javaagent:E:\Gitee\bytecode-dance\java-agent\example\target\TheAgent.jar
```

运行结果：

```
我是修改后的ToString
```

#  redefineClasses示例二：Hot Swap

**Application**

```java
public class Program {
    public static void main(String[] args) throws Exception {
        HelloWorld instance = new HelloWorld();
        for (int i = 1; i < 20; i++) {
            instance.test(12, 3);
            System.out.println("intValue: " + HelloWorld.intValue);
        }
    }
}
```

```java
public class HelloWorld {
    public static int intValue = 20;

    public void test(int a, int b) {
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        try {
            Thread.sleep(5000);
        } catch (Exception ignored) {
        }
        int c = a * b;
        System.out.println("a * b = " + c);
        System.out.println("============");
        System.out.println();
    }
}
```

**Agent Jar**

```java
import java.lang.instrument.Instrumentation;

public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        Class<?> agentClass = LoadTimeAgent.class;
        System.out.println("===>Premain-Class: " + agentClass.getName());
        System.out.println("ClassLoader: " + agentClass.getClassLoader());
        System.out.println("Thread Id: " + Thread.currentThread().getName() + "@" + Thread.currentThread().getId());
        System.out.println("Can-Redefine-Classes: " + inst.isRedefineClassesSupported());
        System.out.println("Can-Retransform-Classes: " + inst.isRetransformClassesSupported());
        System.out.println("Can-Set-Native-Method-Prefix: " + inst.isNativeMethodPrefixSupported());
        System.out.println("========= ========= =========");

        Thread t = new HotSwapThread("hot-swap-thread", inst);
        t.setDaemon(true);
        t.start();
    }
}
```
利用到 JDK 自带的 **WatchService** api 来监控字节码文件发生改变

Instrumentation 中的redefineClasses方法来热重载类

```java
import java.io.InputStream;
import java.lang.instrument.ClassDefinition;
import java.lang.instrument.Instrumentation;
import java.nio.file.*;
import java.util.List;

import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

public class HotSwapThread extends Thread {
    private final Instrumentation inst;

    public HotSwapThread(String name, Instrumentation inst) {
        super(name);
        this.inst = inst;
    }

    @Override
    public void run() {
        try {
            FileSystem fs = FileSystems.getDefault();
            WatchService watchService = fs.newWatchService();
            // 注意：修改这里的路径信息
            Path watchPath = fs.getPath("E:\\Gitee\\bytecode-dance\\java-agent\\hot-swap\\target\\classes\\com\\potato\\");
            watchPath.register(watchService, ENTRY_MODIFY);
            WatchKey changeKey;
            while ((changeKey = watchService.take()) != null) {
                // Prevent receiving two separate ENTRY_MODIFY events: file modified and timestamp updated.
                // Instead, receive one ENTRY_MODIFY event with two counts.
                Thread.sleep( 50 );

                System.out.println("Thread Id: ===>" + Thread.currentThread().getName() + "@" + Thread.currentThread().getId());
                List<WatchEvent<?>> watchEvents = changeKey.pollEvents();
                for (WatchEvent<?> watchEvent : watchEvents) {
                    // Ours are all Path type events:
                    WatchEvent<Path> pathEvent = (WatchEvent<Path>) watchEvent;

                    Path path = pathEvent.context();
                    WatchEvent.Kind<Path> eventKind = pathEvent.kind();
                    System.out.println(eventKind + "(" + pathEvent.count() +")" + " for path: " + path);
                    String filepath = path.toFile().getCanonicalPath();
                    if (!filepath.endsWith("HelloWorld.class")) continue;

                    Class<?> clazz = Class.forName("com.potato.HelloWorld");
                    if (inst.isModifiableClass(clazz)) {
                        System.out.println("Before Redefine");
                        InputStream in = clazz.getResourceAsStream("HelloWorld.class");
                        int available = in.available();
                        byte[] bytes = new byte[available];
                        in.read(bytes);
                        ClassDefinition classDefinition = new ClassDefinition(clazz, bytes);
                        inst.redefineClasses(classDefinition);
                        System.out.println("After Redefine");
                    }

                }
                changeKey.reset(); // Important!
                System.out.println("Thread Id: <===" + Thread.currentThread().getName() + "@" + Thread.currentThread().getId());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
```

**Run**

```
-javaagent:E:\Gitee\bytecode-dance\java-agent\hot-swap\target\TheAgent.jar
```

当HelloWorld发生修改并编译时，比如改为：

```java
System.out.println("============+++");
```

此时的输出就会变成

```
intValue: 20
a = 12
b = 3
Thread Id: ===>hot-swap-thread@6
ENTRY_MODIFY(1) for path: HelloWorld.class
Before Redefine
After Redefine
Thread Id: <===hot-swap-thread@6
a * b = 36
============

intValue: 20
a = 12
b = 3
a * b = 36
============+++

intValue: 20
a = 12
b = 3
a * b = 36
============+++
```

**细节之处**

- 第一点，`redefineClasses()`方法是对已经加载的类进行以“新”换“旧”操作。
- 第二点，如果某个方法正在执行（active stack frames），修改之后的方法会在下一次执行。
- 第三点，静态初始化（class initialization）不会再次执行，不受`redefineClasses()`方法的影响。
- 第四点，`redefineClasses()`方法的功能是有限的，主要集中在对方法体（method body）的修改。
- 第五点，当`redefineClasses()`方法出现异常的时候，就相当于“什么都没有发生过”，不会对类产生影响。

**总结**

- 第一点，`redefineClasses()`方法可以对Class进行重新定义。
- 第二点，`redefineClasses()`方法的一个使用场景就是fix-and-continue。
- 第三点，使用`redefineClasses()`方法需要注意一些细节。

# retransformClasses示例一：修改toString方法



