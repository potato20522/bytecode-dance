https://blog.csdn.net/ljz2016/article/details/83309599/

ClassFileTransformer接口，更改运行期间的字节码，而且这个更改发生在JVM加载这个类之前



ClassFileTransformer需要添加到Instrumentation实例中才能生效。

获取Instrumentation实例的方法有2种：

1. 虚拟机启动时，通过agent class的premain方法获得
2. 虚拟机启动后，通过agent class的agentmain方法获得



