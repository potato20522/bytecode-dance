package com.potato.asm;

import java.util.EnumSet;

public enum MethodInfo {
    NAME_AND_DESC,
    PARAMETER_VALUES,
    RETURN_VALUE,
    CLASSLOADER,
    STACK_TRACE,
    THREAD_INFO;

    public static final EnumSet<MethodInfo> ALL = EnumSet.allOf(MethodInfo.class);
}