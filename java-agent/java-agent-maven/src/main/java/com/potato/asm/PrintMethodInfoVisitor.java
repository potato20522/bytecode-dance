package com.potato.asm;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.Set;

public class PrintMethodInfoVisitor extends ClassVisitor {
    private static final String ALL = "*";

    private String owner;
    private final String methodName;
    private final String methodDesc;
    private final Set<MethodInfo> flags;

    public PrintMethodInfoVisitor(ClassVisitor classVisitor, Set<MethodInfo> flags) {
        this(classVisitor, ALL, ALL, flags);
    }

    public PrintMethodInfoVisitor(ClassVisitor classVisitor, String methodName, String methodDesc, Set<MethodInfo> flags) {
        super(Const.ASM_VERSION, classVisitor);
        this.methodName = methodName;
        this.methodDesc = methodDesc;
        this.flags = flags;
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(version, access, name, signature, superName, interfaces);
        this.owner = name;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);

        if (mv == null) return mv;

        boolean isAbstract = (access & Opcodes.ACC_ABSTRACT) != 0;
        boolean isNative = (access & Opcodes.ACC_NATIVE) != 0;
        if (isAbstract || isNative) return mv;

        if (name.equals("<init>") || name.equals("<clinit>")) return mv;

        boolean process = false;
        if (ALL.equals(methodName) && ALL.equals(methodDesc)) {
            process = true;
        }
        else if (name.equals(methodName) && ALL.equals(methodDesc)) {
            process = true;
        }
        else if (name.equals(methodName) && descriptor.equals(methodDesc)) {
            process = true;
        }

        if (process) {
            String line = String.format("---> %s.%s:%s", owner, name, descriptor);
            System.out.println(line);
            mv = new PrintMethodInfoStdAdapter(mv, owner, access, name, descriptor, flags);
        }

        return mv;
    }
}