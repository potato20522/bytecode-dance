package com.potato.instrument;

import com.potato.asm.MethodInfo;
import com.potato.asm.PrintMethodInfoVisitor;
import org.objectweb.asm.*;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.*;

public class ASMTransformer implements ClassFileTransformer {
    public static final List<String> ignoredPackages = Arrays.asList("com/", "com/sun/", "java/", "javax/", "jdk/", "com/potato", "org/", "sun/");

    private final String internalName;

    public ASMTransformer(String internalName) {
        Objects.requireNonNull(internalName);
        this.internalName = internalName.replace(".", "/");
    }

    @Override
    public byte[] transform(ClassLoader loader,
                            String className,
                            Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain,
                            byte[] classfileBuffer) throws IllegalClassFormatException {
        if (className == null) return null;

        for (String name : ignoredPackages) {
            if (className.startsWith(name)) {
                return null;
            }
        }
        System.out.println("candidate class: " + className);

        if (className.equals(internalName)) {
            System.out.println("transform class: " + className);
            ClassReader cr = new ClassReader(classfileBuffer);
            ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
            Set<MethodInfo> flags = EnumSet.of(
                    MethodInfo.NAME_AND_DESC,
                    MethodInfo.PARAMETER_VALUES,
                    MethodInfo.RETURN_VALUE);
            ClassVisitor cv = new PrintMethodInfoVisitor(cw, flags);

            int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
            cr.accept(cv, parsingOptions);

            return cw.toByteArray();
        }

        return null;
    }
}