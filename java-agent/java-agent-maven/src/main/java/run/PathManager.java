package run;

import java.io.File;
import java.net.URISyntaxException;

public class PathManager {
    public static String getJarPath() {
        String filepath = null;

        try {
            filepath = new File(PathManager.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath();
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
        }

        if (filepath == null || !filepath.endsWith(".jar")) {
            filepath = System.getProperty("user.dir") + File.separator + "target/TheAgent.jar";
        }

        return filepath.replace(File.separator, "/");
    }
}