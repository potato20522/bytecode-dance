package run;

import java.util.Formatter;

public class LoadTimeInstrumentation {
    public static void main(String[] args) {
        usage();
    }

    public static void usage() {
        String jarPath = PathManager.getJarPath();
        StringBuilder sb = new StringBuilder();
        Formatter fm = new Formatter(sb);
        fm.format("Usage:%n");
        fm.format("    java -javaagent:/path/to/TheAgent.jar sample.Program%n");
        fm.format("Example:%n");
        fm.format("    java -cp ./target/classes/ -javaagent:./target/TheAgent.jar sample.Program%n");
        fm.format("    java -cp ./target/classes/ -javaagent:%s sample.Program", jarPath);
        String result = sb.toString();
        System.out.println(result);
    }
}