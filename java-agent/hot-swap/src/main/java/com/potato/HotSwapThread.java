package com.potato;

import java.io.InputStream;
import java.lang.instrument.ClassDefinition;
import java.lang.instrument.Instrumentation;
import java.nio.file.*;
import java.util.List;

import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

public class HotSwapThread extends Thread {
    private final Instrumentation inst;

    public HotSwapThread(String name, Instrumentation inst) {
        super(name);
        this.inst = inst;
    }

    @Override
    public void run() {
        try {
            FileSystem fs = FileSystems.getDefault();
            WatchService watchService = fs.newWatchService();
            // 注意：修改这里的路径信息
            Path watchPath = fs.getPath("E:\\Gitee\\bytecode-dance\\java-agent\\hot-swap\\target\\classes\\com\\potato\\");
            watchPath.register(watchService, ENTRY_MODIFY);
            WatchKey changeKey;
            while ((changeKey = watchService.take()) != null) {
                // Prevent receiving two separate ENTRY_MODIFY events: file modified and timestamp updated.
                // Instead, receive one ENTRY_MODIFY event with two counts.
                Thread.sleep( 50 );

                System.out.println("Thread Id: ===>" + Thread.currentThread().getName() + "@" + Thread.currentThread().getId());
                List<WatchEvent<?>> watchEvents = changeKey.pollEvents();
                for (WatchEvent<?> watchEvent : watchEvents) {
                    // Ours are all Path type events:
                    WatchEvent<Path> pathEvent = (WatchEvent<Path>) watchEvent;

                    Path path = pathEvent.context();
                    WatchEvent.Kind<Path> eventKind = pathEvent.kind();
                    System.out.println(eventKind + "(" + pathEvent.count() +")" + " for path: " + path);
                    String filepath = path.toFile().getCanonicalPath();
                    if (!filepath.endsWith("HelloWorld.class")) continue;

                    Class<?> clazz = Class.forName("com.potato.HelloWorld");
                    if (inst.isModifiableClass(clazz)) {
                        System.out.println("Before Redefine");
                        InputStream in = clazz.getResourceAsStream("HelloWorld.class");
                        int available = in.available();
                        byte[] bytes = new byte[available];
                        in.read(bytes);
                        ClassDefinition classDefinition = new ClassDefinition(clazz, bytes);
                        inst.redefineClasses(classDefinition);
                        System.out.println("After Redefine");
                    }

                }
                changeKey.reset(); // Important!
                System.out.println("Thread Id: <===" + Thread.currentThread().getName() + "@" + Thread.currentThread().getId());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}