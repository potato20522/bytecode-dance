package com.potato;

import java.lang.instrument.Instrumentation;

public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        Class<?> agentClass = LoadTimeAgent.class;
        System.out.println("===>Premain-Class: " + agentClass.getName());
        System.out.println("ClassLoader: " + agentClass.getClassLoader());
        System.out.println("Thread Id: " + Thread.currentThread().getName() + "@" + Thread.currentThread().getId());
        System.out.println("Can-Redefine-Classes: " + inst.isRedefineClassesSupported());
        System.out.println("Can-Retransform-Classes: " + inst.isRetransformClassesSupported());
        System.out.println("Can-Set-Native-Method-Prefix: " + inst.isNativeMethodPrefixSupported());
        System.out.println("========= ========= =========");

        Thread t = new HotSwapThread("hot-swap-thread", inst);
        t.setDaemon(true);
        t.start();
    }
}