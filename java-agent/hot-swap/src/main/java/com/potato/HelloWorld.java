package com.potato;

public class HelloWorld {
    public static int intValue = 20;

    public void test(int a, int b) {
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        try {
            Thread.sleep(5000);
        } catch (Exception ignored) {
        }
        int c = a * b;
        System.out.println("a * b = " + c);
        System.out.println("============");
        System.out.println();
    }
}