# 概述

## 1.JavaAgent是什么

Java字节码加载到JVM里，JavaAgent可以对其进行修改

.class--->ASM-->Java agent -->jvm

Arthtas和skywalking，ja-netfilter都用到了这项技术

来源：https://lsieun.github.io/java-agent/s01ch01/java-agent-overview.html

Java agent is a powerful tool introduced with Java 5.

> 出现时间：Java 5（2004.09）；到了Java 6的时候（2006.12），有了一些改进；到了Java 9的时候（2017.09），又有一些改进。

在操作系统当中，Java Agent的具体表现形式是一个`.jar`文件。 An agent is deployed as a JAR file.

> 存在形式：jar文件

在Java Agent当中，核心的作用是进行bytecode instrumentation。 The true power of java agents lie on their ability to do the bytecode instrumentation.

> 主要功能：bytecode instrumentation

**The mechanism for instrumentation is modification of the byte-codes of methods.**

```
Java Agent = bytecode instrumentation
```

### 1.1. Instrumentation分类

Instrumentation can be inserted in one of three ways: [Link](https://docs.oracle.com/en/java/javase/11/docs/specs/jvmti.html#bci)

- **Static Instrumentation**: The class file is instrumented before it is loaded into the VM - for example, by creating a duplicate directory of `*.class` files which have been modified to add the instrumentation. This method is extremely awkward and, in general, an agent cannot know the origin of the class files which will be loaded.
- **Load-Time Instrumentation**: When a class file is loaded by the VM, the raw bytes of the class file are sent for instrumentation to the agent. This mechanism provides efficient and complete access to one-time instrumentation.
- **Dynamic Instrumentation**: A class which is already loaded (and possibly even running) is modified. Classes can be modified multiple times and can be returned to their original state. The mechanism allows instrumentation which changes during the course of execution.

其实，这里就是讲了对`.class`文件进行修改（Instrumentation）的三个不同的时机（时间和机会）：没有被加载、正在被加载、已经被加载。

![img](img/JavaAgent三个组成部分.assets/three-ways-of-instrumentation.png)

对于Java Agent这部分内容来说，我们只关注**Load-Time Instrumentation**和**Dynamic Instrumentation**两种情况。

### 1.2. 如何编写代码

上面的Instrumentation分类，只是一个抽象的划分，终究是要落实到具体的代码上：

```
写代码 --> 编译成.class文件 --> 生成jar包
```

那么，编写Java Agent的代码，需要哪些知识呢？需要两方面的知识：

- 一方面，熟悉`java.lang.instrument`相关的API。这些API是我们编写Java Agent的主要依据，是我们关注的重点。
- 另一方面，需要掌握操作字节码的类库，并不是我们关注的重点。比较常用的字节码的类库有：[ASM](https://lsieun.github.io/java/asm/index.html)、ByteBuddy和Javassist。

知识体系：

```
    Java Agent       一个机会：可以修改字节码的机会
------------------
    Java ASM         操作字节码的类库
------------------
    ClassFile        理论基础
```

### 1.3. 如何启动Java Agent

启动Java Agent有两种方式：命令行和Attach机制。

第一种方式，是从命令行（Command Line）启动Java Agent，它对应着Load-Time Instrumentation。

在使用`java`命令时，使用`-javagent`选项：

```
-javaagent:jarpath[=options]
```

具体示例：

```
java -cp ./target/classes/ -javaagent:./target/TheAgent.jar sample.Program
```

第二种方式，是通过虚拟机提供的Attach机制来启动Java Agent，它对应着Dynamic Instrumentation。

```
import com.sun.tools.attach.VirtualMachine;

public class VMAttach {
    public static void main(String[] args) throws Exception {
        // 注意：需要修改pid的值
        String pid = "1234";
        String agentPath = "D:\\git-repo\\learn-java-agent\\target\\TheAgent.jar";
        VirtualMachine vm = VirtualMachine.attach(pid);
        vm.loadAgent(agentPath);
        vm.detach();
    }
}
```

## 2. 如何学习Java Agent

在我们学习Java Agent的过程中，可以从四个层面来着手：

- 第一个层面，用户层面。也就是说，Java Agent是一件事物（野马、怪物），那从用户（人）的角度出发，如何使用（驾驭、打败）它呢？有两种方式，

  - 第一种方式，在启动JVM之前，执行`java`命令时，使用`-javaagent`选项加载Agent Jar，这就是Load-Time Instrumentation
  - 第二种方式，在启动JVM之后，使用Attach机制加载Agent Jar，这就是Dynamic Instrumentation
  
- 第二个层面，**磁盘层面**。也就是说，从磁盘（操作系统）的角度来说，Java Agent就是一个`.jar`文件，它包含哪些主要组成部分。

- 第三个层面，**Java层面**。也就是说，我们在使用Java语言开发Java Agent，主要是利用`java.lang.instrument`包的API来实现一些功能。

- 第四个层面，**JVM层面**。也就是说，Java Agent也是要运行在JVM之上的，它要与JVM进行“沟通”，理解其中一些细节之处，能够帮助我们更好的掌握Java Agent。

![Java Agent的四个层次](img/JavaAgent三个组成部分.assets/java-agent-mindmap.png)

## 3. 总结

本文内容总结如下：

- 第一点，了解Java Agent是什么。Java Agent的核心作用就是进行bytecode Instrumentation。
- 第二点，如何学习Java Agent。在学习Java Agent的过程当中，我们把相关的内容放到四个不同的层面来理解，这样便于形成一个整体的、有逻辑的知识体系。

# 三个主要组成部分

来源：https://lsieun.github.io/java-agent/java-agent-01.html

## 1. 三个主要组成部分

在Java Agent对应的`.jar`文件里，有三个主要组成部分：

- Manifest：

  java-agent用到的META-INF/MANIFEST.MF相关属性：Premain-Class、Agent-Class、Boot-Class-Path、Can-Redefine-Classes、Can-Retransform-Classes等

- Agent Class

- ClassFileTransformer

![Agent Jar中的三个组成部分](img/JavaAgent三个组成部分.assets/agent-jar-three-components.png)

```
                ┌─── Manifest ───────────────┼─── META-INF/MANIFEST.MF
                │
                │                            ┌─── LoadTimeAgent.class: premain
TheAgent.jar ───┼─── Agent Class ────────────┤
                │                            └─── DynamicAgent.class: agentmain
                │
                └─── ClassFileTransformer ───┼─── ASMTransformer.class
```

## 2. Manifest Attributes

首先，在Manifest文件当中，可以定义的属性非常多，但是与Java Agent相关的属性有6、7个。

- 在[Java 8](https://docs.oracle.com/javase/8/docs/api/java/lang/instrument/package-summary.html) 版本当中，定义的属性有6个；
- 在[Java 9](https://docs.oracle.com/javase/9/docs/api/java/lang/instrument/package-summary.html) 至[Java 17](https://docs.oracle.com/en/java/javase/17/docs/api/java.instrument/java/lang/instrument/package-summary.html) 版本当中，定义的属性有7个。其中，`Launcher-Agent-Class`属性，是Java 9引入的。

其次，我们将Manifest定义的属性分成了三组：基础、能力和特殊情况。

```
                                       ┌─── Premain-Class
                       ┌─── Basic ─────┤
                       │               └─── Agent-Class
                       │
                       │               ┌─── Can-Redefine-Classes
                       │               │
Manifest Attributes ───┼─── Ability ───┼─── Can-Retransform-Classes
                       │               │
                       │               └─── Can-Set-Native-Method-Prefix
                       │
                       │               ┌─── Boot-Class-Path
                       └─── Special ───┤
                                       └─── Launcher-Agent-Class
```

分组的目的，是为了便于理解：一下子记住7个属性，不太容易；分成三组，每次记忆两、三个属性，就相对容易一些。

这些分组，也对应着先、后的学习顺序，它也是由简单到复杂的过程。

**注意**：这个分组是我个人的理解，并不一定是对的。如果你有更好的认知方式，可以按自己的思路来。

### 2.1. 基础

An attribute in the JAR file manifest specifies the **agent class** which will be loaded to start the agent.

- `Premain-Class`: 这个属性用来指定JVM**启动时**的代理agent,它必须包含**premain方法**，如果这个属性不存在，则JVM将终止。注意是类的全路径
- `Agent-Class`: 如果agent实现支持在JVM**启动后**某个时间启动代理的机制，那么该属性则指定该代理类。如果该属性不存在，代理将不会启动。

```
Premain-Class: com.potato.agent.LoadTimeAgent
Agent-Class: com.potato.agent.DynamicAgent
```

An agent JAR file may have both the `Premain-Class` and `Agent-Class` attributes present in the manifest.

- When the agent is started on the command-line using the `-javaagent` option then the `Premain-Class` attribute specifies the name of the agent class and the `Agent-Class` attribute is ignored.
- Similarly, if the agent is started sometime after the VM has started, then the `Agent-Class` attribute specifies the name of the agent class (the value of `Premain-Class` attribute is ignored).

### 2.2. 能力

能力，体现在两个层面上：JVM和 Java Agent。

```
                              ┌─── redefine
                              │
           ┌─── Java Agent ───┼─── retransform
           │                  │
           │                  └─── native method prefix
Ability ───┤
           │                  ┌─── redefine
           │                  │
           └─── JVM ──────────┼─── retransform
                              │
                              └─── native method prefix
```

下面三个属性，就是确定Java Agent的能力：

- `Can-Redefine-Classes`: 	可选值就是true false,大小写无所谓，默认false。该属性用来指定该agent是否针对redefineClass产生作用
- `Can-Retransform-Classes`: 可选值就是true false,大小写无所谓，默认false。该属性用来指定该agent是否针对retransformClass产生作用
- `Can-Set-Native-Method-Prefix`:  可选值就是true false,大小写无所谓，默认false。该属性用来指定该agent是否支持native方法前缀设置。

### 2.3. 特殊情况

- `Boot-Class-Path`: A list of paths to be searched by the bootstrap class loader. Paths represent directories or libraries (commonly referred to as JAR or zip libraries on many platforms). These paths are searched by the bootstrap class loader after the platform specific mechanisms of locating a class have failed. Paths are searched in the order listed. Paths in the list are separated by one or more spaces. A path takes the syntax of the path component of a hierarchical URI. The path is absolute if it begins with a slash character (`/`), otherwise it is relative. A relative path is resolved against the absolute path of the agent JAR file. Malformed and non-existent paths are ignored. When an agent is started sometime after the VM has started then paths that do not represent a JAR file are ignored. This attribute is optional.  该属性可以指定BootStrapClassLoad加载的路径(路径需要带上指定哪个文件)，多个路径用空格分开

- `Launcher-Agent-Class`: If an implementation supports a mechanism to start an application as an executable JAR then the main manifest may include this attribute to specify the class name of an agent to start before the application `main` method is invoked.如果agent打包到可执行JAR文件中，可执行JAR文件的清单中必须包含 Launcher-Agent-Class 属性，指定一个在应用main函数调用之前代理启动的类。JVM尝试在代理上调用以下方法：

  ```java
  public static void agentmain(String agentArgs, Instrumentation inst)
  ```

  如果，代理类没有实现上述方法，JVM则调用下面的方法。

  ```java
  public static void agentmain(String agentArgs
  ```

  

## 3. Agent Class

### 3.1. LoadTimeAgent

如果我们想使用Load-Time Instrumentation，那么就必须有一个`premain`方法，它有两种写法。

The JVM first attempts to invoke the following method on the agent class:（推荐使用）

```
public static void premain(String agentArgs, Instrumentation inst);
```

If the agent class does not implement this method then the JVM will attempt to invoke:

```
public static void premain(String agentArgs);
```

### 3.2. DynamicAgent

如果我们想使用Dynamic Instrumentation，那么就必须有一个`agentmain`方法，它有两种写法。

The JVM first attempts to invoke the following method on the agent class:（推荐使用）

```
public static void agentmain(String agentArgs, Instrumentation inst);
```

If the agent class does not implement this method then the JVM will attempt to invoke:

```
public static void agentmain(String agentArgs);
```

## 4. ClassFileTransformer

在`java.lang.instrument`下包含了`Instrumentation`和`ClassFileTransformer`接口：

- `java.lang.instrument.Instrumentation`
- `java.lang.instrument.ClassFileTransformer`

在`Instrumentation`接口中，定义了添加和移除`ClassFileTransformer`的方法：

```
public interface Instrumentation {
    void addTransformer(ClassFileTransformer transformer, boolean canRetransform);

    boolean removeTransformer(ClassFileTransformer transformer);
}
```

在`ClassFileTransformer`接口中，定义了`transform`抽象方法：

```
public interface ClassFileTransformer {
    byte[] transform(ClassLoader         loader,
                     String              className,
                     Class<?>            classBeingRedefined,
                     ProtectionDomain    protectionDomain,
                     byte[]              classfileBuffer) throws IllegalClassFormatException;
            
}
```

当我们想对Class进行bytecode instrumentation时，就要实现`ClassFileTransformer`接口，并重写它的`transform`方法。

## 5. 总结

本文内容总结如下：

- 第一点，了解Agent Jar的三个主要组成部分：Manifest、Agent Class和ClassFileTransformer。
- 第二点，在Agent Jar当中，这些不同的组成部分之间是如何联系在一起的。

```
Manifest --> Agent Class --> Instrumentation --> ClassFileTransformer
```



# Load-Time Agent打印加载的类(手动打包)

来源：https://lsieun.github.io/java-agent/s01ch01/quick-start-example-01.html

手工打包，使用记事本和cmd就行

**预期目标：打印正在加载的类。**

目录结构：

```
src
 ├─── com.potato
 │    ├─── agent
 │    │    └─── LoadTimeAgent.java
 │    └─── instrument
 │         └─── InfoTransformer.java
 ├─── manifest.txt
 └─── sample
      ├─── HelloWorld.java
      └─── Program.java
```



## Application

HelloWorld.java:

```java
package sample;

public class HelloWorld {
    public static int add(int a, int b) {
        return a + b;
    }

    public static int sub(int a, int b) {
        return a - b;
    }
}
```

Program.java:

```java
package sample;

import java.lang.management.ManagementFactory;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Program {
    public static void main(String[] args) throws Exception {
        // (1) print process id
        String nameOfRunningVM = ManagementFactory.getRuntimeMXBean().getName();
        System.out.println(nameOfRunningVM);

        // (2) count down
        int count = 600;
        for (int i = 0; i < count; i++) {
            String info = String.format("|%03d| %s remains %03d seconds", i, nameOfRunningVM, (count - i));
            System.out.println(info);

            Random rand = new Random(System.currentTimeMillis());
            int a = rand.nextInt(10);
            int b = rand.nextInt(10);
            boolean flag = rand.nextBoolean();
            String message;
            if (flag) {
                message = String.format("a + b = %d", HelloWorld.add(a, b));
            }
            else {
                message = String.format("a - b = %d", HelloWorld.sub(a, b));
            }
            System.out.println(message);

            TimeUnit.SECONDS.sleep(1);
        }
    }
}
```

## Agent Jar

manifest.txt

修改`manifest.txt`文件内容：

```
Premain-Class: com.potato.agent.LoadTimeAgent

```
注意：在manifest.txt文件的结尾处有一个空行。

LoadTimeAgent.java

```java
package com.potato.agent;

import com.potato.instrument.InfoTransformer;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;

public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        System.out.println("Premain-Class: " + LoadTimeAgent.class.getName());
        ClassFileTransformer transformer = new InfoTransformer();
        inst.addTransformer(transformer);
    }
}
```

InfoTransformer.java:

```java
package com.potato.instrument;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.Formatter;

public class InfoTransformer implements ClassFileTransformer {
    @Override
    public byte[] transform(ClassLoader loader,
                            String className,
                            Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain,
                            byte[] classfileBuffer) throws IllegalClassFormatException {
        StringBuilder sb = new StringBuilder();
        Formatter fm = new Formatter(sb);
        fm.format("ClassName: %s%n", className);
        fm.format("    ClassLoader: %s%n", loader);
        fm.format("    ClassBeingRedefined: %s%n", classBeingRedefined);
        fm.format("    ProtectionDomain: %s%n", protectionDomain);
        System.out.println(sb.toString());

        return null;
    }
}
```

## 生成Jar包

使用命令行进行编译打包

```cmd
cd hello-world
javac src/sample/*.java -d out
tree /f out
└─sample
        HelloWorld.class
        Program.class
```

运行结果：

```cmd
E:\Gitee\bytecode-dance\java-agent\hello-world>cd out
E:\Gitee\bytecode-dance\java-agent\hello-world\out>java sample.Program
7868@DESKTOP-ULTBUSD
|000| 7868@DESKTOP-ULTBUSD remains 600 seconds
a - b = 4
|001| 7868@DESKTOP-ULTBUSD remains 599 seconds
a + b = 9
|002| 7868@DESKTOP-ULTBUSD remains 598 seconds
a - b = 0
|003| 7868@DESKTOP-ULTBUSD remains 597 seconds
a - b = 7
|004| 7868@DESKTOP-ULTBUSD remains 596 seconds
a - b = 0
```

**打包Agent Jar**

以下使用windows cmd命令：

编译：

```cmd
echo off
cd /d E:\Gitee\bytecode-dance\java-agent\hello-world
for /r %a in (*.java) do echo %a >> sources.txt
type sources.txt

E:\Gitee\bytecode-dance\java-agent\hello-world\src\com\potato\agent\LoadTimeAgent.java
E:\Gitee\bytecode-dance\java-agent\hello-world\src\com\potato\instrument\InfoTransformer.java
E:\Gitee\bytecode-dance\java-agent\hello-world\src\sample\HelloWorld.java
E:\Gitee\bytecode-dance\java-agent\hello-world\src\sample\Program.java

:: 进行编译
javac -d out/ @sources.txt
```

生成Jar包：

```cmd
::复制manifest.txt文件
xcopy /y .\src\manifest.txt .\out

cd out
# 进行打包（第一种方式）
            ┌─── f: TheAgent.jar
         ┌──┴──┐
$ jar -cvfm TheAgent.jar manifest.txt . # .表示当前目录
          └─────────┬────────┘
                    └─── m: manifest.txt
# 进行打包（第二种方式）
                   ┌─── f: TheAgent.jar
          ┌────────┴────────┐
$ jar -cvmf manifest.txt TheAgent.jar .
         └───┬──┘
             └─── m: manifest.txt
```

输出信息：

```
jar -cvfm TheAgent.jar manifest.txt .
已添加清单
正在添加: com/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: com/potato/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: com/potato/agent/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: com/potato/agent/LoadTimeAgent.class(输入 = 1194) (输出 = 615)(压缩了 48%)
正在添加: com/potato/instrument/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: com/potato/instrument/InfoTransformer.class(输入 = 1218) (输出 = 643)(压缩了 47%)
正在添加: manifest.txt(输入 = 43) (输出 = 42)(压缩了 2%)
正在添加: sample/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: sample/HelloWorld.class(输入 = 305) (输出 = 219)(压缩了 28%)
正在添加: sample/Program.class(输入 = 1526) (输出 = 916)(压缩了 39%)
```

## 运行

在使用java命令时，我们可以通过使用-javaagent选项来使用Agent Jar：

```cmd
java -javaagent:TheAgent.jar sample.Program
```

部分输出结果：（打印了正在加载的类）

```
java -javaagent:TheAgent.jar sample.Program
Premain-Class: com.potato.agent.LoadTimeAgent
ClassName: jdk/internal/vm/PostVMInitHook
    ClassLoader: null
    ClassBeingRedefined: null
    ProtectionDomain: null
...
ClassName: jdk/internal/vm/PostVMInitHook$2
    ClassLoader: null
    ClassBeingRedefined: null
    ProtectionDomain: null
ClassName: sample/Program
    ClassLoader: jdk.internal.loader.ClassLoaders$AppClassLoader@383534aa
    ClassBeingRedefined: null
    ProtectionDomain: ProtectionDomain  (file:/E:/Gitee/bytecode-dance/java-agent/hello-world/out/ <no signer certificates>)
 jdk.internal.loader.ClassLoaders$AppClassLoader@383534aa
 <no principals>
 java.security.Permissions@15975490 (
 ("java.io.FilePermission" "E:\Gitee\bytecode-dance\java-agent\hello-world\out\-" "read")
 ("java.lang.RuntimePermission" "exitVM")
)
...
|001| 1312@DESKTOP-ULTBUSD remains 599 seconds
a - b = -3
|002| 1312@DESKTOP-ULTBUSD remains 598 seconds
a - b = -6
|003| 1312@DESKTOP-ULTBUSD remains 597 seconds
a - b = 2
...
```

另外，在使用java命令时，可以添加-verbose:class选项，它可以显示每个已加载类的信息。

```cmd
java -verbose:class sample.Program

```
# Load-Time Agent打印方法接收的参数(手动打包)

来源：https://lsieun.github.io/java-agent/s01ch01/quick-start-example-02.html

预期目标：借助于JDK内置的ASM打印出方法接收的参数，使用Load-Time Instrumentation的方式实现。

```mermaid
graph TB
subgraph JVM
    A(Agent)
    B(Java Application)
    end
style JVM fill: #ebebeb,color:red
```





代码目录结构：

```
java-agent-manual-02
└─── src
     ├─── com.potato
     │    ├─── agent
     │    │    └─── LoadTimeAgent.java
     │    ├─── asm
     │    │    ├─── adapter
     │    │    │    └─── MethodInfoAdapter.java
     │    │    ├─── cst
     │    │    │    └─── Const.java
     │    │    └─── visitor
     │    │         └─── MethodInfoVisitor.java
     │    ├─── instrument
     │    │    └─── ASMTransformer.java
     │    └─── utils
     │         └─── ParameterUtils.java
     ├─── manifest.txt
     └─── sample
          ├─── HelloWorld.java
          └─── Program.java
```

代码逻辑梳理：

```
Manifest --> Agent Class --> Instrumentation --> ClassFileTransformer --> ASM
```

## Application

HelloWorld.java：

```java
package sample;

public class HelloWorld {
    public static int add(int a, int b) {
        return a + b;
    }

    public static int sub(int a, int b) {
        return a - b;
    }
}
```

Program.java:

```java
package sample;

import java.lang.management.ManagementFactory;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Program {
    public static void main(String[] args) throws Exception {
        // (1) print process id
        String nameOfRunningVM = ManagementFactory.getRuntimeMXBean().getName();
        System.out.println(nameOfRunningVM);

        // (2) count down
        int count = 600;
        for (int i = 0; i < count; i++) {
            String info = String.format("|%03d| %s remains %03d seconds", i, nameOfRunningVM, (count - i));
            System.out.println(info);

            Random rand = new Random(System.currentTimeMillis());
            int a = rand.nextInt(10);
            int b = rand.nextInt(10);
            boolean flag = rand.nextBoolean();
            String message;
            if (flag) {
                message = String.format("a + b = %d", HelloWorld.add(a, b));
            }
            else {
                message = String.format("a - b = %d", HelloWorld.sub(a, b));
            }
            System.out.println(message);

            TimeUnit.SECONDS.sleep(1);
        }
    }
}
```

## ASM相关

在这个部分，我们要借助于JDK内置的ASM类库（`jdk.internal.org.objectweb.asm`），来实现打印方法参数的功能。

ParameterUtils.java:

在`ParameterUtils.java`文件当中，主要是定义了各种类型的`print`方法：

```java
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class ParameterUtils {
    private static final DateFormat fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void printValueOnStack(boolean value) {
        System.out.println("    " + value);
    }

    public static void printValueOnStack(byte value) {
        System.out.println("    " + value);
    }

    public static void printValueOnStack(char value) {
        System.out.println("    " + value);
    }

    public static void printValueOnStack(short value) {
        System.out.println("    " + value);
    }

    public static void printValueOnStack(int value) {
        System.out.println("    " + value);
    }

    public static void printValueOnStack(float value) {
        System.out.println("    " + value);
    }

    public static void printValueOnStack(long value) {
        System.out.println("    " + value);
    }

    public static void printValueOnStack(double value) {
        System.out.println("    " + value);
    }

    public static void printValueOnStack(Object value) {
        if (value == null) {
            System.out.println("    " + value);
        }
        else if (value instanceof String) {
            System.out.println("    " + value);
        }
        else if (value instanceof Date) {
            System.out.println("    " + fm.format(value));
        }
        else if (value instanceof char[]) {
            System.out.println("    " + Arrays.toString((char[]) value));
        }
        else if (value instanceof Object[]) {
            System.out.println("    " + Arrays.toString((Object[]) value));
        }
        else {
            System.out.println("    " + value.getClass() + ": " + value.toString());
        }
    }

    public static void printText(String str) {
        System.out.println(str);
    }

    public static void printStackTrace() {
        Exception ex = new Exception();
        ex.printStackTrace(System.out);
    }
}
```

Const.java:

在`Const.java`文件当中，主要是定义了`ASM_VERSION`常量，它标识了使用的ASM的版本：

```java
package com.potato.cst;


import jdk.internal.org.objectweb.asm.Opcodes;

/**
 * 标识了使用的ASM的版本
 */
public class Const {
    public static final int ASM_VERSION = Opcodes.ASM5;
}
```

MethodInfoAdapter.java:

```java
package com.potato.adapter;


import jdk.internal.org.objectweb.asm.MethodVisitor;
import jdk.internal.org.objectweb.asm.Opcodes;
import jdk.internal.org.objectweb.asm.Type;
import com.potato.cst.Const;

public class MethodInfoAdapter extends MethodVisitor {
    private final String owner;
    private final int methodAccess;
    private final String methodName;
    private final String methodDesc;

    public MethodInfoAdapter(MethodVisitor methodVisitor, String owner,
                             int methodAccess, String methodName, String methodDesc) {
        super(Const.ASM_VERSION, methodVisitor);
        this.owner = owner;
        this.methodAccess = methodAccess;
        this.methodName = methodName;
        this.methodDesc = methodDesc;
    }

    @Override
    public void visitCode() {
        if (mv != null) {
            String line = String.format("Method Enter: %s.%s:%s", owner, methodName, methodDesc);
            printMessage(line);

            int slotIndex = (methodAccess & Opcodes.ACC_STATIC) != 0 ? 0 : 1;
            Type methodType = Type.getMethodType(methodDesc);
            Type[] argumentTypes = methodType.getArgumentTypes();
            for (Type t : argumentTypes) {
                int sort = t.getSort();
                int size = t.getSize();
                int opcode = t.getOpcode(Opcodes.ILOAD);
                super.visitVarInsn(opcode, slotIndex);

                if (sort >= Type.BOOLEAN && sort <= Type.DOUBLE) {
                    String desc = t.getDescriptor();
                    printValueOnStack("(" + desc + ")V");
                }
                else {
                    printValueOnStack("(Ljava/lang/Object;)V");
                }
                slotIndex += size;
            }
        }

        super.visitCode();
    }

    private void printMessage(String str) {
        super.visitLdcInsn(str);
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "com/potato/utils/ParameterUtils", "printText", "(Ljava/lang/String;)V", false);
    }

    private void printValueOnStack(String descriptor) {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "com/potato/utils/ParameterUtils", "printValueOnStack", descriptor, false);
    }

    private void printStackTrace() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "com/potato/utils/ParameterUtils", "printStackTrace", "()V", false);
    }
}
```

MethodInfoVisitor.java:

```java
package com.potato.visitor;

import com.potato.adapter.MethodInfoAdapter;
import com.potato.cst.Const;
import jdk.internal.org.objectweb.asm.ClassVisitor;
import jdk.internal.org.objectweb.asm.MethodVisitor;

public class MethodInfoVisitor extends ClassVisitor {
    private String owner;

    public MethodInfoVisitor(ClassVisitor classVisitor) {
        super(Const.ASM_VERSION, classVisitor);
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(version, access, name, signature, superName, interfaces);
        this.owner = name;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);
        if (mv != null && !name.equals("<init>") && !name.equals("<clinit>")) {
            mv = new MethodInfoAdapter(mv, owner, access, name, descriptor);
        }
        return mv;
    }
}
```

## Agent Jar

**manifest.txt**

在`manifest.txt`文件中，记录Agent Class的信息：

```
Premain-Class: com.potato.agent.LoadTimeAgent

```

注意：在`manifest.txt`文件的结尾处有**一个空行**。(make sure the last line in the file is **a blank line**)

那么，如果不添加一个空行，会有什么结果呢？虽然可以成功生成`.jar`文件，但是不会将`manifest.txt`里的信息（`Premain-Class: lsieun.agent.LoadTimeAgent`）转换到`META-INF/MANIFEST.MF`里。

LoadTimeAgent.java:

```java
package com.potato.agent;

import com.potato.instrument.ASMTransformer;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;

public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        System.out.println("Premain-Class: " + LoadTimeAgent.class.getName());
        ClassFileTransformer transformer = new ASMTransformer();
        inst.addTransformer(transformer);
    }
}
```

 ASMTransformer.java:

```java
package com.potato.instrument;

import jdk.internal.org.objectweb.asm.ClassReader;
import jdk.internal.org.objectweb.asm.ClassVisitor;
import jdk.internal.org.objectweb.asm.ClassWriter;
import com.potato.visitor.MethodInfoVisitor;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

public class ASMTransformer implements ClassFileTransformer {
    @Override
    public byte[] transform(ClassLoader loader,
                            String className,
                            Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain,
                            byte[] classfileBuffer) throws IllegalClassFormatException {
        if (className == null) return null;
        if (className.startsWith("java")) return null;
        if (className.startsWith("javax")) return null;
        if (className.startsWith("jdk")) return null;
        if (className.startsWith("sun")) return null;
        if (className.startsWith("org")) return null;
        if (className.startsWith("com")) return null;
        if (className.startsWith("com.potato")) return null;

        System.out.println("candidate className: " + className);

        if (className.equals("sample/HelloWorld")) {
            ClassReader cr = new ClassReader(classfileBuffer);
            ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
            ClassVisitor cv = new MethodInfoVisitor(cw);

            int parsingOptions = 0;
            cr.accept(cv, parsingOptions);

            return cw.toByteArray();
        }

        return null;
    }
}
```

## 生成Jar包

编译：

打开windows cmd

```cmd
cd /d E:\Gitee\bytecode-dance\java-agent\java-agent-manual-02
:: 添加输出目录
mkdir out
:: 找到所有.java文件
for /r %a in (*.java) do echo %a >> sources.txt

:: 查看sources.txt内容
type sources.txt
E:\Gitee\bytecode-dance\java-agent\java-agent-manual-02\src\com\potato\adapter\MethodInfoAdapter.java
E:\Gitee\bytecode-dance\java-agent\java-agent-manual-02\src\com\potato\agent\LoadTimeAgent.java
E:\Gitee\bytecode-dance\java-agent\java-agent-manual-02\src\com\potato\cst\Const.java
E:\Gitee\bytecode-dance\java-agent\java-agent-manual-02\src\com\potato\instrument\ASMTransformer.java
E:\Gitee\bytecode-dance\java-agent\java-agent-manual-02\src\com\potato\utils\ParameterUtils.java
E:\Gitee\bytecode-dance\java-agent\java-agent-manual-02\src\com\potato\visitor\MethodInfoVisitor.java
E:\Gitee\bytecode-dance\java-agent\java-agent-manual-02\src\sample\HelloWorld.java
E:\Gitee\bytecode-dance\java-agent\java-agent-manual-02\src\sample\Program.java


```

以下列出错误编译和正确编译两种示例：

```cmd
# 错误的编译
javac -d out/ @sources.txt

# 正确的编译
javac -XDignore.symbol.file -d out/ @sources.txt
```



注意：在编译的时候，要添加`-XDignore.symbol.file`选项；否则，会编译出错。同时还要注意是java8的情况下，java9就不行了。

那么，如果不使用这个选项，为什么会出错呢？是因为在上面的代码当中用到了`jdk.internal.org.objectweb.asm`里的类，如果不使用这个选项，就会提示找不到相应的类。

编译完成之后，我们需要将分散的内容整合成一个Jar包文件：

```cmd
# 复制manifest.txt文件
xcopy /y src\manifest.txt out

# 切换目录
cd out/

# 进行打包（第一种方式）
            ┌─── f: TheAgent.jar
         ┌──┴──┐
$ jar -cvfm TheAgent.jar manifest.txt .
          └─────────┬────────┘
                    └─── m: manifest.txt
# 进行打包（第二种方式）
                   ┌─── f: TheAgent.jar
          ┌────────┴────────┐
$ jar -cvmf manifest.txt TheAgent.jar .
         └───┬──┘
             └─── m: manifest.txt
```

打包过程中的输出信息：

```
jar -cvfm TheAgent.jar manifest.txt .\com
已添加清单
正在添加: com/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: com/potato/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: com/potato/adapter/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: com/potato/adapter/MethodInfoAdapter.class(输入 = 2367) (输出 = 1231)(压缩了 47%)
正在添加: com/potato/agent/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: com/potato/agent/LoadTimeAgent.class(输入 = 887) (输出 = 505)(压缩了 43%)
正在添加: com/potato/cst/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: com/potato/cst/Const.class(输入 = 298) (输出 = 243)(压缩了 18%)
正在添加: com/potato/instrument/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: com/potato/instrument/ASMTransformer.class(输入 = 1736) (输出 = 923)(压缩了 46%)
正在添加: com/potato/utils/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: com/potato/utils/ParameterUtils.class(输入 = 2514) (输出 = 1051)(压缩了 58%)
正在添加: com/potato/visitor/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: com/potato/visitor/MethodInfoVisitor.class(输入 = 1177) (输出 = 553)(压缩了 53%)
```

## 运行

在使用`java`命令时，我们可以通过使用`-javaagent`选项来使用Java Agent Jar：

```cmd
java -javaagent:TheAgent.jar sample.Program
```

输出结果：

```
Premain-Class: com.potato.agent.LoadTimeAgent
candidate className: sample/Program
14184@DESKTOP-ULTBUSD
|000| 14184@DESKTOP-ULTBUSD remains 600 seconds
candidate className: sample/HelloWorld
Method Enter: sample/HelloWorld.add:(II)I
    7
    4
a + b = 11
|001| 14184@DESKTOP-ULTBUSD remains 599 seconds
Method Enter: sample/HelloWorld.add:(II)I
    8
    2
a + b = 10
|002| 14184@DESKTOP-ULTBUSD remains 598 seconds
Method Enter: sample/HelloWorld.sub:(II)I
    1
    6
a - b = -5
|003| 14184@DESKTOP-ULTBUSD remains 597 seconds
Method Enter: sample/HelloWorld.add:(II)I
    1
    0
a + b = 1
```

那么，`TheAgent.jar`到底做了一件什么事情呢？

在一般情况下，我们先编写`HelloWorld.java`文件，然后编译生成`HelloWorld.class`文件，最后加载到JVM当中运行。

当Instrumentation发生的时候，它是将原有的`HelloWorld.class`的内容进行修改（bytecode transformation），生成一个新的`HelloWorld.class`，最后将这个新的`HelloWorld.class`加载到JVM当中运行。

```mermaid
graph LR
    S(HelloWorld.java)--编译-->B1(HelloWorld.class)--加载原字节码-->JVM
    B1--字节码修改:Instrumentation/Java Agent-->B2(修改后的字节码HelloWorld.class)
    B2--加载修改后的字节码-->JVM

```



## 总结

文内容总结如下：

- 第一点，本文的主要目的是希望大家对Java Agent有一个整体的印象，因此不需要理解技术细节（特别是[ASM](https://lsieun.github.io/java/asm/index.html)相关内容）。
- 第二点，Agent Jar当中有三个重要组成部分：manifest、Agent Class和ClassFileTransformer。
- 第三点，当使用`javac`命令编译时，如果在程序当中使用到了`jdk.*`或`sun.*`当中的类，要添加`-XDignore.symbol.file`选项。
- 第四点，当使用`java`命令加载Agent Jar时（Load-Time Instrumentation），需要添加`-javaagent`选项。



# Dynamic Agent打印方法接收的参数(手动打包)

permain方法只能在java程序启动之前执行，并不能程序启动之后再执行，但是在实际的很多的情况下，我们没有办法在虚拟机启动之时就为其设定代理，这样实际上限制了instrument的应用。而Java SE 6的新特性改变了这种情况，可以通过Java Tool API中的attach方式来达到这种**程序启动之后设置代理**的效果。

Attach API 不是 Java 的标准 API，而是 Sun 公司提供的一套扩展 API，用来向目标 JVM “附着”（Attach）代理工具程序的。有了它，开发者可以方便的监控一个 JVM，运行一个外加的代理程序。

Attach API 很简单，只有 2 个主要的类，都在 `com.sun.tools.attach`包里面： `VirtualMachine` 代表一个 Java 虚拟机，也就是程序需要监控的目标虚拟机，提供了 JVM 枚举，`Attach` 动作和 `Detach` 动作（从 JVM 上面解除一个代理）等等 ; `VirtualMachineDescriptor` 则是一个描述虚拟机的容器类，配合`VirtualMachine`类完成各种功能。

Attach API功能

1. 列出当前所有的JVM实例描述
2. Attach到其中一个JVM上，建立通信管道
3. 让目标JVM加载Agent

来源：https://lsieun.github.io/java-agent/s01ch01/quick-start-example-03.html

预期目标：借助于JDK内置的ASM打印出方法接收的参数，使用Dynamic Instrumentation的方式实现。

![img](img/JavaAgent三个组成部分.assets/virtual-machine-of-dynamic-instrumentation.png)

代码目录结构：

```cmd
src                                
  │  manifest.txt                  
  │                                
  ├─attach                         
  │      VMAttach.java             
  │                                
  ├─com                            
  │  └─potato                      
  │          ASMTransformer.java   
  │          Const.java            
  │          DynamicAgent.java     
  │          MethodInfoAdapter.java
  │          MethodInfoVisitor.java
  │          ParameterUtils.java   
  │                                
  └─sample                         
          HelloWorld.java          
          Program.java             
```

## Application

HelloWorld.java、Program.java同上

## ASM相关

ParameterUtils.java、Const.java、MethodInfoAdapter.java、MethodInfoVisitor.java同上

## Agent Jar

**manifest.txt**

```
Agent-Class: com.potato.DynamicAgent
Can-Retransform-Classes: true
```

注意：在`manifest.txt`文件的结尾处有**一个空行**。

**DynamicAgent.java**

```java
package com.potato;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;

public class DynamicAgent {
    public static void agentmain(String agentArgs, Instrumentation inst) {
        ClassFileTransformer transformer = new ASMTransformer();
        try {
            inst.addTransformer(transformer, true);
            Class<?> targetClass = Class.forName("sample.HelloWorld");
            inst.retransformClasses(targetClass);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            inst.removeTransformer(transformer);
        }
    }
}
```

ASMTransformer.java 同上

## JVM Attach

VMAttach.java
在下面的代码中，我们要用到com.sun.tools.attach里定义的类，因此编译的时候需要用到tools.jar文件。

```java
package attach;

import com.sun.tools.attach.VirtualMachine;
import com.sun.tools.attach.VirtualMachineDescriptor;

import java.util.List;

public class VMAttach {
    public static void main(String[] args) throws Exception {
        String agent = "TheAgent.jar";
        System.out.println("Agent Path: " + agent);
        //拿到所有JVM进程,VirtualMachineDescriptor封装了JVM的标识符
        List<VirtualMachineDescriptor> vmds = VirtualMachine.list();
        for (VirtualMachineDescriptor vmd : vmds) {
            if (vmd.displayName().equals("sample.Program")) {
                VirtualMachine vm = VirtualMachine.attach(vmd.id());
                System.out.println("Load Agent");
                vm.loadAgent(agent);
                System.out.println("Detach");
                vm.detach();
            }
        }
    }
}
```

## 编译和运行

```cmd
tree #查看当前的目录和子目录的结构
tree /f#查看当前的目录和子目录的结构，包含文件
```



```cmd
cd java-agent-manual-03
mkdir out
::编译 编译Application：sample包
javac .\src\sample\*.java -d out

::编译和生成Agent.jar:
for /r %a in (.\src\com\potato\*.java) do echo %a >> sources.txt
::编译
javac -XDignore.symbol.file -d out/ @sources.txt
::拷贝manifest.txt
xcopy /y  .\src\manifest.txt out
cd out
::打包
jar -cvfm TheAgent.jar manifest.txt

::编译VM Attach
cd ..
javac -cp "%JAVA_HOME%/lib/tools.jar";. -d out/ src/attach/VMAttach.java
```

**运行**

```cmd
cd out
::先启动app
java -cp TheAgent.jar sample.Program

12964@DESKTOP-ULTBUSD
|000| 12964@DESKTOP-ULTBUSD remains 600 seconds
a + b = 7
|001| 12964@DESKTOP-ULTBUSD remains 599 seconds
a + b = 15
|002| 12964@DESKTOP-ULTBUSD remains 598 seconds
a + b = 4
|003| 12964@DESKTOP-ULTBUSD remains 597 seconds
a - b = 4
|004| 12964@DESKTOP-ULTBUSD remains 596 seconds
a + b = 9


::切到另一个cmd窗口,再进行attach
java -cp "%JAVA_HOME%/lib/tools.jar";. attach.VMAttach
Agent Path: TheAgent.jar
Load Agent
Detach

::此时，原来的窗口，打印的日志变为：
candidate className: sample/HelloWorld
|005| 12964@DESKTOP-ULTBUSD remains 595 seconds
Method Enter: sample/HelloWorld.sub:(II)I
    8
    8
a - b = 0
|006| 12964@DESKTOP-ULTBUSD remains 594 seconds
Method Enter: sample/HelloWorld.sub:(II)I
    3
    1
a - b = 2
|007| 12964@DESKTOP-ULTBUSD remains 593 seconds
Method Enter: sample/HelloWorld.sub:(II)I
    3
    0
a - b = 3
|008| 12964@DESKTOP-ULTBUSD remains 592 seconds
Method Enter: sample/HelloWorld.add:(II)I
    4
    7
a + b = 11
```

本文内容总结如下：

- 第一点，本文的主要目的是对Java Agent有一个整体的印象，因此不需要理解技术细节。
- 第二点，Java Agent的Jar包当中有三个重要组成部分：manifest、Agent Class和ClassFileTransformer。
- 第三点，当使用`javac`命令编译时，如果在程序当中使用到了`jdk.*`或`sun.*`当中的类，要添加`-XDignore.symbol.file`选项。
- 第四点，当运行Dynamic Instrumentation时，需要在`CLASSPATH`当中引用`JAVA_HOME/lib/tools.jar`。

# Maven:Load-Time Agent和Dynamic Agent

来源：https://lsieun.github.io/java-agent/s01ch01/java-agent-using-maven.html

预期目标：打印方法接收的参数值和返回值，借助于Maven管理依赖和进行编译，避免手工打Jar包的麻烦。

本文内容虽然很多，但是我们静下心来想一想，它有一个简单的目标：生成一个Agent Jar。因此，在过程当中的内容细节，都是为`TheAgent.jar`做一定的铺垫。

## 创建maven项目

新建一个Maven项目，取名为`java-agent-maven`，代码目录结构：

```
src                                                 
  ├─main                                            
  │  ├─java                                         
  │  │  ├─com                                       
  │  │  │  └─potato                                 
  │  │  │      │  Main.java                         
  │  │  │      │                                    
  │  │  │      ├─agent                              
  │  │  │      │      DynamicAgent.java             
  │  │  │      │      LoadTimeAgent.java            
  │  │  │      │                                    
  │  │  │      ├─asm                                
  │  │  │      │      Const.java                    
  │  │  │      │      MethodInfo.java               
  │  │  │      │      PrintMethodInfoStdAdapter.java
  │  │  │      │      PrintMethodInfoVisitor.java   
  │  │  │      │                                    
  │  │  │      └─instrument                         
  │  │  │              ASMTransformer.java          
  │  │  │                                           
  │  │  ├─example                                   
  │  │  │      HelloWorld.java                      
  │  │  │      Program.java                         
  │  │  │                                           
  │  │  └─run                                       
  │  │          LoadTimeInstrumentation.java        
  │  │          PathManager.java                    
```



`META-INF/MANIFEST.MF`的信息由`pom.xml`文件中`maven-jar-plugin`提供。

生成Jar文件，我们有三种选择：

- 第一种，`maven-jar-plugin` + `maven-dependency-plugin`
- 第二种，`maven-assembly-plugin`
- 第三种，`maven-shade-plugin`

### pom.xml

#### properties

```xml
<properties>
  <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
   <java.version>1.8</java.version>
  <maven.compiler.source>8</maven.compiler.source>
  <maven.compiler.target>8</maven.compiler.target>
  <asm.version>9.2</asm.version>
</properties>
```

#### ASM依赖

在这里不再使用JDK内置的ASM类库，因为内置的版本比较低。

我们想使用更高的ASM版本，也就能够支持更高版本`.class`文件操作。

```xml
<dependency>
    <groupId>org.ow2.asm</groupId>
    <artifactId>asm</artifactId>
    <version>${asm.version}</version>
</dependency>
<dependency>
    <groupId>org.ow2.asm</groupId>
    <artifactId>asm-util</artifactId>
    <version>${asm.version}</version>
</dependency>
<dependency>
    <groupId>org.ow2.asm</groupId>
    <artifactId>asm-commons</artifactId>
    <version>${asm.version}</version>
</dependency>
<dependency>
    <groupId>org.ow2.asm</groupId>
    <artifactId>asm-tree</artifactId>
    <version>${asm.version}</version>
</dependency>
<dependency>
    <groupId>org.ow2.asm</groupId>
    <artifactId>asm-analysis</artifactId>
    <version>${asm.version}</version>
</dependency>
```

#### **tools.jar**

在`tools.jar`文件当中，包含了`com.sun.tools.attach.VirtualMachine`类，会在`DynamicInstrumentation`类当中用到。

```xml
<dependency>
  <groupId>com.sun</groupId>
  <artifactId>tools</artifactId>
  <version>8</version>
  <scope>system</scope>
  <systemPath>${env.JAVA_HOME}/lib/tools.jar</systemPath>
</dependency>
```

#### **plugins**

```xml
<build>
  <finalName>TheAgent</finalName>
  <plugins>
  </plugins>
</build>
```

##### **compiler-plugin**

下面的`maven-compiler-plugin`插件主要关注`compilerArgs`下的三个参数：

- `-g`: 生成所有调试信息
- `-parameters`: 生成MethodParameter属性
- `-XDignore.symbol.file`: 在编译过程中，进行link时，不使用`lib/ct.sym`，而是直接使用`rt.jar`文件。

```xml
<!-- Java Compiler -->
<plugin>
  <groupId>org.apache.maven.plugins</groupId>
  <artifactId>maven-compiler-plugin</artifactId>
  <version>3.8.1</version>
  <configuration>
    <source>${java.version}</source>
    <target>${java.version}</target>
    <fork>true</fork>
    <compilerArgs>
      <arg>-g</arg>
      <arg>-parameters</arg>
      <arg>-XDignore.symbol.file</arg>
    </compilerArgs>
  </configuration>
</plugin>
```

##### **jar-plugin**

下面的[`maven-jar-plugin`](https://maven.apache.org/shared/maven-archiver/index.html)插件主要做以下两件事情：

- 第一，设置`META-INF/MANIFEST.MF`中的信息。
- 第二，确定在jar包当中包含哪些文件。

关于`<archive>`的配置，可以参考[Apache Maven Archiver](https://maven.apache.org/shared/maven-archiver/index.html)。

```xml
<plugin>
  <groupId>org.apache.maven.plugins</groupId>
  <artifactId>maven-jar-plugin</artifactId>
  <version>3.2.0</version>
  <configuration>
    <archive>
      <manifest>
        <mainClass>com.potato.Main</mainClass>
        <addClasspath>true</addClasspath>
        <classpathPrefix>lib/</classpathPrefix>
        <addDefaultImplementationEntries>false</addDefaultImplementationEntries>
        <addDefaultSpecificationEntries>false</addDefaultSpecificationEntries>
      </manifest>
      <manifestEntries>
        <Premain-Class>com.potato.agent.LoadTimeAgent</Premain-Class>
        <Agent-Class>com.potato.agent.DynamicAgent</Agent-Class>
        <Launcher-Agent-Class>com.potato.agent.LauncherAgent</Launcher-Agent-Class>
        <Can-Redefine-Classes>true</Can-Redefine-Classes>
        <Can-Retransform-Classes>true</Can-Retransform-Classes>
        <Can-Set-Native-Method-Prefix>true</Can-Set-Native-Method-Prefix>
      </manifestEntries>
      <addMavenDescriptor>false</addMavenDescriptor>
    </archive>
    <includes>
      <include>com.potato/**</include>
    </includes>
  </configuration>
</plugin>
```

##### **dependency-plugin**

下面的`maven-dependency-plugin`插件主要目的：将依赖的jar包复制到`lib`目录下。

```xml
<plugin>
  <groupId>org.apache.maven.plugins</groupId>
  <artifactId>maven-dependency-plugin</artifactId>
  <version>3.2.0</version>
  <executions>
    <execution>
      <id>lib-copy-dependencies</id>
      <phase>package</phase>
      <goals>
        <goal>copy-dependencies</goal>
      </goals>
      <configuration>
        <excludeArtifactIds>tools</excludeArtifactIds>
        <outputDirectory>${project.build.directory}/lib</outputDirectory>
        <overWriteReleases>false</overWriteReleases>
        <overWriteSnapshots>false</overWriteSnapshots>
        <overWriteIfNewer>true</overWriteIfNewer>
      </configuration>
    </execution>
  </executions>
</plugin>
```

##### **assembly-plugin**

下面的[`maven-assembly-plugin`](https://maven.apache.org/plugins/maven-assembly-plugin/index.html)插件主要目的：生成一个jar文件，它包含了依赖的jar包。

```xml
<plugin>
  <groupId>org.apache.maven.plugins</groupId>
  <artifactId>maven-assembly-plugin</artifactId>
  <version>3.3.0</version>
  <configuration>
    <archive>
      <manifest>
        <mainClass>com.potato.Main</mainClass>
        <addDefaultEntries>false</addDefaultEntries>
      </manifest>
      <manifestEntries>
        <Premain-Class>com.potato.agent.LoadTimeAgent</Premain-Class>
        <Agent-Class>com.potato.agent.DynamicAgent</Agent-Class>
        <Launcher-Agent-Class>com.potato.agent.LauncherAgent</Launcher-Agent-Class>
        <Can-Redefine-Classes>true</Can-Redefine-Classes>
        <Can-Retransform-Classes>true</Can-Retransform-Classes>
        <Can-Set-Native-Method-Prefix>true</Can-Set-Native-Method-Prefix>
      </manifestEntries>
    </archive>
    <descriptorRefs>
      <descriptorRef>jar-with-dependencies</descriptorRef>
    </descriptorRefs>
  </configuration>
  <executions>
    <execution>
      <id>make-assembly</id>
      <phase>package</phase>
      <goals>
        <goal>single</goal>
      </goals>
    </execution>
  </executions>
</plugin>
```

##### shade-plugin

下面的[`maven-shade-plugin`](https://maven.apache.org/plugins/maven-shade-plugin/index.html)插件主要目的：生成一个jar文件，它包含了依赖的jar包，可以进行精简。

```xml
<plugin>
  <groupId>org.apache.maven.plugins</groupId>
  <artifactId>maven-shade-plugin</artifactId>
  <version>3.2.4</version>
  <configuration>
    <minimizeJar>true</minimizeJar>
    <filters>
      <filter>
        <artifact>*:*</artifact>
        <excludes>
          <exclude>run/*</exclude>
          <exclude>sample/*</exclude>
        </excludes>
      </filter>
    </filters>
  </configuration>
  <executions>
    <execution>
      <phase>package</phase>
      <goals>
        <goal>shade</goal>
      </goals>
      <configuration>
        <transformers>
          <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
            <manifestEntries>
              <Main-Class>com.potato.Main</Main-Class>
              <Premain-Class>com.potato.agent.LoadTimeAgent</Premain-Class>
              <Agent-Class>com.potato.agent.DynamicAgent</Agent-Class>
              <Launcher-Agent-Class>com.potato.agent.LauncherAgent</Launcher-Agent-Class>
              <Can-Redefine-Classes>true</Can-Redefine-Classes>
              <Can-Retransform-Classes>true</Can-Retransform-Classes>
              <Can-Set-Native-Method-Prefix>true</Can-Set-Native-Method-Prefix>
            </manifestEntries>
          </transformer>
        </transformers>
      </configuration>
    </execution>
  </executions>
</plugin>
```

## Application

HelloWorld.java、Program.java同上

## ASM相关

Const.java同上

PrintMethodInfoStdAdapter.java：打印方法的入参和返回值

PrintMethodInfoVisitor.java：asm方法访问器

MethodInfo.java:方法的类型枚举

## Agent Jar

LoadTimeAgent.java:

```java
package com.potato.agent;

import com.potato.instrument.ASMTransformer;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;

public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        System.out.println("Premain-Class: " + LoadTimeAgent.class.getName());
        System.out.println("Can-Redefine-Classes: " + inst.isRedefineClassesSupported());
        System.out.println("Can-Retransform-Classes: " + inst.isRetransformClassesSupported());
        System.out.println("Can-Set-Native-Method-Prefix: " + inst.isNativeMethodPrefixSupported());
        System.out.println("========= ========= =========");

        ClassFileTransformer transformer = new ASMTransformer("sample/HelloWorld");
        inst.addTransformer(transformer, false);
    }
}
```

DynamicAgent.java:

```java
package com.potato.agent;

import com.potato.instrument.ASMTransformer;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;

public class DynamicAgent {
    public static void agentmain(String agentArgs, Instrumentation inst) {
        System.out.println("Agent-Class: " + DynamicAgent.class.getName());
        System.out.println("Can-Redefine-Classes: " + inst.isRedefineClassesSupported());
        System.out.println("Can-Retransform-Classes: " + inst.isRetransformClassesSupported());
        System.out.println("Can-Set-Native-Method-Prefix: " + inst.isNativeMethodPrefixSupported());
        System.out.println("========= ========= =========");

        ClassFileTransformer transformer = new ASMTransformer("sample/HelloWorld");
        inst.addTransformer(transformer, true);

        try {
            Class<?> targetClass = Class.forName("sample.HelloWorld");
            inst.retransformClasses(targetClass);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            inst.removeTransformer(transformer);
        }
    }
}
```

**LauncherAgent.java**

```java
package lsieun.agent;

import java.lang.instrument.Instrumentation;

public class LauncherAgent {
    public static void agentmain(String agentArgs, Instrumentation inst) {
        System.out.println("Launcher-Agent-Class: " + LauncherAgent.class.getName());
        System.out.println("Can-Redefine-Classes: " + inst.isRedefineClassesSupported());
        System.out.println("Can-Retransform-Classes: " + inst.isRetransformClassesSupported());
        System.out.println("Can-Set-Native-Method-Prefix: " + inst.isNativeMethodPrefixSupported());
        System.out.println("========= ========= =========");
    }
}
```

ASMTransformer.java:

```java
package com.potato.instrument;

import com.potato.asm.MethodInfo;
import com.potato.asm.PrintMethodInfoVisitor;
import org.objectweb.asm.*;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.*;

public class ASMTransformer implements ClassFileTransformer {
    public static final List<String> ignoredPackages = Arrays.asList("com/", "com/sun/", "java/", "javax/", "jdk/", "com/potato", "org/", "sun/");

    private final String internalName;

    public ASMTransformer(String internalName) {
        Objects.requireNonNull(internalName);
        this.internalName = internalName.replace(".", "/");
    }

    @Override
    public byte[] transform(ClassLoader loader,
                            String className,
                            Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain,
                            byte[] classfileBuffer) throws IllegalClassFormatException {
        if (className == null) return null;

        for (String name : ignoredPackages) {
            if (className.startsWith(name)) {
                return null;
            }
        }
        System.out.println("candidate class: " + className);

        if (className.equals(internalName)) {
            System.out.println("transform class: " + className);
            ClassReader cr = new ClassReader(classfileBuffer);
            ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
            Set<MethodInfo> flags = EnumSet.of(
                    MethodInfo.NAME_AND_DESC,
                    MethodInfo.PARAMETER_VALUES,
                    MethodInfo.RETURN_VALUE);
            ClassVisitor cv = new PrintMethodInfoVisitor(cw, flags);

            int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
            cr.accept(cv, parsingOptions);

            return cw.toByteArray();
        }

        return null;
    }
}
```



Main.java:

```java
package com.potato;

public class Main {
    public static void main(String[] args) {
        System.out.println("This is a Java Agent Jar");
    }
}
```

## Run

LoadTimeInstrumentation.java:

```java
package run;

import java.util.Formatter;

public class LoadTimeInstrumentation {
    public static void main(String[] args) {
        usage();
    }

    public static void usage() {
        String jarPath = PathManager.getJarPath();
        StringBuilder sb = new StringBuilder();
        Formatter fm = new Formatter(sb);
        fm.format("Usage:%n");
        fm.format("    java -javaagent:/path/to/TheAgent.jar sample.Program%n");
        fm.format("Example:%n");
        fm.format("    java -cp ./target/classes/ -javaagent:./target/TheAgent.jar sample.Program%n");
        fm.format("    java -cp ./target/classes/ -javaagent:%s sample.Program", jarPath);
        String result = sb.toString();
        System.out.println(result);
    }
}
```

DynamicInstrumentation.java:

```java
package run;

import com.sun.tools.attach.VirtualMachine;
import com.sun.tools.attach.VirtualMachineDescriptor;

import java.util.List;

public class DynamicInstrumentation {
    public static void main(String[] args) throws Exception {
        String agent = PathManager.getJarPath();
        System.out.println("Agent Path: " + agent);
        List<VirtualMachineDescriptor> vmds = VirtualMachine.list();
        for (VirtualMachineDescriptor vmd : vmds) {
            if (vmd.displayName().equals("sample.Program")) {
                VirtualMachine vm = VirtualMachine.attach(vmd.id());
                vm.getSystemProperties();
                System.out.println("Load Agent");
                vm.loadAgent(agent);
                System.out.println("Detach");
                vm.detach();
            }
        }
    }
}
```

PathManager.java:

```java
package run;

import java.io.File;
import java.net.URISyntaxException;

public class PathManager {
    public static String getJarPath() {
        String filepath = null;

        try {
            filepath = new File(PathManager.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath();
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
        }

        if (filepath == null || !filepath.endsWith(".jar")) {
            filepath = System.getProperty("user.dir") + File.separator + "target/TheAgent.jar";
        }

        return filepath.replace(File.separator, "/");
    }
}
```



## 打包运行

```
mvn clean package
```

上述命令执行完成之后，会在`target`文件夹下生成`TheAgent.jar`

运行Load-Time Instrumentation：

```cmd
java -cp .\target\classes\ -javaagent:.\target\TheAgent.jar sample.Program
```

运行Dynamic Instrumentation：

```cmd
java -cp "%JAVA_HOME%/lib/tools.jar";.\target\classes\ run.DynamicInstrumentation
```

如果是Java 9及以上的版本，不需要引用`tools.jar`文件，可以直接运行：

```cmd
java -cp .\target\classes\ run.DynamicInstrumentation
```

**运行结果**

```
Premain-Class: com.potato.agent.LoadTimeAgent
Can-Redefine-Classes: true
Can-Retransform-Classes: true
Can-Set-Native-Method-Prefix: true
========= ========= =========
candidate class: sample/Program
14004@DESKTOP-ULTBUSD
|000| 14004@DESKTOP-ULTBUSD remains 600 seconds
candidate class: sample/HelloWorld
transform class: sample/HelloWorld
---> sample/HelloWorld.add:(II)I
---> sample/HelloWorld.sub:(II)I
Method Enter: sample/HelloWorld.add:(II)I
    0: 8
    1: 4
Method Return: sample/HelloWorld.add:(II)I
    12
=================================================================================
a + b = 12
|001| 14004@DESKTOP-ULTBUSD remains 599 seconds
Method Enter: sample/HelloWorld.add:(II)I
    0: 8
    1: 3
Method Return: sample/HelloWorld.add:(II)I
    11
=================================================================================
a + b = 11
|002| 14004@DESKTOP-ULTBUSD remains 598 seconds
Method Enter: sample/HelloWorld.add:(II)I
    0: 4
    1: 7
Method Return: sample/HelloWorld.add:(II)I
    11
    
......
a + b = 17
Agent-Class: com.potato.agent.DynamicAgent
Can-Redefine-Classes: true
Can-Retransform-Classes: true
Can-Set-Native-Method-Prefix: true
========= ========= =========
candidate class: sample/HelloWorld
transform class: sample/HelloWorld
---> sample/HelloWorld.add:(II)I
---> sample/HelloWorld.sub:(II)I
|006| 14004@DESKTOP-ULTBUSD remains 594 seconds
Method Enter: sample/HelloWorld.add:(II)I
    0: 0
    1: 4
Method Enter: sample/HelloWorld.add:(II)I
    0: 0
    1: 4
Method Return: sample/HelloWorld.add:(II)I
    4
=================================================================================
```



## 总结

本文内容总结如下：

- 第一点，使用Maven会提供很大的方便，但是Agent Jar的核心三要素没有发生变化，包括manifest、Agent Class和ClassFileTransformer，三者缺一不可。
- 第二点，使用ASM修改字节码（bytecode）的内容是属于Java Agent的“辅助部分”。如果我们熟悉其它的字节码操作类库（例如，Javassist、ByteBuddy），可以将ASM替换掉。
- 第三点，细节之处的把握。
  - 在`pom.xml`文件中，对`${env.JAVA_HOME}/lib/tools.jar`进行了依赖，是因为我们用到`com.sun.tools.attach.VirtualMachine`类。
  - 在`pom.xml`文件中，`maven-jar-plugin`部分提供的与manifest相关的信息，会转换到`META-INF/MANIFEST.MF`文件中去。

# 总结

## 1. 三个组成部分

在Java Agent对应的`.jar`文件里，有三个主要组成部分：

- Manifest
- Agent Class
- ClassFileTransformer

![Agent Jar中的三个组成部分](img/JavaAgent三个组成部分.assets/agent-jar-three-components-16465805459543.png)

三个组成部分：

```
                ┌─── Manifest ───────────────┼─── META-INF/MANIFEST.MF
                │
                │                            ┌─── LoadTimeAgent.class: premain
TheAgent.jar ───┼─── Agent Class ────────────┤
                │                            └─── DynamicAgent.class: agentmain
                │
                └─── ClassFileTransformer ───┼─── ASMTransformer.class
```

彼此之间的关系：

```
Manifest --> Agent Class --> Instrumentation --> ClassFileTransformer
```

## 2. Load-Time VS. Dynamic

### 2.1. Load-Time

在Load-Time Instrumentation当中，只涉及到一个JVM：

![img](img/JavaAgent三个组成部分.assets/virtual-machine-of-load-time-instrumentation-16465805459545.png)

在Manifest部分，需要定义`Premain-Class`属性。

在Agent Class部分，需要定义`premain`方法。下面是`premain`的两种写法：

```
public static void premain(String agentArgs, Instrumentation inst);
public static void premain(String agentArgs);
```

在运行的时候，需要配置`-javaagent`选项加载Agent Jar：

```
java -cp ./target/classes/ -javaagent:./target/TheAgent.jar sample.Program
```

在运行的过程当中，先执行Agent Class的`premain`方法，再执行Application的`main`方法。

### 2.2. Dynamic

在Dynamic Instrumentation当中，涉及到两个JVM：

![img](img/JavaAgent三个组成部分.assets/virtual-machine-of-dynamic-instrumentation-16465805459557.png)

在Manifest部分，需要定义`Agent-Class`属性。

在Agent Class部分，需要定义`agentmain`方法。下面是`agentmain`的两种写法：

```
public static void agentmain(String agentArgs, Instrumentation inst);
public static void agentmain(String agentArgs);
```

在运行的时候，需要使用Attach机制加载Agent Jar。

在运行的过程当中，一般Application的`main`方法已经开始执行，而Agent Class的`agentmain`方法后执行。

## 3. 总结

本文内容总结如下：

- 第一点，Agent Jar的三个组成部分：Manifest、Agent Class和ClassFileTransformer。
- 第二点，对Load-Time Instrumentation和Dynamic Instrumentation有一个初步的理解。
  - Load-Time Instrumentation: `Premain-Class` —> `premain()` —> `-javaagent`
  - Dynamic Instrumentation: `Agent-Class` —> `agentmain()` —> Attach

1
