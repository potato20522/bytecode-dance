# 两种方式

- 方式一：-javaagent

- 方式二：attach到进程上

# Load-Time: agentArgs参数

来源：https://lsieun.github.io/java-agent/s01ch02/load-time-agent-premain-agentArgs.html

首先，我们需要注意：并不是所有的虚拟机，都支持从command line启动Java Agent。但一般都支持。

其次，如何在command-line为Agent Jar添加参数信息呢？

## 命令行启动

### Command-Line

从命令行启动Java Agent需要使用`-javagent`选项：

```
-javaagent:jarpath[=options]
```

- `jarpath` is the path to the agent JAR file.
- `options` is the agent options.

```
                                                  ┌─── -javaagent:jarpath
                             ┌─── Command-Line ───┤
                             │                    └─── -javaagent:jarpath=options
Load-Time Instrumentation ───┤
                             │                    ┌─── MANIFEST.MF - Premain-Class: lsieun.agent.LoadTimeAgent
                             └─── Agent Jar ──────┤
                                                  └─── Agent Class - premain(String agentArgs, Instrumentation inst)
```

示例：

```
java -cp ./target/classes/ -javaagent:./target/TheAgent.jar=this-is-a-long-message sample.Program
```

### Agent Jar

在`TheAgent.jar`中，依据`META-INF/MANIFEST.MF`里定义`Premain-Class`属性找到Agent Class:

```
Premain-Class: lsieun.agent.LoadTimeAgent
```

The agent is passed its agent `options` via the `agentArgs` parameter.

```java
public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        // ...
    }
}
```

The agent `options` are passed as a single string, any additional parsing should be performed by the agent itself.

![img](img/JavaAgent两种启动方式.assets/java-agent-command-line-options.png)

## 示例一：读取agentArgs

### agent

演示 premain 方法中的agentArgs参数

LoadTimeAgent.java 包含premain方法，在main方法执行之前执行

```java
package com.potato;

import java.lang.instrument.Instrumentation;

public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        System.out.println("Premain-Class: " + LoadTimeAgent.class.getName());
        System.out.println("agentArgs: " + agentArgs);
        System.out.println("Instrumentation Class: " + inst.getClass().getName());
    }
}
```
pom.xml：
```xml
<build>
  <finalName>TheAgent</finalName>
  <plugins>
    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-jar-plugin</artifactId>
      <version>3.2.0</version>
      <configuration>
        <archive>
          <manifest>
            <addDefaultImplementationEntries>false</addDefaultImplementationEntries>
            <addDefaultSpecificationEntries>false</addDefaultSpecificationEntries>
          </manifest>
          <manifestEntries>
            <Premain-Class>com.potato.LoadTimeAgent</Premain-Class>
            <Can-Redefine-Classes>true</Can-Redefine-Classes>
            <Can-Retransform-Classes>true</Can-Retransform-Classes>
            <Can-Set-Native-Method-Prefix>true</Can-Set-Native-Method-Prefix>
          </manifestEntries>
          <addMavenDescriptor>false</addMavenDescriptor>
        </archive>
        <includes>
          <include>com/potato/**</include>
        </includes>
      </configuration>
    </plugin>
  </plugins>
</build>
```

对agent执行打包：

```cmd
mvn clean package
```

### 目标业务代码

再写个main函数，假定这是我们的业务应用，功能是每过1秒就行随机的两个数相加或相减

Program.java

```java
package sample;
public class Program {
  public static void main(String[] args) throws Exception {
     // (1) print process id
        String nameOfRunningVM = ManagementFactory.getRuntimeMXBean().getName();
        System.out.println(nameOfRunningVM);

        // (2) count down
        int count = 600;
        for (int i = 0; i < count; i++) {
            String info = String.format("|%03d| %s remains %03d seconds", i, nameOfRunningVM, (count - i));
            System.out.println(info);

            Random rand = new Random(System.currentTimeMillis());
            int a = rand.nextInt(10);
            int b = rand.nextInt(10);
            boolean flag = rand.nextBoolean();
            String message;
            if (flag) {
                message = String.format("a + b = %d", HelloWorld.add(a, b));
            }
            else {
                message = String.format("a - b = %d", HelloWorld.sub(a, b));
            }
            System.out.println(message);

            TimeUnit.SECONDS.sleep(1);
        }
  }
}
```

```java
public class HelloWorld {
    public static int add(int a, int b) {
        return a + b;
    }

    public static int sub(int a, int b) {
        return a - b;
    }
}
```

### 执行和结果

**1.不加参数：**

执行时加上agent

```
-javaagent:E:\Gitee\bytecode-dance\java-agent\load-time-agentargs\target\TheAgent.jar
```

结果

```
Premain-Class: com.potato.LoadTimeAgent
agentArgs: null
Instrumentation Class: sun.instrument.InstrumentationImpl
16488@DESKTOP-ULTBUSD
|000| 16488@DESKTOP-ULTBUSD remains 600 seconds
a - b = 5
|001| 16488@DESKTOP-ULTBUSD remains 599 seconds
a + b = 16
...
```

****

**2.加上参数时**

```
-javaagent:E:\Gitee\bytecode-dance\java-agent\load-time-agentargs\target\TheAgent.jar=this-is-a-long-message
```

使用等于号来指定agent参数

执行结果：

```
Premain-Class: com.potato.LoadTimeAgent
agentArgs: this-is-a-long-message
Instrumentation Class: sun.instrument.InstrumentationImpl
26812@DESKTOP-ULTBUSD
|000| 26812@DESKTOP-ULTBUSD remains 600 seconds
a - b = 5
|001| 26812@DESKTOP-ULTBUSD remains 599 seconds
a - b = -4
```

## 示例二：解析agentArgs

我们传入的信息，一般情况下是`key-value`的形式，有人喜欢用`:`分隔，有人喜欢用`=`分隔：

```
username:tomcat,password:123456
username=tomcat,password=123456
```

### agent

LoadTimeAgent.java

```java
import java.lang.instrument.Instrumentation;

public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        System.out.println("Premain-Class: " + LoadTimeAgent.class.getName());
        System.out.println("agentArgs: " + agentArgs);
        System.out.println("Instrumentation Class: " + inst.getClass().getName());

        if (agentArgs != null) {
            String[] array = agentArgs.split(",");
            int length = array.length;
            for (int i = 0; i < length; i++) {
                String item = array[i];
                String[] key_value_pair = getKeyValuePair(item);

                String key = key_value_pair[0];
                String value = key_value_pair[1];

                String line = String.format("|%03d| %s: %s", i, key, value);
                System.out.println(line);
            }
        }
    }

    private static String[] getKeyValuePair(String str) {
        {
            int index = str.indexOf("=");
            if (index != -1) {
                return str.split("=", 2);
            }
        }

        {
            int index = str.indexOf(":");
            if (index != -1) {
                return str.split(":", 2);
            }
        }
        return new String[]{str, ""};
    }
}
```

### 执行和结果

第一次运行，使用`:`分隔：

```
-javaagent:E:\Gitee\bytecode-dance\java-agent\load-time-agentargs\target\TheAgent.jar=username:tomcat,password:123456
```

执行结果：

```
Premain-Class: com.potato.LoadTimeAgent
agentArgs: username:tomcat,password:123456
Instrumentation Class: sun.instrument.InstrumentationImpl
|000| username: tomcat
|001| password: 123456
7280@DESKTOP-ULTBUSD
|000| 7280@DESKTOP-ULTBUSD remains 600 seconds
a - b = -3
|001| 7280@DESKTOP-ULTBUSD remains 599 seconds
a + b = 11
```

第二次运行，使用`=`分隔：

-javaagent:E:\Gitee\bytecode-dance\java-agent\load-time-agentargs\target\TheAgent.jar=username=tomcat,password=123456

执行结果：

```
Premain-Class: com.potato.LoadTimeAgent
agentArgs: username=tomcat,password=123456
Instrumentation Class: sun.instrument.InstrumentationImpl
|000| username: tomcat
|001| password: 123456
21108@DESKTOP-ULTBUSD
|000| 21108@DESKTOP-ULTBUSD remains 600 seconds
a + b = 10
|001| 21108@DESKTOP-ULTBUSD remains 599 seconds
a + b = 11
```

## 总结

本文内容总结如下：

- 第一点，在命令行启动Java Agent，需要使用`-javaagent:jarpath[=options]`选项，其中的`options`信息会转换成为`premain`方法的`agentArgs`参数。
- 第二点，对于`agentArgs`参数的进一步解析，需要由我们自己来完成。



# Load-Time: inst参数

在`LoadTimeAgent`类当中，有一个`premain`方法，我们关注两个问题：

- 第一个问题，`Instrumentation`是一个接口，它的具体实现是哪个类？
- 第二个问题，是“谁”调用了`LoadTimeAgent.premain()`方法的呢？

```java
public static void premain(String agentArgs, Instrumentation inst)
```

## 查看StackTrace

LoadTimeAgent

通过打印异常堆栈的来查看谁调用了 premain

```java
import java.lang.instrument.Instrumentation;

public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        System.out.println("Premain-Class: " + LoadTimeAgent.class.getName());
        System.out.println("agentArgs: " + agentArgs);
        System.out.println("Instrumentation Class: " + inst.getClass().getName());

        Exception ex = new Exception("Exception from LoadTimeAgent");
        ex.printStackTrace(System.out);
    }
}
```

```
-javaagent:E:\Gitee\bytecode-dance\java-agent\load-time-inst\target\TheAgent.jar
```

运行结果:

```
Premain-Class: com.potato.LoadTimeAgent
agentArgs: null
Instrumentation Class: sun.instrument.InstrumentationImpl
java.lang.Exception: Exception from LoadTimeAgent
	at com.potato.LoadTimeAgent.premain(LoadTimeAgent.java:14)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at sun.instrument.InstrumentationImpl.loadClassAndStartAgent(InstrumentationImpl.java:386)
	at sun.instrument.InstrumentationImpl.loadClassAndCallPremain(InstrumentationImpl.java:401)
29404@DESKTOP-ULTBUSD
|000| 29404@DESKTOP-ULTBUSD remains 600 seconds
a - b = -5
|001| 29404@DESKTOP-ULTBUSD remains 599 seconds
a - b = -8
```

从上面的输出结果中，可以看到InstrumentationImpl.loadClassAndCallPremain方法。

## InstrumentationImpl

`sun.instrument.InstrumentationImpl`实现了`java.lang.instrument.Instrumentation`接口：

```java
public class InstrumentationImpl implements Instrumentation {
}
```

### loadClassAndCallPremain

在`sun.instrument.InstrumentationImpl`类当中，`loadClassAndCallPremain`方法的实现非常简单，它直接调用了`loadClassAndStartAgent`方法：

```java
public class InstrumentationImpl implements Instrumentation {
    private void loadClassAndCallPremain(String classname, String optionsString) throws Throwable {
        loadClassAndStartAgent(classname, "premain", optionsString);
    }
}
```

### loadClassAndCallAgentmain

```java
public class InstrumentationImpl implements Instrumentation {
    private void loadClassAndCallAgentmain(String classname, String optionsString) throws Throwable {
        loadClassAndStartAgent(classname, "agentmain", optionsString);
    }
}
```

### loadClassAndStartAgent

在`sun.instrument.InstrumentationImpl`类当中，`loadClassAndStartAgent`方法的作用就是通过Java反射的机制来对`premain`或`agentmain`方法进行调用。

在`loadClassAndStartAgent`源码中，我们能够看到更多的细节信息：

- 第一步，从自身的方法定义中，去寻找目标方法：先找带有两个参数的方法；如果没有找到，则找带有一个参数的方法。如果第一步没有找到，则进行第二步。
- 第二步，从父类的方法定义中，去寻找目标方法：先找带有两个参数的方法；如果没有找到，则找带有一个参数的方法。

## 总结

本文内容总结如下：

第一点，在premain方法中，Instrumentation接口的具体实现是sun.instrument.InstrumentationImpl类。
第二点，查看Stack Trace，可以看到sun.instrument.InstrumentationImpl.loadClassAndCallPremain方法对LoadTimeAgent.premain方法进行了调用。

# Dynamic: Attach API

## Attach API

在进行Dynamic Instrumentation的时候，我们需要用到Attach API，它允许一个JVM连接到另一个JVM。

> 作用：允许一个JVM连接到另一个JVM

Attach API是在Java 1.6引入的。

> 时间：Java 1.6之后

在Attach API当中，主要的类位于`com.sun.tools.attach`包，它有版本的变化：

- 在Java 8版本，`com.sun.tools.attach`包位于`JDK_HOME/lib/tools.jar`文件。
- 在Java 9版本之后，`com.sun.tools.attach`包位于`jdk.attach`模块（`JDK_HOME/jmods/jdk.attach.jmod`文件）。

> 位置：tools.jar文件或jdk.attach模块

我们主要使用Java 8版本。

![img](img/JavaAgent两种启动方式.assets/virtual-machine-of-dynamic-instrumentation.png)

在`com.sun.tools.attach`包当中，包含的类内容如下：

> com.sun.tools.attach有哪些主要的类

```
                        ┌─── spi ──────────────────────────────┼─── AttachProvider
                        │
                        ├─── AgentInitializationException
                        │
                        ├─── AgentLoadException
                        │
                        ├─── AttachNotSupportedException
com.sun.tools.attach ───┤
                        ├─── AttachOperationFailedException
                        │
                        ├─── AttachPermission
                        │
                        ├─── VirtualMachine
                        │
                        └─── VirtualMachineDescriptor
```

在上面这些类当中，我们忽略掉其中的Exception和Permission类，简化之后如下：

```
                        ┌─── spi ────────────────────────┼─── AttachProvider
                        │
com.sun.tools.attach ───┼─── VirtualMachine
                        │
                        └─── VirtualMachineDescriptor
```

在这三个类当中，核心的类是`VirtualMachine`类，代码围绕着它来展开； `VirtualMachineDescriptor`类比较简单，就是对几个字段（id、provider和display name）的包装； 而`AttachProvider`类提供了底层实现。

## VirtualMachine

A `com.sun.tools.attach.VirtualMachine` represents a Java virtual machine to which this Java virtual machine has attached. The Java virtual machine to which it is attached is sometimes called the **target virtual machine**, or **target VM**.

![img](img/JavaAgent两种启动方式.assets/vm-attach-load-agent-detach.png)

使用`VirtualMachine`类，我们分成三步：

- 第一步，与target VM建立连接，获得一个`VirtualMachine`对象。
- 第二步，使用`VirtualMachine`对象，可以将Agent Jar加载到target VM上，也可以从target VM读取一些属性信息。
- 第三步，与target VM断开连接。

```
                                       ┌──VirtualMachine.attach(String id)
                  ┌─── 1. Get VM ──────┤
                  │                    └─VirtualMachine.attach(VirtualMachineDescriptor vmd)
                  │
                  │                                            ┌─── VirtualMachine.loadAgent(String agent)
                  │                    ┌─── Load Agent ────────┤
VirtualMachine ───┤                    │                       └─── VirtualMachine.loadAgent(String agent, String options)
                  ├─── 2. Use VM ──────┤
                  │                    │                       ┌─── VirtualMachine.getAgentProperties()
                  │                    └─── read properties ───┤
                  │                                            └─── VirtualMachine.getSystemProperties()
                  │
                  └─── 3. detach VM ───┼─── VirtualMachine.detach()
```



## Get VM

attach到 JVM 上有两种方式：通过pid、通过虚拟机描述符

```java
public abstract class VirtualMachine {
    public static VirtualMachine attach(String id) throws AttachNotSupportedException, IOException {
        // ...
    }
}
```

```java
public abstract class VirtualMachine {
    public static VirtualMachine attach(VirtualMachineDescriptor vmd) throws AttachNotSupportedException, IOException {
        // ...
    }
}
```

## Use VM

### 加载agent

```java
public abstract class VirtualMachine {
    public void loadAgent(String agent) throws AgentLoadException, AgentInitializationException, IOException {
        loadAgent(agent, null);
    }

    public abstract void loadAgent(String agent, String options)
        throws AgentLoadException, AgentInitializationException, IOException;    
}
```

String agent表示agent jar包的路径

### 读取属性

加载之后，还可以读取目标虚拟机中的系统属性以及agent的系统属性

```java
public abstract class VirtualMachine {
    public abstract Properties getSystemProperties() throws IOException;
    public abstract Properties getAgentProperties() throws IOException;
}
```

## Detach VM

从目标虚拟机中分离

```java
public abstract class VirtualMachine {
    public abstract void detach() throws IOException;
}
```

## 其他方法

返回target VM的进程ID值。`id()`

```
public abstract class VirtualMachine {
    private final String id;

    public final String id() {
        return id;
    }
}
```

返回一组对象，描述所有潜在的target VM对象。`list()VirtualMachineDescriptor`

```java
public abstract class VirtualMachine {
    public static List<VirtualMachineDescriptor> list() {
        // ...
    }
}
```

返回一个对象。`provider()``AttachProvider`

```java
public abstract class VirtualMachine {
    private final AttachProvider provider;

    public final AttachProvider provider() {
        return provider;
    }
}
```

## 虚拟机描述符

VirtualMachineDescriptor 描述一个虚拟机的

com.sun.tools.attach.VirtualMachineDescriptor

```java
public class VirtualMachineDescriptor {
    private String id;//进程id
    private String displayName;
    private AttachProvider provider;//对尝试附加到虚拟机时应使用的 的引用

    public String id() {
        return id;
    }
    
    public String displayName() {
        return displayName;
    }
    
    public AttachProvider provider() {
        return provider;
    }    
}
```

通过调用 VirtualMachine.list()  方法返回 VirtualMachineDescriptor 对象

## AttachProvider

com.sun.tools.attach.spi.AttachProvider是一个抽象类，它需要一个具体的实现：

```java
public abstract class AttachProvider {
    //...
}
```

不同平台上的JVM，它对应的具体实现是不一样的：

- Linux: sun.tools.attach.LinuxAttachProvider

- Windows: sun.tools.attach.WindowsAttachProvider

AttachProvider 的实现通常与Java虚拟机的实现、版本、平台、甚至操作模式相关联，例如，Sun 的 JDK 实现的AttachProvider 只能附加到 Sun 的 HotSpot 虚拟机

## 总结

本文内容总结如下：

- 第一点，Attach API位于包：

  ```plaintext
  com.sun.tools.attach
  ```

  - 在Java 8版本，包位于文件。`com.sun.tools.attach``JDK_HOME/lib/tools.jar`
  - 在Java 9版本之后，包位于模块（文件）。`com.sun.tools.attach``jdk.attach``JDK_HOME/jmods/jdk.attach.jmod`

- 第二点，在包当中，重要的类有三个：（核心功能）、（三个属性）和（底层实现）。`com.sun.tools.attach``VirtualMachine``VirtualMachineDescriptor``AttachProvider`

- 第三点，使用类分成三步：

  ```plaintext
  VirtualMachine
  ```

  - 第一步，与target VM建立连接，获得一个对象。`VirtualMachine`
  - 第二步，使用对象，可以将Agent Jar加载到target VM上，也可以从target VM读取一些属性信息。`VirtualMachine`
  - 第三步，与target VM断开连接。

# Dynamic: Attach API示例

## 目标程序

还是上面的load-Time

## agent

```java
public static void agentmain(String agentArgs, Instrumentation inst) {
    Class<?> agentClass = DynamicAgent.class;
    System.out.println("Agent-Class: " + agentClass.getName());
    System.out.println("agentArgs: " + agentArgs);
    System.out.println("Instrumentation: " + inst.getClass().getName());
    System.out.println("ClassLoader: " + agentClass.getClassLoader());
    System.out.println("Thread Id: " + Thread.currentThread().getName() + "@" +
            Thread.currentThread().getId() + "(" + Thread.currentThread().isDaemon() + ")"
    );
    System.out.println("Can-Redefine-Classes: " + inst.isRedefineClassesSupported());
    System.out.println("Can-Retransform-Classes: " + inst.isRetransformClassesSupported());
    System.out.println("Can-Set-Native-Method-Prefix: " + inst.isNativeMethodPrefixSupported());
    System.out.println("========= ========= =========");
}
```

## Attach

### 演示一

演示attach和deattach、获取目标虚拟机中的 System Properties、获取目标虚拟机中的 Agent Properties

```java
public class VMAttach {
    public static void main(String[] args) throws IOException, AttachNotSupportedException,
            AgentLoadException, AgentInitializationException {
        // 注意：需要修改pid的值
        String pid = "23176";
        String agentPath = "E:\\Gitee\\bytecode-dance\\java-agent\\attach-demo\\target\\TheAgent.jar";
        VirtualMachine vm = VirtualMachine.attach(pid);
        vm.loadAgent(agentPath, "Hello JVM Attach");

        //获取目标虚拟机中的 System Properties
        Properties systemProperties = vm.getSystemProperties();
        systemProperties.list(System.out);

        System.out.println("=====");

        //获取目标虚拟机中的 Agent Properties
        Properties agentProperties = vm.getAgentProperties();
        agentProperties.list(System.out);

        System.out.println("=====");


        vm.detach();
    }
```

运行结果

```
-- listing properties --
java.runtime.name=Java(TM) SE Runtime Environment
sun.boot.library.path=C:\Program Files\Java\jdk1.8.0_301\jr...
java.vm.version=25.301-b09
java.vm.vendor=Oracle Corporation
java.vendor.url=http://java.oracle.com/
path.separator=;
java.vm.name=Java HotSpot(TM) 64-Bit Server VM
file.encoding.pkg=sun.io
user.script=
user.country=CN
sun.java.launcher=SUN_STANDARD
sun.os.patch.level=
java.vm.specification.name=Java Virtual Machine Specification
user.dir=E:\Gitee\bytecode-dance\java-agent
java.runtime.version=1.8.0_301-b09
java.awt.graphicsenv=sun.awt.Win32GraphicsEnvironment
java.endorsed.dirs=C:\Program Files\Java\jdk1.8.0_301\jr...
os.arch=amd64
visualvm.id=613640677725300
java.io.tmpdir=C:\Users\20522\AppData\Local\Temp\
line.separator=

java.vm.specification.vendor=Oracle Corporation
user.variant=
os.name=Windows 10
sun.jnu.encoding=GBK
java.library.path=C:\Program Files\Java\jdk1.8.0_301\bi...
java.specification.name=Java Platform API Specification
java.class.version=52.0
sun.management.compiler=HotSpot 64-Bit Tiered Compilers
os.version=10.0
user.home=C:\Users\20522
user.timezone=Asia/Shanghai
java.awt.printerjob=sun.awt.windows.WPrinterJob
file.encoding=UTF-8
java.specification.version=1.8
java.class.path=C:\Program Files\Java\jdk1.8.0_301\jr...
user.name=20522
java.vm.specification.version=1.8
sun.java.command=sample.Program
java.home=C:\Program Files\Java\jdk1.8.0_301\jre
sun.arch.data.model=64
user.language=zh
java.specification.vendor=Oracle Corporation
awt.toolkit=sun.awt.windows.WToolkit
java.vm.info=mixed mode
java.version=1.8.0_301
java.ext.dirs=C:\Program Files\Java\jdk1.8.0_301\jr...
sun.boot.class.path=C:\Program Files\Java\jdk1.8.0_301\jr...
java.vendor=Oracle Corporation
file.separator=\
java.vendor.url.bug=http://bugreport.sun.com/bugreport/
sun.io.unicode.encoding=UnicodeLittle
sun.cpu.endian=little
sun.desktop=windows
sun.cpu.isalist=amd64
=====
-- listing properties --
sun.jvm.args=-Dvisualvm.id=613640677725300 -javaag...
sun.jvm.flags=
sun.java.command=sample.Program
=====

进程已结束,退出代码0

```



### 演示二

遍历所有的VirtualMachineDescriptor

```java
@Test
public void main2() throws IOException, AttachNotSupportedException {
    List<VirtualMachineDescriptor> list = VirtualMachine.list();

    for (VirtualMachineDescriptor vmd : list) {
        String id = vmd.id();
        String displayName = vmd.displayName();
        AttachProvider provider = vmd.provider();
        System.out.println("Id: " + id);
        System.out.println("Name: " + displayName);
        System.out.println("Provider: " + provider);
        System.out.println("=====================");
    }
}
```

结果：

```
Id: 30672
Name: 
Provider: sun.tools.attach.WindowsAttachProvider@41a4555e
=====================
Id: 17940
Name: com.intellij.rt.junit.JUnitStarter -ideVersion5 -junit4 com.potato.VMAttach,main2
Provider: sun.tools.attach.WindowsAttachProvider@41a4555e
=====================
Id: 24468
Name: org.jetbrains.jps.cmdline.Launcher C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/plugins/java/lib/jps-builders.jar;C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/plugins/java/lib/jps-builders-6.jar;C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/plugins/java/lib/jps-javac-extension-1.jar;C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/lib/util.jar;C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/lib/annotations.jar;C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/lib/3rd-party-rt.jar;C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/lib/jna.jar;C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/lib/kotlin-stdlib-jdk8.jar;C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/lib/protobuf.jar;C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/lib/platform-api.jar;C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/lib/jps-model.jar;C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/plugins/java/lib/javac2.jar;C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/lib/forms_rt.jar;C:/Program Files/JetBrains/IntelliJ IDEA 2021.3/plugins/ja
Provider: sun.tools.attach.WindowsAttachProvider@41a4555e
=====================
Id: 15432
Name: org.jetbrains.idea.maven.server.RemoteMavenServer36
Provider: sun.tools.attach.WindowsAttachProvider@41a4555e
=====================
Id: 23176
Name: sample.Program
Provider: sun.tools.attach.WindowsAttachProvider@41a4555e
=====================
Id: 15708
Name: org.jetbrains.idea.maven.server.RemoteMavenServer36
Provider: sun.tools.attach.WindowsAttachProvider@41a4555e
=====================
Id: 26956
Name: 
Provider: sun.tools.attach.WindowsAttachProvider@41a4555e
=====================

进程已结束,退出代码0

```

### 演示三

```java
@Test
public void main3() throws IOException, AttachNotSupportedException {
    List<VirtualMachineDescriptor> list = VirtualMachine.list();

    String className = "sample.Program";
    VirtualMachine vm = null;
    for (VirtualMachineDescriptor item : list) {
        String displayName = item.displayName();
        if (displayName != null && displayName.equals(className)) {
            vm = VirtualMachine.attach(item);
            break;
        }
    }

    if (vm != null) {
        Properties properties = vm.getSystemProperties();
        properties.list(System.out);
        vm.detach();
    }
}
```

结果：

```
-- listing properties --
java.runtime.name=Java(TM) SE Runtime Environment
sun.boot.library.path=C:\Program Files\Java\jdk1.8.0_301\jr...
java.vm.version=25.301-b09
java.vm.vendor=Oracle Corporation
java.vendor.url=http://java.oracle.com/
path.separator=;
java.vm.name=Java HotSpot(TM) 64-Bit Server VM
file.encoding.pkg=sun.io
user.script=
user.country=CN
sun.java.launcher=SUN_STANDARD
sun.os.patch.level=
java.vm.specification.name=Java Virtual Machine Specification
user.dir=E:\Gitee\bytecode-dance\java-agent
java.runtime.version=1.8.0_301-b09
java.awt.graphicsenv=sun.awt.Win32GraphicsEnvironment
java.endorsed.dirs=C:\Program Files\Java\jdk1.8.0_301\jr...
os.arch=amd64
visualvm.id=613640677725300
java.io.tmpdir=C:\Users\20522\AppData\Local\Temp\
line.separator=

java.vm.specification.vendor=Oracle Corporation
user.variant=
os.name=Windows 10
sun.jnu.encoding=GBK
java.library.path=C:\Program Files\Java\jdk1.8.0_301\bi...
java.specification.name=Java Platform API Specification
java.class.version=52.0
sun.management.compiler=HotSpot 64-Bit Tiered Compilers
os.version=10.0
user.home=C:\Users\20522
user.timezone=Asia/Shanghai
java.awt.printerjob=sun.awt.windows.WPrinterJob
file.encoding=UTF-8
java.specification.version=1.8
java.class.path=C:\Program Files\Java\jdk1.8.0_301\jr...
user.name=20522
java.vm.specification.version=1.8
sun.java.command=sample.Program
java.home=C:\Program Files\Java\jdk1.8.0_301\jre
sun.arch.data.model=64
user.language=zh
java.specification.vendor=Oracle Corporation
awt.toolkit=sun.awt.windows.WToolkit
java.vm.info=mixed mode
java.version=1.8.0_301
java.ext.dirs=C:\Program Files\Java\jdk1.8.0_301\jr...
sun.boot.class.path=C:\Program Files\Java\jdk1.8.0_301\jr...
java.vendor=Oracle Corporation
file.separator=\
java.vendor.url.bug=http://bugreport.sun.com/bugreport/
sun.io.unicode.encoding=UnicodeLittle
sun.cpu.endian=little
sun.desktop=windows
sun.cpu.isalist=amd64

```

## AttachProvider

```
import com.sun.tools.attach.VirtualMachine;
import com.sun.tools.attach.spi.AttachProvider;

public class VMAttach {
    public static void main(String[] args) throws Exception {
        // 注意：需要修改pid的值
        String pid = "1234";
        VirtualMachine vm = VirtualMachine.attach(pid);
        AttachProvider provider = vm.provider();
        String name = provider.name();
        String type = provider.type();
        System.out.println("Provider Name: " + name);
        System.out.println("Provider Type: " + type);
        System.out.println("Provider Impl: " + provider.getClass());
    }
}
```

Windows 7环境下输出：

```
Provider Name: sun
Provider Type: windows
Provider Impl: class sun.tools.attach.WindowsAttachProvider
```

Ubuntu 20环境下输出：

```
Provider Name: sun
Provider Type: socket
Provider Impl: class sun.tools.attach.LinuxAttachProvider
```

# agent和attach对比

https://lsieun.github.io/java-agent/s01ch04/load-time-vs-dynamic-agent.html

前面的例子中Load-Time Instrumentation使用的是agent, Dynamic Instrumentation 使用的是attach

## 虚拟机数量

Load-Time Instrumentation只涉及到一个虚拟机：

![img](img/JavaAgent两种启动方式.assets/virtual-machine-of-load-time-instrumentation.png)

Dynamic Instrumentation涉及到两个虚拟机：

![img](img/JavaAgent两种启动方式.assets/virtual-machine-of-dynamic-instrumentation-16544194182964.png)

## 时机不同

在进行Load-Time Instrumentation时，会执行Agent Jar当中的`premain()`方法；`premain()`方法是先于`main()`方法执行，此时Application当中使用的**大多数类还没有被加载**。

在进行Dynamic Instrumentation时，会执行Agent Jar当中的`agentmain()`方法；而`agentmain()`方法是往往是在`main()`方法之后执行，此时Application当中使用的**大多数类已经被加载**。

## 能力不同

Load-Time Instrumentation可以做很多事情：添加和删除字段、添加和删除方法等。

Dynamic Instrumentation做的事情比较有限，大多集中在对于方法体的修改。

## 线程不同

Load-Time Instrumentation是运行在`main`线程里：

```java
Thread Id: main@1(false)
package lsieun.agent;

import lsieun.utils.*;

import java.lang.instrument.Instrumentation;

public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        // 第一步，打印信息：agentArgs, inst, classloader, thread
        PrintUtils.printAgentInfo(LoadTimeAgent.class, "Premain-Class", agentArgs, inst);
    }
}
```

Dynamic Instrumentation是运行在`Attach Listener`线程里：

```java
Thread Id: Attach Listener@5(true)
package lsieun.agent;

import lsieun.utils.*;

import java.lang.instrument.Instrumentation;

public class DynamicAgent {
    public static void agentmain(String agentArgs, Instrumentation inst) {
        // 第一步，打印信息：agentArgs, inst, classloader, thread
        PrintUtils.printAgentInfo(DynamicAgent.class, "Agent-Class", agentArgs, inst);
    }
}
```

## Exception处理

在处理Exception的时候，Load-Time Instrumentation和Dynamic Instrumentation有差异： [Link](https://docs.oracle.com/javase/8/docs/api/java/lang/instrument/package-summary.html)

- 当Load-Time Instrumentation时，出现异常，会报告错误信息，并且停止执行，退出虚拟机。
- 当Dynamic Instrumentation时，出现异常，会报告错误信息，但是不会停止虚拟机，而是继续执行。

### Agent Class不存在

 **Load-Time Agent**

在`pom.xml`中，修改`Premain-Class`属性，指向一个不存在的Agent Class：

```xml
<Premain-Class>lsieun.agent.NonExistentAgent</Premain-Class>
```

会出现`ClassNotFoundException`异常：

```
$ java -cp ./target/classes/ -javaagent:./target/TheAgent.jar sample.Program
Exception in thread "main" java.lang.ClassNotFoundException: lsieun.agent.NonExistentAgent
        at java.net.URLClassLoader.findClass(URLClassLoader.java:382)
        at java.lang.ClassLoader.loadClass(ClassLoader.java:418)
        at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:355)
        at java.lang.ClassLoader.loadClass(ClassLoader.java:351)
        at sun.instrument.InstrumentationImpl.loadClassAndStartAgent(InstrumentationImpl.java:304)
        at sun.instrument.InstrumentationImpl.loadClassAndCallPremain(InstrumentationImpl.java:401)
FATAL ERROR in native method: processing of -javaagent failed
```



**Dynamic Agent**

在`pom.xml`中，修改`Agent-Class`属性，指向一个不存在的Agent Class：

```
<Agent-Class>lsieun.agent.NonExistentAgent</Agent-Class>
```

会出现`ClassNotFoundException`异常：

```
Exception in thread "Attach Listener" java.lang.ClassNotFoundException: lsieun.agent.NonExistentAgent
	at java.net.URLClassLoader.findClass(URLClassLoader.java:382)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:418)
	at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:355)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:351)
	at sun.instrument.InstrumentationImpl.loadClassAndStartAgent(InstrumentationImpl.java:304)
	at sun.instrument.InstrumentationImpl.loadClassAndCallAgentmain(InstrumentationImpl.java:411)
Agent failed to start!
```



### incompatible xxx-main

**Load-Time Agent**

如果`premain`方法不符合规范：

```java
public class LoadTimeAgent {
    public static void premain() {
        // do nothing
    }
}
```

会出现`NoSuchMethodException`异常：

```
$ java -cp ./target/classes/ -javaagent:./target/TheAgent.jar sample.Program
Exception in thread "main" java.lang.NoSuchMethodException: lsieun.agent.LoadTimeAgent.premain(java.lang.String, java.lang.instrument.Instrumentation)
        at java.lang.Class.getDeclaredMethod(Class.java:2130)
        at sun.instrument.InstrumentationImpl.loadClassAndStartAgent(InstrumentationImpl.java:327)
        at sun.instrument.InstrumentationImpl.loadClassAndCallPremain(InstrumentationImpl.java:401)
FATAL ERROR in native method: processing of -javaagent failed
```



**Dynamic Agent**

如果`agentmain`方法不符合规范：

```java
public class DynamicAgent {
    public static void agentmain() {
        // do nothing
    }
}
```

会出现`NoSuchMethodException`异常：

```
Exception in thread "Attach Listener" java.lang.NoSuchMethodException: lsieun.agent.DynamicAgent.agentmain(java.lang.String, java.lang.instrument.Instrumentation)
	at java.lang.Class.getDeclaredMethod(Class.java:2130)
	at sun.instrument.InstrumentationImpl.loadClassAndStartAgent(InstrumentationImpl.java:327)
	at sun.instrument.InstrumentationImpl.loadClassAndCallAgentmain(InstrumentationImpl.java:411)
Agent failed to start!
```



### 抛出异常

**Load-Time Agent**

```java
import java.lang.instrument.Instrumentation;

public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        throw new RuntimeException("exception from LoadTimeAgent");
    }
}
```



```
$ java -cp ./target/classes/ -javaagent:./target/TheAgent.jar sample.Program
Exception in thread "main" java.lang.reflect.InvocationTargetException
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:498)
        at sun.instrument.InstrumentationImpl.loadClassAndStartAgent(InstrumentationImpl.java:386)
        at sun.instrument.InstrumentationImpl.loadClassAndCallPremain(InstrumentationImpl.java:401)
Caused by: java.lang.RuntimeException: exception from LoadTimeAgent
        at lsieun.agent.LoadTimeAgent.premain(LoadTimeAgent.java:7)
        ... 6 more
FATAL ERROR in native method: processing of -javaagent failed
```

**Dynamic Agent**

```java
import java.lang.instrument.Instrumentation;

public class DynamicAgent {
    public static void agentmain(String agentArgs, Instrumentation inst) {
        throw new RuntimeException("exception from DynamicAgent");
    }
}
```



```
Exception in thread "Attach Listener" java.lang.reflect.InvocationTargetException
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at sun.instrument.InstrumentationImpl.loadClassAndStartAgent(InstrumentationImpl.java:386)
	at sun.instrument.InstrumentationImpl.loadClassAndCallAgentmain(InstrumentationImpl.java:411)
Caused by: java.lang.RuntimeException: exception from DynamicAgent
	at lsieun.agent.DynamicAgent.agentmain(DynamicAgent.java:7)
	... 6 more
Agent failed to start!
```



## 总结

本文内容总结如下：

- 第一点，虚拟机的数量不同。
- 第二点，时机不同和能力不同。
- 第三点，线程不同。
- 第四点，处理异常的方式不同。

1
