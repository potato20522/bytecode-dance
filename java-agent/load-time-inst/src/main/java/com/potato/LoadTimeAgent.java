package com.potato;

import java.lang.instrument.Instrumentation;

/**
 * 通过打印异常堆栈的来查看谁调用了 premain
 */
public class LoadTimeAgent {
    public static void premain(String agentArgs, Instrumentation inst) {
        System.out.println("Premain-Class: " + LoadTimeAgent.class.getName());
        System.out.println("agentArgs: " + agentArgs);
        System.out.println("Instrumentation Class: " + inst.getClass().getName());

        Exception ex = new Exception("Exception from LoadTimeAgent");
        ex.printStackTrace(System.out);
    }
}