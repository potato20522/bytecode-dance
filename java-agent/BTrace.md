# 简介

[性能工具之Java调试工具BTrace入门 - 云+社区 - 腾讯云 (tencent.com)](https://cloud.tencent.com/developer/article/1465647)

[Btrace官方教程-中文版 - 落烨无痕 - 博客园 (cnblogs.com)](https://www.cnblogs.com/danny-djy/p/9990566.html)

[首页 ·btraceio/btrace Wiki (github.com)](https://github.com/btraceio/btrace/wiki)



BTrace是Java的安全动态跟踪工具，通过Java Attach技术，将字节码注入到正在运行的目标程序，达到追踪的效果。BTrace基于ASM、Java Attach Api、Instruments开发。

BTrace的最大好处,是可以通过自己编写的脚本,获取应用的一切调用信息。而不需要不断地修改代码，不重启应用，不上线的情况下，查看指定方法的运行环境，如：方法出入参，运行环境，方法耗时等运行时环境。正因为 BTrace 是动态追踪，尽量避免影响线上服务的可用性，在编写BTrace 脚本，也如下约定：

- 不能创建新的对象
- 不能创建新的数组
- 不能抛出异常
- 不能捕获异常
- 不能对实例或静态方法调用-只有从BTraceUtils中的public static方法中或在当前脚本中声明的方法，可以被BTrace调用
- 不能有外部，内部，嵌套或本地类
- 不能有同步块或同步方法
- 不能有循环（for，while，do..while）
- 不能继承抽象类（父类必须是java.lang.Object）
- 不能实现接口
- 不能有断言语句
- 不能有class保留字

以上的限制可以通过通过unsafe模式绕过。追踪脚本和引擎都必须设置为unsafe模式。脚本需要使用注解为 `@BTrace(unsafe=true)`，需要修改BTrace安装目录下bin中btrace脚本将 `-Dcom.sun.btrace.unsafe=false`改为 `-Dcom.sun.btrace.unsafe=true`。