package com.potato;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

/**
 * 自定义ClassVisitor，修改foo方法字节码
 */
public class MyClassVisitor extends ClassVisitor {
    private static boolean hasModified = false;

    public MyClassVisitor(int api, ClassVisitor classVisitor) {
        super(api, classVisitor);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
                                     String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);
        if ("foo".equals(name) && !hasModified) {
            System.out.println("----准备修改foo方法----");
            hasModified = true;
            return new MyMethodVisitor(api, mv, access, name, descriptor);
        }
        return mv;
    }
}