package com.potato;

import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;

/**
 * agent主程序,agentmain方法在目标进程的main之后运行
 */
public class MyBizAgentMain {
    public static void agentmain(String agentArgs, Instrumentation inst) throws UnmodifiableClassException {
        System.out.println("---agent called---");
        inst.addTransformer(new MyClassFileTransformer(),true);//添加类文件转换器，第二个参数必须设置为true，表示可以重新转换类文件
        Class[] classes = inst.getAllLoadedClasses();
        for (Class aClass : classes) {
            if ("com.potato.biz.MyBizMain".equals(aClass.getName())) {
                System.out.println("----重新加载MyBizMain开始----");
                inst.retransformClasses(aClass);
                System.out.println("----重新加载MyBizMain完毕----");
                break;
            }
        }
    }
    public static void premain(String agentArgs, Instrumentation inst){
        System.out.println("premain success");
    }
}