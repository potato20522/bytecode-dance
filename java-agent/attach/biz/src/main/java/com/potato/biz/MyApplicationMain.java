package com.potato.biz;

import java.util.Random;

/**
 * 我们的目的是在其打印过程中，
 * 通过java agent将其打印的内容修改为“------我是MyBizMain的Agent-----”
 */
public class MyApplicationMain {
    static Random r = new Random();

    public String foo(int input) {
        try {
            Thread.sleep(input);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "------我是MyBizMain-----";
    }

    public static void main(String[] args) throws InterruptedException {
        MyApplicationMain myApplicationMain = new MyApplicationMain();
        while (true) {
            System.out.println(myApplicationMain.foo(r.nextInt(10) * 1000));
        }
    }

}