package com.potato.bytecode;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.Modifier;
import javassist.bytecode.ClassFile;

import java.lang.reflect.Method;

/**
 * @author rickiyang
 * @date 2019-08-07
 * @Desc
 */
public class UpdatePerson {

    private static String getFilePath() {
        return UpdatePerson.class.getClassLoader().getResource("").getFile();
    }



    public static void update() throws Exception {
        ClassPool pool = ClassPool.getDefault();
        CtClass cc = pool.get(PersonService.class.getName());

        CtMethod personFly = cc.getDeclaredMethod("personFly");
        personFly.insertBefore("System.out.println(\"起飞之前准备降落伞\");");
        personFly.insertAfter("System.out.println(\"成功落地。。。。\");");


        //新增一个方法
        CtMethod ctMethod = new CtMethod(CtClass.voidType, "joinFriend", new CtClass[]{}, cc);
        ctMethod.setModifiers(Modifier.PUBLIC);
        ctMethod.setBody("{System.out.println(\"i want to be your friend\");}");
        cc.addMethod(ctMethod);
        cc.writeFile(getFilePath());

        //新增语句


        //cc.writeFile();
        //Object person = cc.toClass().newInstance();
        // 调用 personFly 方法
        //Method personFlyMethod = person.getClass().getMethod("personFly");
        //personFlyMethod.invoke(person);
        //调用 joinFriend 方法
        //Method execute = person.getClass().getMethod("joinFriend");
        //execute.invoke(person);
    }

    public static void main(String[] args) {
        try {
            update();
        } catch (Exception e) {
            e.printStackTrace();
        }
        PersonService obj = new PersonService();
        obj.personFly();

    }
}