package com.potato.bytecode.classvisitor;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;

public class SingletonVisitor extends ClassVisitor {
    public SingletonVisitor(int api, ClassVisitor classVisitor) {
        super(api, classVisitor);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);
        if (mv != null && "<init>".equals(name)) {
            mv = new InitMethodAdapter(api, mv);
        }
        return mv;
    }

    private static class InitMethodAdapter extends MethodVisitor {
        public InitMethodAdapter(int api, MethodVisitor methodVisitor) {
            super(api, methodVisitor);
        }

        @Override
        public void visitCode() {
            super.visitVarInsn(ALOAD, 0);
            super.visitCode();
            super.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);

        }
    }

}
