package sample;

public class Singleton {
    public static Singleton obj = new Singleton();
    public Singleton() {
        System.out.println("hello");
        System.out.println(obj);
    }
//    public Singleton build() {
//        return obj;
//    }

    public static void main(String[] args) {
        Singleton s1 = new Singleton();
        Singleton s2 = new Singleton();
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s1==s2);
    }
}
