package com.potato.bytecode;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

import java.io.IOException;
import java.io.InputStream;

import static org.objectweb.asm.Opcodes.ASM9;

public final class BCUtils {
    public static byte[] read(Class<?> type) {
        return read(type.getName());
    }

    public static byte[] read(String className) {
        ClassReader classReader = null;
        try {
            classReader = new ClassReader(className);
        } catch (IOException e) {
            throw new RuntimeException("字节码读取失败");
        }
        ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        ClassVisitor classVisitor = new ClassVisitor(ASM9, classWriter) {
        };
        int parsingOptions = ClassReader.SKIP_FRAMES;
        classReader.accept(classVisitor, parsingOptions);
        return classWriter.toByteArray();
    }

    public static byte[] read2(String className) {
        byte[] data = null;
        String jarname = "/" + className.replace('.', '/') + ".class";
        try (InputStream is = BCUtils.class.getResourceAsStream(jarname)) {
            if (is == null) {
                throw new RuntimeException("can not found bytecode of " + className);
            }
            data = new byte[is.available()];
            is.read(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
}
