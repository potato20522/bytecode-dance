package com.potato.bytecode.exception;

public class ReadClassByteCodeException extends RuntimeException{
    public ReadClassByteCodeException(String message) {
        super(message);
    }
}
