package com.potato.bytecode;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

class ByteCodeUtilsTest {

    @Test
    void readByteCode() {
        byte[] bytes = BCUtils.read(BCUtils.class);
        System.out.println(new String(bytes));
    }
    @Test
    void readByteCode2() {
        byte[] bytes1 = BCUtils.read(BCUtils.class);
        System.out.println(new String(bytes1));
        byte[] bytes2 = BCUtils.read2(BCUtils.class.getName());
        System.out.println("=================");
        System.out.println(new String(bytes2));
        System.out.println(Arrays.equals(bytes1, bytes2));
    }

}