package sample;

/**
 * 预期目标：在下面的HelloWorld类中，我们定义了一个clone()方法，
 * 但存在一个问题，也就是，如果没有实现Cloneable接口，clone()方法就会出错，
 * 我们的目标是希望通过ASM为HelloWorld类添加上Cloneable接口。
 */
public class HelloWorld {
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}