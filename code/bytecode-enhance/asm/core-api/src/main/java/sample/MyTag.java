package sample;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface MyTag {
    String value() default "";
    String name();
    int age();
    String[] path() default {};
    int[] path2() default {};
    IdType type() default IdType.NONE;
}