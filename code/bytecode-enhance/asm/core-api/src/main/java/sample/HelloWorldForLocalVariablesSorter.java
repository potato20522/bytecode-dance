package sample;

import java.util.Random;

public class HelloWorldForLocalVariablesSorter {
    public void test(int a, int b) throws Exception {
        int c = a + b;
        int d = c * 10;
        Random rand = new Random();
        int value = rand.nextInt(d);
        Thread.sleep(value);
    }
}
