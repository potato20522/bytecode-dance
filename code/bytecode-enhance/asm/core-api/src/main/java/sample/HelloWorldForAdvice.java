package sample;

import com.potato.bytecode.utilandcommons.ClassPrintParameterVisitor;

/**
 * 想实现的预期目标：打印出构造方法（<init>()）和test()的参数和返回值。
 * {@link ClassPrintParameterVisitor}
 */
public class HelloWorldForAdvice {
    private String name;
    private int age;

    public HelloWorldForAdvice(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void test(long idCard, Object obj) {
        int hashCode = 0;
        hashCode += name.hashCode();
        hashCode += age;
        hashCode += (int) (idCard % Integer.MAX_VALUE);
        hashCode += obj.hashCode();
        hashCode = Math.abs(hashCode);
        System.out.println("Hash Code is " + hashCode);
        if (hashCode % 2 == 1) {
            throw new RuntimeException("illegal");
        }
    }
}
