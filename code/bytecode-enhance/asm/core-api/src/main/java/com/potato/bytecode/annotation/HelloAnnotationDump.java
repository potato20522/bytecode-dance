package com.potato.bytecode.annotation;

import com.potato.bytecode.FileUtils;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;

import static org.objectweb.asm.Opcodes.*;

public class HelloAnnotationDump {
    public static void main(String[] args) throws Exception {
        String relative_path = "sample/HelloAnnotation.class";
        String filepath = FileUtils.getFilePath(relative_path);

        // (1) 生成byte[]内容
        byte[] bytes = dump();

        // (2) 保存byte[]到文件
        FileUtils.writeBytes(filepath, bytes);
    }

    public static byte[] dump() throws Exception {

        ClassWriter classWriter = new ClassWriter(0);
        MethodVisitor methodVisitor;
        AnnotationVisitor annotationVisitor0;

        classWriter.visit(V1_8, ACC_PUBLIC | ACC_SUPER, "sample/HelloAnnotation", null, "java/lang/Object", null);

        {
            annotationVisitor0 = classWriter.visitAnnotation("Lsample/MyTag;", true);
            annotationVisitor0.visit("name", "zhangsan");
            annotationVisitor0.visit("age", 11);
            {
                AnnotationVisitor annotationVisitor1 = annotationVisitor0.visitArray("path");
                annotationVisitor1.visit(null, "aa");
                annotationVisitor1.visit(null, "bb");
                annotationVisitor1.visitEnd();
            }
            annotationVisitor0.visit("path2", new int[]{1, 2, 3});
            annotationVisitor0.visitEnum("type", "Lsample/IdType;", "AUTO");
            annotationVisitor0.visitEnd();
        }

        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            methodVisitor.visitCode();
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
            methodVisitor.visitInsn(RETURN);
            methodVisitor.visitMaxs(1, 1);
            methodVisitor.visitEnd();
        }
        classWriter.visitEnd();

        return classWriter.toByteArray();
    }
}