package com.potato.bytecode.utilandcommons.merge;

public class HelloWorld {
    static {
        System.out.println("This is static initialization method");
    }

    private String name;
    private int age;

    public HelloWorld() {
        this("tomcat", 10);
    }

    public HelloWorld(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void test() {
        System.out.println("This is test method.");
    }

    @Override
    public String toString() {
        return String.format("HelloWorld { name='%s', age=%d }", name, age);
    }
}