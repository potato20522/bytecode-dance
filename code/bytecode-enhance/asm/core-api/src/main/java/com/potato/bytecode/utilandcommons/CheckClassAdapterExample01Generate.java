package com.potato.bytecode.utilandcommons;

import com.potato.bytecode.FileUtils;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.util.CheckClassAdapter;

import static org.objectweb.asm.Opcodes.*;

/**
 * 可以借助于CheckClassAdapter类来检查生成的字节码内容是否正确
 * 方式一：在生成类或转换类的过程中进行检查
 * // 第一步，应用于Class Generation
 * // 串联ClassVisitor：cv --- cca --- cw
 * ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
 * CheckClassAdapter cca = new CheckClassAdapter(cw);
 * ClassVisitor cv = new MyClassVisitor(cca);
 * <p>
 * // 第二步，应用于Class Transformation
 * byte[] bytes = ... // 这里是class file bytes
 * ClassReader cr = new ClassReader(bytes);
 * cr.accept(cv, 0);
 */
public class CheckClassAdapterExample01Generate {
    public static String OBJECT = "java/lang/Object";

    public static void main(String[] args) {
        String classPackageName = "sample/HelloWorld";
        String relativePath = classPackageName + ".class";
        String filepath = FileUtils.getFilePath(relativePath);

        // (1) 生成byte[]内容
        byte[] bytes = dump(classPackageName);

        // (2) 保存byte[]到文件
        FileUtils.writeBytes(filepath, bytes);
    }

    public static byte[] dump(String classPackageName) {
        //1.创建ClassWriter对象
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        CheckClassAdapter cv = new CheckClassAdapter(cw);
        //2.调用visitXxx()方法
        cv.visit(V1_8, ACC_PUBLIC + ACC_SUPER, classPackageName, null, OBJECT, null);
        {
            FieldVisitor fv = cv.visitField(ACC_PRIVATE, "initValue", "I", null, null);
            fv.visitEnd();
        }
        {
            MethodVisitor mv = cv.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            mv.visitCode();
            mv.visitVarInsn(ALOAD, 0);
            mv.visitMethodInsn(INVOKESPECIAL, OBJECT, "<init>", "()V", false);
            mv.visitInsn(RETURN);
            mv.visitMaxs(1, 1);
            mv.visitEnd();
        }
        {
            MethodVisitor mv2 = cv.visitMethod(ACC_PUBLIC, "test", "()V", null, null);
            mv2.visitCode();
            mv2.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
            mv2.visitLdcInsn("Hello World");
            mv2.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
            mv2.visitInsn(RETURN);
            mv2.visitMaxs(2, 1);
            mv2.visitEnd();
        }
        cv.visitEnd();

        return cw.toByteArray();
    }
}
