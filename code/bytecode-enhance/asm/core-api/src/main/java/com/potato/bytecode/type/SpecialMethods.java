package com.potato.bytecode.type;


import org.junit.jupiter.api.Test;
import org.objectweb.asm.Type;

public class SpecialMethods {
    @Test
    void getArrayDimensions() {
        Type t = Type.getType("[[[[[Ljava/lang/String;");

        int dimensions = t.getDimensions();
        Type elementType = t.getElementType();

        System.out.println(dimensions);    // 5
        System.out.println(elementType);   // Ljava/lang/String;
    }

    @Test
    void getArgumentTypes() {
        Type methodType = Type.getMethodType("(Ljava/lang/String;I)V");

        String descriptor = methodType.getDescriptor();
        Type[] argumentTypes = methodType.getArgumentTypes();
        Type returnType = methodType.getReturnType();

        System.out.println("Descriptor: " + descriptor);
        System.out.println("Argument Types:");
        for (Type t : argumentTypes) {
            System.out.println("    " + t);
        }
        System.out.println("Return Type: " + returnType);
    }

    @Test
    void getSize() {
        Type intType = Type.INT_TYPE;
        System.out.println(intType.getSize()); // 1

        Type longType = Type.LONG_TYPE;
        System.out.println(longType.getSize()); // 2

        Type t = Type.getMethodType("(II)I");
        int value = t.getArgumentsAndReturnSizes();

        int argumentsSize = value >> 2;
        int returnSize = value & 0b11;

        System.out.println(argumentsSize); // 3
        System.out.println(returnSize);    // 1
    }
}
