package com.potato.bytecode.classreader;

import com.potato.bytecode.FileUtils;
import org.objectweb.asm.ClassReader;

import java.util.Arrays;

/**
 * import java.io.Serializable;
 *
 * public class HelloWorld2 extends Exception implements Serializable, Cloneable {
 *
 * }
 *
 * access: 33
 * className: sample/HelloWorld2
 * superName: java/lang/Exception
 * interfaces: [java/io/Serializable, java/lang/Cloneable]
 */
public class HelloWorldRun {
    public static void main(String[] args) throws Exception {
        String relative_path = "sample/HelloWorld2.class";
        String filepath = FileUtils.getFilePath(relative_path);
        byte[] bytes = FileUtils.readBytes(filepath);

        //（1）构建ClassReader
        ClassReader cr = new ClassReader(bytes);

        // (2) 调用getXxx()方法
        int access = cr.getAccess();
        System.out.println("access: " + access);

        String className = cr.getClassName();
        System.out.println("className: " + className);

        String superName = cr.getSuperName();
        System.out.println("superName: " + superName);

        String[] interfaces = cr.getInterfaces();
        System.out.println("interfaces: " + Arrays.toString(interfaces));
    }
}