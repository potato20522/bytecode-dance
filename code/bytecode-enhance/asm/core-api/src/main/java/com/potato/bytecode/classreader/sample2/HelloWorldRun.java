package com.potato.bytecode.classreader.sample2;

import sample.HelloWorld;

/**
 * HelloWorldTransformCore 给类添加实现接口 的验证
 */
public class HelloWorldRun {
    public static void main(String[] args) throws Exception {
        HelloWorld obj = new HelloWorld();
        Object anotherObj = obj.clone();
        System.out.println(anotherObj);
    }
}