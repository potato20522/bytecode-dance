package com.potato.bytecode.modifymethod.clearbody;

public class HelloWorldRun {
    public static void main(String[] args) throws Exception {
        HelloWorld instance = new HelloWorld();
        instance.verify("jerry", "123");
    }
}