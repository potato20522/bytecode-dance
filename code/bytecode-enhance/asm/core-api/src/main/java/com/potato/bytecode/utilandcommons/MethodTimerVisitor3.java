package com.potato.bytecode.utilandcommons;

import com.potato.bytecode.FileUtils;
import org.objectweb.asm.*;
import org.objectweb.asm.commons.AdviceAdapter;
import org.objectweb.asm.commons.LocalVariablesSorter;
import sample.HelloWorldForLocalVariablesSorter;

import static org.objectweb.asm.Opcodes.*;
//方式一：使用LocalVariablesSorter类
public class MethodTimerVisitor3 extends ClassVisitor {
    public MethodTimerVisitor3(int api, ClassVisitor classVisitor) {
        super(api, classVisitor);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);
        if (mv != null && !"<init>".equals(name) && !"<clinit>".equals(name)) {
            boolean isAbstractMethod = (access & ACC_ABSTRACT) != 0;
            boolean isNativeMethod = (access & ACC_NATIVE) != 0;
            if (!isAbstractMethod && !isNativeMethod) {
                mv = new MethodTimerAdapter3(api, access, name, descriptor, mv);
            }
        }
        return mv;
    }

    /**
     * 需要注意的是，我们使用的是mv.visitVarInsn(opcode, var)方法，
     * 而不是使用super.visitVarInsn(opcode, var)方法。
     * 为什么要使用mv，而不使用super呢？
     * 因为使用super.visitVarInsn(opcode, var)方法，
     * 实质上是调用了LocalVariablesSorter.visitVarInsn(opcode, var)，
     * 它会进一步调用remap(var, type)方法，这就可能导致新添加的变量在local variables中的位置发生“位置偏移”。
     */
    private static class MethodTimerAdapter3 extends LocalVariablesSorter {
        private final String methodName;
        private final String methodDesc;
        private int slotIndex;
        public MethodTimerAdapter3(int api, int access, String name, String descriptor, MethodVisitor methodVisitor) {
            super(api,access, descriptor, methodVisitor);
            this.methodName = name;
            this.methodDesc = descriptor;
        }

        @Override
        public void visitCode() {
            //首先，实现自己的逻辑: long t = System.currentTimeMillis();
            slotIndex = newLocal(Type.LONG_TYPE);//创建一个局部变量,类型为long
            //调用静态方法： System.currentTimeMillis()
            mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "currentTimeMillis", "()J", false);
            //lstore指令，将操作数栈(Operand Stack)的栈顶long类型数值存入局部变量表slotIndex的位置
            mv.visitVarInsn(LSTORE,slotIndex);
            //其次，调用父类的实现
            super.visitCode();
        }

        @Override
        public void visitInsn(int opcode) {
            //首先，实现自己的逻辑: t = System.currentTimeMillis() - t;
            //如果是return指令或者是抛异常指令
            if (opcode >= IRETURN && opcode <= RETURN || opcode == ATHROW) {
                //调用静态方法： System.currentTimeMillis()
                mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "currentTimeMillis", "()J", false);
                //lload:将局部变量表slotIndex的位置的数，压入到操作数栈的栈顶
                mv.visitVarInsn(LLOAD, slotIndex);
                //lsub指令：将操作数栈顶的两个long数值相减，并将结果压入栈顶
                mv.visitInsn(LSUB);
                //lstore指令，将操作数栈(Operand Stack)的栈顶long类型数值存入局部变量表slotIndex的位置
                mv.visitVarInsn(LSTORE, slotIndex);

                // System.out.println("test method execute: " + t); //注意使用StringBuilder进行字符串拼接
                mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
                mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
                mv.visitInsn(DUP);
                mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
                mv.visitLdcInsn(methodName + methodDesc + " method execute: ");
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
                mv.visitVarInsn(LLOAD, slotIndex);
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(J)Ljava/lang/StringBuilder;", false);
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
            }
            // 其次，调用父类的实现
            super.visitInsn(opcode);
        }
    }
}


//方式二：使用AdviceAdapter类
class MethodTimerVisitor4 extends ClassVisitor {
    public MethodTimerVisitor4(int api, ClassVisitor classVisitor) {
        super(api, classVisitor);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);
        if (mv != null) {
            boolean isAbstractMethod = (access & ACC_ABSTRACT) != 0;
            boolean isNativeMethod = (access & ACC_NATIVE) != 0;
            if (!isAbstractMethod && !isNativeMethod) {
                mv = new MethodTimerAdapter4(api, mv, access, name, descriptor);
            }
        }
        return mv;
    }

    private static class MethodTimerAdapter4 extends AdviceAdapter {
        private int slotIndex;

        public MethodTimerAdapter4(int api, MethodVisitor mv, int access, String name, String descriptor) {
            super(api, mv, access, name, descriptor);
        }

        @Override
        protected void onMethodEnter() {
            slotIndex = newLocal(Type.LONG_TYPE);
            mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "currentTimeMillis", "()J", false);
            mv.visitVarInsn(LSTORE, slotIndex);
        }

        @Override
        protected void onMethodExit(int opcode) {
            if ((opcode >= IRETURN && opcode <= RETURN) || opcode == ATHROW) {
                mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "currentTimeMillis", "()J", false);
                mv.visitVarInsn(LLOAD, slotIndex);
                mv.visitInsn(LSUB);
                mv.visitVarInsn(LSTORE, slotIndex);
                mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
                mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
                mv.visitInsn(DUP);
                mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
                mv.visitLdcInsn(getName() + methodDesc + " method execute: ");
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
                mv.visitVarInsn(LLOAD, slotIndex);
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(J)Ljava/lang/StringBuilder;", false);
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
            }
        }
    }
}

class HelloWorldTransformCore2 {
    public static void main(String[] args) {
        String relative_path = "sample/HelloWorldForLocalVariablesSorter.class";
        String filepath = FileUtils.getFilePath(relative_path);
        byte[] bytes1 = FileUtils.readBytes(filepath);

        //（1）构建ClassReader
        ClassReader cr = new ClassReader(bytes1);

        //（2）构建ClassWriter
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        //（3）串连ClassVisitor
        int api = Opcodes.ASM9;
//        ClassVisitor cv = new MethodTimerVisitor3(api, cw);
        ClassVisitor cv = new MethodTimerVisitor4(api, cw);

        //（4）结合ClassReader和ClassVisitor
        int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
        cr.accept(cv, parsingOptions);

        //（5）生成byte[]
        byte[] bytes2 = cw.toByteArray();

        FileUtils.writeBytes(filepath, bytes2);
    }
}

class HelloWorldRun2 {
    public static void main(String[] args) throws Exception {
        HelloWorldForLocalVariablesSorter instance = new HelloWorldForLocalVariablesSorter();
        instance.test(10, 20);
    }
}