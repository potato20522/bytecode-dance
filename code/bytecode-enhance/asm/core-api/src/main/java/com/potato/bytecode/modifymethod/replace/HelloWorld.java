package com.potato.bytecode.modifymethod.replace;

/**
 * 替换方法体
 */
public class HelloWorld {
    public void test(int a, int b) {
        int c = Math.max(a, b);
        System.out.println(c);
    }
}