package com.potato.bytecode.utilandcommons;

import com.potato.bytecode.FileUtils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.util.CheckClassAdapter;

import java.io.PrintWriter;

import static org.objectweb.asm.Opcodes.*;
import static org.objectweb.asm.Opcodes.RETURN;

/**
 * 第二种使用方式，是在生成类（Class Generation）或转换类（Class Transformation）的结束后进行检查：
 * byte[] bytes = ... // 这里是class file bytes
 * PrintWriter printWriter = new PrintWriter(System.out);
 * CheckClassAdapter.verify(new ClassReader(bytes), false, printWriter);
 */
public class CheckClassAdapterExample02Generate {
    public static String OBJECT = "java/lang/Object";
    public static void main(String[] args) {
        String classPackageName = "sample/HelloWorld";
        String relativePath = classPackageName + ".class";
        String filepath = FileUtils.getFilePath(relativePath);

        // (1) 生成byte[]内容
        byte[] bytes = dump(classPackageName);

        // (2) 保存byte[]到文件
        FileUtils.writeBytes(filepath, bytes);

        // (3) 检查
        PrintWriter printWriter = new PrintWriter(System.out);
        CheckClassAdapter.verify(new ClassReader(bytes), false, printWriter);
    }
    public static byte[] dump(String classPackageName) {
        //1.创建ClassWriter对象
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        //2.调用visitXxx()方法
        cw.visit(V1_8, ACC_PUBLIC + ACC_SUPER, classPackageName, null, OBJECT, null);
        {
            FieldVisitor fv = cw.visitField(ACC_PRIVATE, "initValue", "I", null, null);
            fv.visitEnd();
        }
        {
            MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            mv.visitCode();
            mv.visitVarInsn(ALOAD, 0);
            mv.visitMethodInsn(INVOKESPECIAL, OBJECT, "<init>", "()V", false);
            mv.visitInsn(RETURN);
            mv.visitMaxs(1, 1);
            mv.visitEnd();
        }
        {
            MethodVisitor mv2 = cw.visitMethod(ACC_PUBLIC, "test", "()V", null, null);
            mv2.visitCode();
            mv2.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
            mv2.visitLdcInsn("Hello World");
            mv2.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
            mv2.visitInsn(RETURN);
            mv2.visitMaxs(2, 1);
            mv2.visitEnd();
        }
        cw.visitEnd();

        return cw.toByteArray();
    }
}
