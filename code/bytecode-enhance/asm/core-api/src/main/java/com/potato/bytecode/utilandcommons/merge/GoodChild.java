package com.potato.bytecode.utilandcommons.merge;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GoodChild implements Serializable {
    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public void printDate() {
        Date now = new Date();
        String str = df.format(now);
        System.out.println(str);
    }
}