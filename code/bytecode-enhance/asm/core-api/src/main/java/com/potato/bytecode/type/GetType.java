package com.potato.bytecode.type;

import org.junit.jupiter.api.Test;
import org.objectweb.asm.Type;

public class GetType {
    @Test
    void getStringType() {
        Type t = Type.getType("Ljava/lang/String;");

        int sort = t.getSort();                    // ASM
        String className = t.getClassName();       // Java File
        String internalName = t.getInternalName(); // Class File
        String descriptor = t.getDescriptor();     // Class File

        System.out.println(sort);         // 10，它对应于Type.OBJECT字段
        System.out.println(className);    // java.lang.String   注意，分隔符是“.”
        System.out.println(internalName); // java/lang/String   注意，分隔符是“/”
        System.out.println(descriptor);   // Ljava/lang/String; 注意，分隔符是“/”，前有“L”，后有“;”
    }

    @Test
    void getTypeByClass() {
        Type t = Type.getType(String.class);
        System.out.println(t); //Ljava/lang/String;
    }

    @Test
    void getTypeByDescriptor() {
        Type t1 = Type.getType("Ljava/lang/String;");
        System.out.println(t1);

        // 这里是方法的描述符
        Type t2 = Type.getMethodType("(II)I");
        System.out.println(t2);
    }

    @Test
    void getTypeByInternalName() {
        Type t = Type.getObjectType("java/lang/String");
        System.out.println(t);
    }

    @Test
    void getBtStaticField() {
        Type t = Type.INT_TYPE;
        System.out.println(t);//I
    }
}