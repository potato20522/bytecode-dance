package com.potato.bytecode.utilandcommons.merge;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;


/**
 * 合并两个类中的方法(构造方法排除)
 */
public class ClassMergeVisitor extends ClassVisitor {
    private final ClassNode anotherClass;

    public ClassMergeVisitor(int api, ClassVisitor classVisitor, ClassNode anotherClass) {
        super(api, classVisitor);
        this.anotherClass = anotherClass;
    }

    @Override
    public void visitEnd() {
        //这里没有考虑两个类有重复字段的情况。
        for (FieldNode fn : anotherClass.fields) {
            fn.accept(this);
        }
        for (MethodNode mn : anotherClass.methods) {
            String methodName = mn.name;
            if ("<init>".equals(methodName)) {
                continue;
            }
            mn.accept(this);
        }
        super.visitEnd();
    }
}
