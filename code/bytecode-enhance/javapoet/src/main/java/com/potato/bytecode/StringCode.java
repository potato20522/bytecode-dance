package com.potato.bytecode;

import com.squareup.javapoet.MethodSpec;

public class StringCode {
    public static void main(String[] args) {
//        MethodSpec main = MethodSpec.methodBuilder("main")
//                .addCode(""
//                        + "int total = 0;\n"
//                        + "for (int i = 0; i < 10; i++) {\n"
//                        + "  total += i;\n"
//                        + "}\n")
//                .build();
        MethodSpec main = MethodSpec.methodBuilder("main")
                .addStatement("int total = 0")
                .beginControlFlow("for (int i = 0; i < 10; i++)")
                .addStatement("total += i")
                .endControlFlow()
                .build();
        System.out.println(main);
    }
}
//void main() {
//  int total = 0;
//  for (int i = 0; i < 10; i++) {
//    total += i;
//  }
//}