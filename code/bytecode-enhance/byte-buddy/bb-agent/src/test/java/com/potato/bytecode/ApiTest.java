package com.potato.bytecode;

/**
 * VM options：
 * -javaagent:E:\Gitee\bytecode-dance\code\bytecode-enhance\byte-buddy\bb-agent\target\bb-agent-1.0-SNAPSHOT.jar=testArgs
 *
 */
public class ApiTest {

    public static void main(String[] args) throws InterruptedException {
        ApiTest apiTest = new ApiTest();
        apiTest.echoHi();
    }

    private void echoHi() throws InterruptedException {
        System.out.println("hi agent");
        Thread.sleep((long) (Math.random() * 500));
    }

}
