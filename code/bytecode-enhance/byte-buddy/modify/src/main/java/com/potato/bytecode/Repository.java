package com.potato.bytecode;

public abstract class Repository<T> {

    public abstract T queryData(int id);

}
