package com.potato.bytecode;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatchers;
import org.junit.jupiter.api.Test;

public class ModifyBizMethod {
    @Test
    public void test_byteBuddy() throws Exception {
        DynamicType.Unloaded<?> dynamicType = new ByteBuddy()
                .subclass(BizMethod.class) //新生成的是BizMethod的子类
                .method(ElementMatchers.named("queryUserInfo")) //找到queryUserInfo方法
                .intercept(MethodDelegation.to(MonitorDemo.class))//委托至MonitorDemo类
                .make();

        // 加载类
        Class<?> clazz = dynamicType.load(ModifyBizMethod.class.getClassLoader())
                .getLoaded();

        // 反射调用
        clazz.getMethod("queryUserInfo", String.class, String.class).invoke(clazz.newInstance(), "10001", "Adhl9dkl");
    }

}
