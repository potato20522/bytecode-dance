package com.potato.bytecode.helloworld;

import java.lang.reflect.Field;
import java.util.Vector;

public class StaticCode {
    public static void main(String[] args) throws Exception {
        System.out.println("hello word: " );
        Field classesF = ClassLoader.class.getDeclaredField("classes");
        classesF.setAccessible(true);
        Vector<Class<?>> classes = (Vector<Class<?>>) classesF.get(StaticCode.class.getClassLoader());
        for(Class c : classes) {
            System.out.println(c);
        }
        System.out.println("");
    }
}
