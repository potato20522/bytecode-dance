package com.potato.bytecode.annotation;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.annotation.AnnotationDescription;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

/**
 * @MyTag
 * public class Person {
 *     public Person() {
 *     }
 * }
 */
public class PersonMyTagAnnotation {
    @Test
    void annotationDescription() throws IOException {
        AnnotationDescription description = AnnotationDescription.Builder.ofType(MyTag.class)
                .build();
        new ByteBuddy()
                .subclass(Object.class)
                .annotateType(description)
                .name("com.potato.bytecode.Person")
                .make()
                .saveIn(new File("target/classes/"));
    }

}
