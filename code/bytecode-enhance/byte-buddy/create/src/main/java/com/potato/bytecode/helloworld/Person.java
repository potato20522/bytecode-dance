package com.potato.bytecode.helloworld;

public class Person {
    //静态块在类初始化的时候被执行，而不是类加载的时候
    static {
        System.out.println("static block ");
    }
}
