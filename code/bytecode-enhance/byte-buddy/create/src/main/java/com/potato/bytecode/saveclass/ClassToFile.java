package com.potato.bytecode.saveclass;

import com.potato.bytecode.annotation.MyTag;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.annotation.AnnotationDescription;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

/**
 * 生成的class保存到文件或jar
 */
public class ClassToFile {
    /**
     * class保存到目录
     * @throws IOException
     */
    @Test
    void saveInFile() throws IOException {
        new ByteBuddy()
                .subclass(Object.class)
                .name("sample.HelloWorld")
                .make()
                //saveIn 传入目录中
                .saveIn(new File("target/classes/"));
    }

    /**
     * class保存到全新的jar包
     * @throws IOException
     */
    @Test
    void saveToJar() throws IOException {
        new ByteBuddy()
                .subclass(Object.class)
                .name("sample.HelloWorld")
                .make()
                //toJar class文件保存到全新的jar包中
                //每次都是重新生成jar包，如果之前存在的话，会先全部抹除
                //目录，即target目录必须存在
                .toJar(new File("target/my.jar"));
    }

    /**
     * 作用：class注入到已有的jar包中
     * 先运行saveToJar(),保证已经有一个jar包了
     * 再运行本方法，用解压缩软件查看my.jar里面的内容，发现有HelloWorld.class和Person.class
     * 再次运行的话，会覆盖类重名的class文件
     * 应用场景：向三方库的jar包中添加、替换class文件，
     *   但同时需要注意，如何找到三方库的jar包的路径，不能写死是吧
     * @throws IOException
     */
    @Test
    void saveInject() throws IOException {
//        AnnotationDescription description = AnnotationDescription.Builder.ofType(MyTag.class).build();
        new ByteBuddy()
                .subclass(Object.class)
//                .annotateType(description)
                .name("sample.Person")
                .make()
                .inject(new File("target/my.jar"));
    }
}
