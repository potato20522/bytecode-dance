package com.potato.bytecode.customename;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.implementation.FixedValue;
import net.bytebuddy.matcher.ElementMatchers;

import java.io.File;
import java.io.IOException;

/**
 * 自定义类名(包含包名)
 */
public class Name {
    public static void main(String[] args) throws IOException {
        new ByteBuddy()
                .subclass(Object.class)
                .method(ElementMatchers.named("toString"))
                .intercept(FixedValue.value("Hello World!"))
                .name("com.potato.HelloWorld")
                .make()
                .saveIn(new File("target/classes/"));//将class文件输出到指定目录

    }
}
