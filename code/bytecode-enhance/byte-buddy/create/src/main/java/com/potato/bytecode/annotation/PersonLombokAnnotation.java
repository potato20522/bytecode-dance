package com.potato.bytecode.annotation;

import lombok.Data;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.annotation.AnnotationDescription;
import net.bytebuddy.implementation.FixedValue;
import net.bytebuddy.matcher.ElementMatchers;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

/**
 * 作用域为SOURCE的注解不会生效，比如lombok的@Data
 * 因为byte-buddy是直接生成字节码，而注解处理器是发生在编译期
 */
public class PersonLombokAnnotation {
    private String name;

    @Test
    void lombok() throws IOException {
        AnnotationDescription description = AnnotationDescription.Builder.ofType(Data.class)
                .build();
        new ByteBuddy()
                .subclass(Object.class)
                .annotateType(description)
                .name("com.potato.bytecode.Person")
                .make()
                .saveIn(new File("target/classes/"));

    }
}
