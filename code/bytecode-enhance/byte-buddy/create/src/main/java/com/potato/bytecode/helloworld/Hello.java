package com.potato.bytecode.helloworld;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.FixedValue;
import net.bytebuddy.matcher.ElementMatchers;

/**
 *
 */
public class Hello {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        Class<?> dynamicType = new ByteBuddy()
                .subclass(Object.class) //父类
                .method(ElementMatchers.named("toString")) //创建toString方法
                //intercept拦截方法
                .intercept(FixedValue.value("Hello World!")) //方法返回固定值：Hello World!
                .name("HelloWorld") //类名
                .make() //生成(编译?)
                .load(Hello.class.getClassLoader())//使用类加载器加载
                .getLoaded();//获取

        Object instance = dynamicType.newInstance();//实例化对象
        String toString = instance.toString();//调用toString方法
        System.out.println(toString);
        System.out.println(instance.getClass().getCanonicalName());//获取类名
    }
}
